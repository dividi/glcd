<!--

	Liste des echanges

-->


	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
		<div class="container-fluid">
		
		<!-- Left part -->
		<ul class="nav navbar-nav">
			<li><a class="navbar-brand" href="#"></a></li>	
			<li class="navbar-title">ECHANGES de MANUELS</li>
		</ul>


		<!-- Right part -->
		<ul class="nav navbar-nav navbar-right">

			<!-- Help -->
			<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="D'ici on peut consulter tous les mouvements d'échanges de manuels effectués lors des prêts."><span class="glyphicon glyphicon-info-sign"></span> Info </a></li>
			
			<!-- Filter -->
			<li data-toggle="tooltip-filtre">
				<form class="navbar-form" role="search">
					<div class="form-group">
						<input class="form-control" placeholder=" filtrer" name="filt" id="filt" onKeyPress="return disableEnterKey(event)" onkeyup="filter(this.value, 'echanges_table');" type="text" value=<?PHP echo $_GET['filter'];?>> 
						&nbsp;<span class="navbar-text navbar-right" id="filtercount" title="Nombre de lignes filtrées"></span>
					</div>
				</form>					
			</li>

		</ul>
		</div>
	</nav>

	<div class="spacer"></div>	

<?PHP 

	// cnx à la base de données
	$cnx = new Sql ( $host, $user, $pass, $db );

	// stockage des lignes retournées par sql dans un tableau nommé liste_des_materiels
	$liste = $cnx->QueryAll ( "SELECT echangeID, echangeDate, exeREF, exePrix, exeAchat, exeEtat, exeCD, userID1, userID2, manuelTitre, manuelEdition FROM echanges, exemplaires, manuels  
		WHERE 
			exemplaires.exeID = echanges.exeID AND 
			exemplaires.manuelID = manuels.manuelID
		ORDER BY echangeDate" );

?>
		
	<center>

	<table class="table table-condensed hover" id="echanges_table">
		<th>REF</th>
		<th>Titre</th>
		<th>Achat</th>
		<th>Prix</th>
		<th>CD</th>
		<th>Date</th>
		<th>Prêté à</th>
		<th>Rendu par</th>
		
		<?PHP	
		
			foreach ( $liste as $record ) {
				
						
				$echangeID		= $record['echangeID'];
				$echangeDate	= $record['echangeDate'];
				$exeREF			= $record['exeREF'];
				$exePrix		= $record['exePrix'];
				$exeAchat		= $record['exeAchat'];
				$exeCD			= $record['exeCD'];
				$userID1		= $record['userID1'];
				$userID2		= $record['userID2'];
				$manuelTitre	= $record['manuelTitre'];
				$manuelEdition	= $record['manuelEdition'];
				
				$user1 = $cnx->QueryOne ("SELECT CONCAT (userNom, ' ', userPrenom) as nom FROM users WHERE userID=$userID1");
				$user2 = $cnx->QueryOne ("SELECT CONCAT (userNom, ' ', userPrenom) as nom FROM users WHERE userID=$userID2");
				
				$exeCDlabel = $exeCD==1 ? "<img src='construction/accept.png' height=15>" : "";
				
				
				echo "<tr>";
					echo "<td> $exeREF</td>";
					echo "<td> $manuelTitre </td>";
					
					echo "<td> $exeAchat </td>";
					echo "<td> $exePrix </td>";
					echo "<td> $exeCDlabel </td>";
					echo "<td> $echangeDate </td>";
					echo "<td> $user1 </td>";
					echo "<td> $user2 </td>";
				echo "</tr>";

			}
		?>		

	</table>
	
	<br>
	
	</center>
	
	
<?PHP
	// On se déconnecte de la db
	$cnx->Close();
?>

<script>
	// Filtre rémanent
	filter ( $('#filt').val(), 'echanges_table' );
</script>
