<!--

	Liste des manuels

-->


		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
			<div class="container-fluid">
						
			<!-- Left part -->
			<ul class="nav navbar-nav">
				<li><a class="navbar-brand" href="#"></a></li>	
				<li class="navbar-title">MANUELS</li>
			</ul>


			<!-- Right part -->
			<ul class="nav navbar-nav navbar-right">

				<!-- Add item -->
				<li><a href='manuels_form.php?action=add' class='editbox' data-tip="Ajouter un MANUEL" title='Ajouter un MANUEL'><span class="glyphicon glyphicon-plus-sign"></span> Ajout</a></li>
				
				<!-- Help -->
				<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Dans cette page on peut créer les manuels (les modèles des livres en fait)."><span class="glyphicon glyphicon-info-sign"></span> Info </a></li>
				
				<!-- Filter -->
				<li data-tip="Filtrer le tableau">
					<form class="navbar-form" role="search">
						<div class="form-group">
							<input class="form-control" placeholder=" filtrer" name="filt" id="filt" onKeyPress="return disableEnterKey(event)" onkeyup="filter(this.value, 'manuels_table');" type="text" value=<?PHP echo $_GET['filter'];?>> 
							&nbsp;<span class="navbar-text navbar-right" id="filtercount" data-tip="Nombre de lignes filtrées" title="Nombre de lignes filtrées"></span>
						</div>
					</form>					
				</li>

			</ul>
			</div>
		</nav>

	<div class="spacer"></div>	

<?PHP 

	// cnx à la base de données
	$cnx = new Sql ( $host, $user, $pass, $db );

	// stockage des lignes retournées par sql dans un tableau nommé liste_des_materiels
	$liste_des_manuels = $cnx->QueryAll ( "SELECT manuelID, manuelREF, manuelNiveau, manuelTitre, manuelEdition, matiereNom, editeurNom, manuels.matiereID, manuels.editeurID FROM manuels, matieres, editeurs WHERE manuels.matiereID=matieres.matiereID AND manuels.editeurID=editeurs.editeurID ORDER BY matiereID" );

?>
		
	<input type=hidden name='id_a_poster' id='id_a_poster' value=''>


	<center>
<div class="table-responsive">
	<table class="table table-condensed hover" id="manuels_table">
		<th>ISBN</th>
		<th>Niveau</th>
		<th>Titre</th>
		<th>Editeur</th>
		<th>Edition</th>
		<th>Matière</th>
		<th>Exemplaires</th>
		<th>Prêts</th>
		<th>Stock</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
		
		<?PHP	
				
			foreach ( $liste_des_manuels as $record ) {
							
				$manuelID		= $record['manuelID'];
				$manuelREF		= $record['manuelREF'];
				$manuelNiveau	= $record['manuelNiveau'];
				$manuelTitre	= $record['manuelTitre'];
				$manuelEdition	= $record['manuelEdition'];
				$matiereNom		= $record['matiereNom'];
				$editeurNom		= $record['editeurNom'];
				
				$nb_exemplaires = $cnx->QueryOne("SELECT COUNT(*) FROM exemplaires WHERE manuelID=$manuelID");
				$nb_prets = $cnx->QueryOne ("SELECT COUNT(prets.exeID) FROM prets, exemplaires WHERE prets.exeID=exemplaires.exeID AND manuelID=$manuelID");
				$nb_stock = $nb_exemplaires - $nb_prets;
				
		
				echo "<tr>";
					echo "<td> $manuelREF</td>";
					echo "<td> $manuelNiveau</td>";
					echo "<td> $manuelTitre </td>";
					echo "<td> $editeurNom </td>";
					echo "<td> $manuelEdition </td>";
					echo "<td> $matiereNom </td>";
					echo "<td> $nb_exemplaires </td>";
					echo "<td> $nb_prets </td>";
					echo "<td> $nb_stock </td>";
					
					echo "<td width=20 align=center><a href='manuels_form.php?id=$manuelID&action=mod' class='editbox' data-tip='Modifier' title='Modifier'><span class='glyphicon glyphicon-edit table-icon'></span></a></td>";
					echo "<td width=20 align=center><a href='manuels_form.php?action=del&id=$manuelID' class='editbox' data-tip='Supprimer' title='Supprimer'><span class='glyphicon table-icon glyphicon-remove color-red'></span></a></td>";
					
				echo "</tr>";

			}
		?>		

	</table>
	</div>
	<br>
	
	</center>
	
	
<?PHP
	// On se déconnecte de la db
	$cnx->Close();
?>

<script>
	// Filtre rémanent
	filter ( $('#filt').val(), 'manuels_table' );

</script>
