<?PHP
	#formulaire d'ajout et de modification
	#des exemplaires !
	
	// lib
	include ('config.php');	// fichiers de configuration des bases de données
	require_once ('fonctions.php');
	include_once ('class/Log.class.php');
	include_once ('class/Sql.class.php');

	// connexion à la base de données
	$cnx 	= new Sql ($host, $user,$pass, $db);

?>
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-barcode.min.js"></script>
	
	<link rel="stylesheet" href="css/barcodes_print.css" type="text/css" media="print"  /> 


<script>

	function generateBarcode(target, value){
		var btype = "code128";
		var renderer = "css";

		var settings = {
		  output:renderer,
		  bgColor: "#FFFFFF",
		  color: "#000000",
		  barWidth: "1",
		  barHeight: "58"
		};

		$(target).html("").show().barcode(value, btype, settings);

	}
			
	

</script>

<?PHP

	$parligne = 4;
	$nbcell = 0;

	// cnx à la base de données
	$cnx = new Sql ( $host, $user, $pass, $db );

	$lot = $_POST ['id_a_poster'];
	$lot_array = explode(";", $lot);
	
	// cnx
	$cnx = new Sql($host, $user, $pass, $db);
	
	echo "<table class=barcodes>";
	echo "<tr>";
	foreach ($lot_array as $exeID) {
		
		if ( $exeID <> "" ) {
			
			$exeREF = $cnx->QueryOne ( "SELECT exeREF FROM exemplaires WHERE exeID = $exeID " );	
			
			echo "<td>";
				echo "<div id='$exeID' class='barcode'></div>";
				echo "<script>$('#' + $exeID).html('').show().barcode('$exeREF', 'code128', { output:'css', bgColor: '#FFFFFF',  color: '#000000',  barWidth: '1',  barHeight: '64' });</script>";
			echo "</td>";
			
			
			$nbcell++;
			
			if ($nbcell == $parligne) {
				echo "</tr><tr>";
				$nbcell = 0;
			}

		}
	}
	
	echo "</tr>";
	echo "<span class='fiche-break'></span>";

	echo "</table>";
	
	
	$cnx->Close();
?>





