<?PHP


	// fichier ajax pour les prêts
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');	

if($_REQUEST) {

	// cnx
	$cnx = new Sql($host, $user, $pass, $db);

	$ref 	= $_REQUEST['ref'];
	
	$deja_prete = $cnx->QueryOne("SELECT userID FROM prets, exemplaires WHERE exemplaires.exeID=prets.exeID AND exeREF='$ref' ");
	
	if ($deja_prete) {
		echo "<td colspan=12><span style='color:red;'>L'exemplaire <b>$ref</b> est DEJA prêté !</span></td>";
	}
	else {
		
		$exemplaire = $cnx->QueryRow ( "
			SELECT exeID, manuelNiveau, manuelTitre, manuelEdition, matiereNom, editeurNom, exeEtat, exeREF, exePrix, exeAchat, exeCD
			FROM manuels, matieres, editeurs, exemplaires
			WHERE 
				manuels.matiereID=matieres.matiereID AND
				manuels.editeurID=editeurs.editeurID AND
				exemplaires.manuelID=manuels.manuelID AND
				exeREF = '$ref'" );
				
				$exeID = $exemplaire['exeID'];
				$manuelNiveau = $exemplaire['manuelNiveau'];
				$manuelTitre = $exemplaire['manuelTitre'];
				$manuelEdition = $exemplaire['manuelEdition'];
				$matiereNom = $exemplaire['matiereNom'];
				$editeurNom = $exemplaire['editeurNom'];
				$exeEtat = $exemplaire['exeEtat'];
				$exeREF = $exemplaire['exeREF'];
				$exePrix = $exemplaire['exePrix'];
				$exeCD = $exemplaire['exeCD'];
				$exeAchat = $exemplaire['exeAchat'];

		
		echo "<td></td>";
		echo "<td>$exeREF<input type=hidden value='$exeID' name='exeID[]'></td>";
		echo "<td>$matiereNom</td>";
		echo "<td>$editeurNom</td>";
		echo "<td>$manuelNiveau</td>";
		echo "<td>$manuelTitre</td>";
		echo "<td>$manuelEdition</td>";
		echo "<td>$exeAchat</td>";
		echo "<td>$exePrix</td>";
		
		echo "<td><select name='cd[]' class='cd'>
			<option value=1 "; if($exeCD == 1) echo " selected"; echo">OUI</option>
			<option value=0 "; if($exeCD == 0) echo " selected"; echo">NON</option>
			</select></td>";
			
		echo "<td><select name='etat[]' class='etat'>
			<option value=4 "; if($exeEtat == 4) echo " selected"; echo">4 - NEUF</option>
			<option value=3 "; if($exeEtat == 3) echo " selected"; echo">3 - BON</option>
			<option value=2 "; if($exeEtat == 2) echo " selected"; echo">2 - MOYEN</option>
			<option value=1 "; if($exeEtat == 1) echo " selected"; echo">1 - PASSABLE</option>
		</select></td>";
		echo "<td><a href='#' class='delete' onclick='deleteEXE(\"$ref\");'><span class='glyphicon table-icon glyphicon-remove color-red'></span></a></td>";
	}
	
}
?>
