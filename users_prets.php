<link rel="stylesheet" href="css/fiches_print.css" type="text/css" media="print"  /> 
<meta http-equiv=Content-Type content="text/html; charset=utf-8" />


<?PHP

	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');		
	
	
	
	// Cnx à  la base
	$cnx = new Sql($host, $user, $pass, $db);
	

	$college = $cnx->QueryOne ("SELECT configValue FROM config WHERE configKey='college'");
	$adresse = $cnx->QueryOne ("SELECT configValue FROM config WHERE configKey='adresse'");
	$rentree = $cnx->QueryOne ("SELECT configValue FROM config WHERE configKey='rentree'");
	$rentree2 = $rentree+1;
	$rendre_fiche = $cnx->QueryOne ("SELECT configValue FROM config WHERE configKey='rendre_fiche'");
	

	$lot = $_POST["id_a_poster"];
	$lot_array = explode(";", $lot);
	
	foreach ( $lot_array as $userID ) {
		
		if ($userID <> '') {
					
			$user = $cnx->QueryRow ("SELECT userNom, userPrenom, CONCAT(classeNiveau,classeNom) as classe FROM users, classes WHERE users.classeID=classes.classeID AND userID=$userID;");
			$userNom = $user["userNom"];
			$userPrenom = $user["userPrenom"];
			$userClasse = $user["classe"];
			
			$exeIDs = $cnx->QueryAll ("SELECT exeID FROM prets WHERE userID=$userID;");
				
			$buffer = "";

			$buffer .= "
			<center>
				
				<table id='fiche_pret_entete'>
					
					<tr>
						<td class='logo'><img src='construction/academie.png'></td>
						<td><span class='college'>$college</span><br><span class='college_adr'>$adresse</span></td>
						<td class='logo'><img src='construction/defferre.png'></td>
					</tr>
					<tr>
						<td colspan=3 class='fiche'>FICHE DE PRET MANUELS SCOLAIRES $rentree - $rentree2</td>
					</tr>
					<tr>
						<td class='eleve'>Nom : $userNom</td>
						<td class='eleve'>Prénom : $userPrenom</td>
						<td class='eleve'>Classe : $userClasse</td>
					</tr>		
					
				</table>
			
			
				<table id='fiche_pret_manuels'>
					<th>Titre</th>
					<th>Editeur</th>
					<th>Matière</th>
					<th>CD</th>
					<th>Date Achat</th>
					<th>Prix</th>
					<th>Etat</th>
					<th>Observations</th>
			";
			
				// Livres prêtés
				foreach ($exeIDs as $exemplaire) {
							
					$exeInfo = $cnx->QueryRow ("
						SELECT manuelTitre, manuelEdition, matiereNom, editeurNom, exeREF, exePrix, exeAchat, exeEtat, exeCD
						FROM manuels, matieres, editeurs, exemplaires
						WHERE 
							manuels.matiereID=matieres.matiereID AND
							manuels.editeurID=editeurs.editeurID AND
							exemplaires.manuelID=manuels.manuelID AND
							exeID=".$exemplaire['exeID']);
					
					$manuelTitre = $exeInfo["manuelTitre"];
					$matiereNom = $exeInfo["matiereNom"];
					$editeurNom = $exeInfo["editeurNom"];
					$exeREF = $exeInfo["exeREF"];
					$exePrix = $exeInfo["exePrix"];
					$exeAchat = $exeInfo["exeAchat"];
					$exeEtat = $exeInfo["exeEtat"];
					$exeCD = $exeInfo["exeCD"];
					
					switch ($exeEtat) {
						case 4 : $exeEtatLabel = "NEUF"; $etatClass="neuf"; break;
						case 3 : $exeEtatLabel = "BON"; $etatClass="bon"; break;
						case 2 : $exeEtatLabel = "MOYEN"; $etatClass="moyen"; break;
						case 1 : $exeEtatLabel = "PASSABLE"; $etatClass="passable"; break;
					}	
					
					$exeCDlabel = $exeCD == 1 ? "O": "N";

					$buffer .= "
					<tr>
						<td>$manuelTitre</td>			
						<td>$editeurNom</td>			
						<td>$matiereNom</td>			
						<td>$exeCDlabel</td>					
						<td>$exeAchat</td>			
						<td>$exePrix</td>			
						<td>$exeEtatLabel</td>			
						<td width=250></td>			
					<tr>";	
					
				}
			
				
				$buffer .= "</table>";
				
				$buffer .= "
				<div id='fiche_pret_pied1'>
					Je m'engage à couvrir les manuels et à en prendre soin. En cas de détérioration ou de perte une dégradation sera appliquée en fonction d'un barème dégressif :<br>			
					1ère année = prix du livre neuf / 2ème année = 50% du prix du livre neuf<br>
					3ème année = 30% du prix du livre neuf / 4ème année = 20% du prix du livre neuf<br>
					5ème année et suivante = FORFAIT<br>
					CD = 5€
				</div>
				
				<div id='fiche_pret_arendre'>
					A RENDRE AU PROFESSEUR PRINCIPAL IMPERATIVEMENT AVANT LE : $rendre_fiche
				</div>
				
				<div id='fiche_pret_pied2'>
					ECRIRE SON NOM ET CLASSE SUR CHAQUE LIVRE<br>
					TOUTE PAGE ARRACHEE, ECRITE OU COUVERTURE ABIMEE = <b>DEGRADATION</b>
				</div>
						
				<div id='fiche_pret_signature'>
					<span style='float:left;'>Marseille, le</span> <br>
					<span style='float:left;'>Signature de l'élève</span><span style='float:right;'>Signature du représentant légal de l'élève</span>
				</div>		
				";
				
				echo $buffer;
				
				// On saute à la page suivante si ce n'est pas le dernier user
				if (each($lot_array)) echo "<p class='fiche-break'></p>";
			}
		}
				
	?>
	
	<!-- impression-->
	<script>
		window.print();
	</script>
