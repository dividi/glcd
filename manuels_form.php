<?PHP
	#formulaire d'ajout et de modification
	#des manuels !
	
	// lib
	include ('config.php');	// fichiers de configuration des bases de données
	require_once ('fonctions.php');
	include_once ('class/Log.class.php');
	include_once ('class/Sql.class.php');
	
?>



<script type="text/javascript"> 


	$(function() {	
				
		// **************************************************************** POST AJAX FORMULAIRES
		$("#post_form").click(function(event) {

			/* stop form from submitting normally */
			event.preventDefault(); 
			
			if ( validForm() == true) {
			
				// Permet d'avoir les données à envoyer
				var dataString = $("#formulaire").serialize();
				
				// action du formulaire
				var url = $("#formulaire").attr( 'action' );
				
				var request = $.ajax({
					type: "POST",
					url: url,
					data: dataString,
					dataType: "html"
				 });
				 
				 request.done(function(msg) {
					$('#dialog').dialog('close');
					$('#targetback').show(); $('#target').show();
					$('#target').html(msg);
					window.setTimeout("document.location.href='index.php?page=manuels&filter=" + $('#filt').val() + "'", 2500);
				 });
			}			 
		});	
	});
</script>

<?PHP

	// connexion à la base de données
	$cnx = new Sql ($host, $user,$pass, $db);
	
	$action = $_GET['action'];

	
	
	
	#***************************************************************************
	# 				@@ CREATION
	#***************************************************************************
	
	if ( $action == 'add' ) {	// Formulaire vierge de création

		?>
		
		<script>
			// Donne le focus au premier champ du formulaire
			$('#nom').focus();
		</script>

		<form action="manuels_post.php?action=add" method="post" name="post_form" id="formulaire">
			<center>
			<table class="formtable" >
				
				<tr>
					<TD>REF *</TD>
					<TD><input type=text name=ref id=ref size=13 maxlength=13 class='valid nonvide'></TD>
				</tr>	
				
				<tr>
					<TD>Niveau *</TD>
					<TD><input type=text name=niveau id=niveau size=2 maxlength=2 class='valid nonvide'></TD>
				</tr>
				
				<tr>
					<TD>Titre *</TD>
					<TD><input type=text name=titre id=titre class='valid nonvide'></TD>
				</tr>
				
				<tr>
					<TD>Editeur *</TD>
					<TD>
						<select name=editeur>
							<?PHP
								$editeurs = $cnx->QueryAll("SELECT editeurID, editeurNom FROM editeurs ORDER BY editeurNom");
							
								foreach ($editeurs as $editeur) {
									$editeurID = $editeur["editeurID"];
									$editeurNom = $editeur["editeurNom"];
									
									echo "<option value=$editeurID>$editeurNom</option>";
								}
							?>
						</select>
					</TD>
				</tr>	

				<tr>
					<TD>Edition *</TD>
					<TD><input type=text name=edition id=edition size=4 maxlength=4 class='valid nonvide'></TD>
				</tr>
				
				<tr>
					<TD>Matière *</TD>
					<TD>
						<select name=matiere>
							<?PHP
								$matieres = $cnx->QueryAll("SELECT matiereID, matiereNom FROM matieres ORDER BY matiereNom");
							
								foreach ($matieres as $matiere) {
									$matiereID = $matiere["matiereID"];
									$matiereNom = $matiere["matiereNom"];
									
									echo "<option value=$matiereID>$matiereNom</option>";
								}
							?>
						</select>
					</TD>
				</tr>	
					
			</table>

			<br>
			<input id='post_form' type=submit value='Ajouter MANUEL'>
			</center>

		</FORM>
				

		<?PHP
		
	} 
	
	
	#***************************************************************************
	# 				@@ MODIFICATION
	#***************************************************************************
	
	if ($action == 'mod') {
			
		$id = $_GET['id'];
				
		// Requete pour récupérer les données des champs à modifier
		$manuel = $cnx->QueryRow ( "SELECT manuelID, manuelREF, manuelNiveau, manuelTitre, manuelEdition, manuels.matiereID as matiereID, manuels.editeurID as editeurID FROM manuels, matieres, editeurs WHERE manuels.matiereID=matieres.matiereID AND manuels.editeurID=editeurs.editeurID AND manuelID=$id" );		
		
		// valeurs à affecter aux champs
		$manuelREF		= $manuel['manuelREF'];
		$manuelNiveau	= $manuel['manuelNiveau'];
		$manuelTitre	= $manuel['manuelTitre'];
		$manuelEdition	= $manuel['manuelEdition'];
		$manuelMatiereID= $manuel['matiereID'];
		$manuelEditeurID= $manuel['editeurID'];
		
		?>
		
		<script>
			// Donne le focus au premier champ du formulaire
			$('#nom').focus();
		</script>
		
		<form action="manuels_post.php?action=mod" method="post" name="post_form" id="formulaire">
			<input type=hidden name=id value=<?PHP echo $id;?> >
			<center>
			<table class="formtable" >
							
				<tr>
					<TD>REF *</TD>
					<TD><input type=text name=ref id=ref class='valid nonvide' size=13 maxlength=13 value="<?PHP echo $manuelREF;?>"></TD>
				</tr>	
				
				<tr>
					<TD>Niveau *</TD>
					<TD><input type=text name=niveau id=niveau size=2 maxlength=2 class='valid nonvide' value="<?PHP echo $manuelNiveau;?>"></TD>
				</tr>
				
				<tr>
					<TD>Titre *</TD>
					<TD><input type=text name=titre id=titre class='valid nonvide' value="<?PHP echo $manuelTitre;?>"></TD>
				</tr>
				
				<tr>
					<TD>Editeur *</TD>
					<TD>
						<select name=editeur>
							<?PHP
								$editeurs = $cnx->QueryAll("SELECT editeurID, editeurNom FROM editeurs ORDER BY editeurNom");
							
								foreach ($editeurs as $editeur) {
								
									$editeurID = $editeur["editeurID"];
									$editeurNom = $editeur["editeurNom"];
									
									$select_editeur = $editeurID==$manuelEditeurID ? " selected" : "";
									
									echo "<option value='$editeurID' $select_editeur>$editeurNom</option>";
								}
							?>
						</select>
					</TD>
				</tr>	

				<tr>
					<TD>Edition *</TD>
					<TD><input type=text name=edition id=edition class='valid nonvide' size=4 maxlength=4 value="<?PHP echo $manuelEdition;?>"></TD>
				</tr>
				
				<tr>
					<TD>Matière *</TD>
					<TD>
						<select name=matiere>
							<?PHP
								$matieres = $cnx->QueryAll("SELECT matiereID, matiereNom FROM matieres ORDER BY matiereNom");
							
								foreach ($matieres as $matiere) {
									$matiereID = $matiere["matiereID"];
									$matiereNom = $matiere["matiereNom"];
									
									$select_matiere = $matiereID==$manuelMatiereID ? " selected" : "";
									
									echo "<option value='$matiereID' $select_matiere>$matiereNom</option>";
								}
							?>
						</select>
					</TD>
				</tr>	
						
			</table>
			
			<br>
			<input type=submit value='Modifier MANUEL' id="post_form">

			</center>

		</FORM>
		
		<?PHP

	}

	
	#***************************************************************************
	# 				@@ SUPPRESSION
	#***************************************************************************
	
	if ($action == 'del') {

		$id = $_GET['id'];
		$manuel = $cnx->QueryRow ( "SELECT manuelNiveau, manuelREF, manuelTitre, manuelEdition, matiereNom, editeurNom FROM manuels, matieres, editeurs WHERE manuels.matiereID=matieres.matiereID AND manuels.editeurID=editeurs.editeurID AND manuelID=$id" );
		
		$manuelREF 		= $manuel["manuelREF"];
		$manuelNiveau 	= $manuel["manuelNiveau"];
		$manuelTitre 	= $manuel["manuelTitre"];
		$manuelEdition 	= $manuel["manuelEdition"];
		$matiereNom 	= $manuel["matiereNom"];
		$editeurNom 	= $manuel["editeurNom"];
	
		$exemplaires = $cnx->QueryOne ( "SELECT COUNT(exeID) FROM exemplaires WHERE manuelID=$id" );

		if ($exemplaires > 0) {echo "<h3>Vous ne pouvez pas supprimer le manuel <u>$editeurNom $manuelTitre</u> : <br>Des exemplaires sont associés à ce manuel !</h3>"; exit();}

		echo "Voulez vous vraiment supprimer le manuel $editeurNom $manuelTitre ?";
	?>	
		<center><br><br>
		<form action="manuels_post.php?action=del" method="post" name="post_form" id='formulaire'>
			<input type=hidden value="<?PHP echo $id;?>" name="id">
			<input type=submit value='Supprimer' id="post_form">
			<input type=button onclick="$('#dialog').dialog('close');" value='Annuler'>
		</form>
		</center>
		
	<?PHP	
	}
	
	
?>		
