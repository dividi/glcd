<?PHP
	#formulaire d'ajout et de modification
	#des exemplaires !
	
	// lib
	include ('config.php');	// fichiers de configuration des bases de données
	require_once ('fonctions.php');
	include_once ('class/Log.class.php');
	include_once ('class/Sql.class.php');

	// connexion à la base de données
	$cnx 	= new Sql ($host, $user,$pass, $db);

?>



<script type="text/javascript"> 

	

	// **************************************************************** FILTRE des EAN13 (Ajout de masse)
	function filterEAN13 (manREF){
		
		var ref = $(manREF).val();
		var nb = $("#manNB").val();
	
		if (ref.length == 13) {
	
			for ( i=0 ; i<nb ; i++ ) {
					
				if ( $('#DIV' + ref) ) {$('#' + ref).parent().empty();}
							
				$.post("exemplaires_search.php?type=multi", {
					ref: ref,
				}, function(response){			
					$('#exemplaires_liste').append("<tr id='DIV" + ref + "'>" + response + "</tr>");
				});


				$('#manREF').val("").focus();
				
			}
		}
	}	

	

	// **************************************************************** FILTRE des EAN13 (Ajout unique)
	function filterEAN13unique (manREF){
		
		var ref = $(manREF).val();
	
		if (ref.length == 13) {
			
			//if ( $('#DIV' + ref) ) {$('#' + ref).parent().empty();}
						
			$.post("exemplaires_search.php?type=unique", {
				ref: ref,
			}, function(response){			
				$('#exemplaires_liste').html("<table class='table table-condensed' id='DIV" + ref + "'>" + response + "</table>");
			});
		}
	}		
		
	// **************************************************************** SUPPRIME une LIGNE
	function deleteEXE (ref){
		$('#DIV' + ref).empty();
	}	
	
		

	$(function() {	
				
		// **************************************************************** POST AJAX FORMULAIRES
		$("#post_form").click(function(event) {

			/* stop form from submitting normally */
			event.preventDefault(); 
			
			if ( validForm() == true) {
			
				// Permet d'avoir les données à envoyer
				var dataString = $("#formulaire").serialize();
				
				// action du formulaire
				var url = $("#formulaire").attr( 'action' );
				
				var request = $.ajax({
					type: "POST",
					url: url,
					data: dataString,
					dataType: "html"
				 });
				 
				request.done(function(msg) {
					$('#targetback').show(); $('#target').show();
					$('#target').html(msg);
					if ($('#dialog')) $('#dialog').dialog('close');
					window.setTimeout("document.location.href='index.php?page=exemplaires'", 3000);
				});
			}			 
		});


		
		// **************************************************************** Sur entrée dans la ref manuel, dans les traitement par lot
		$(".ref,#lot_prix,#lot_achat").on ('keydown', function(event){	
			if (event.which == 13) {
				event.preventDefault();
				$(this).blur();
			}
		});	
		
		// Fait apparaitre le traitement par lot
		$('#bt_lot').click(function(){				
			$('#div_lot').toggle();
		});
		
		// Changement du prix par lot
		$('#lot_prix').on('change', function(){				
			$('.prix').val($(this).val());
		});
		
		// Changement de la date d'achat par lot
		$('#lot_achat').on('change', function(){				
			$('.achat').val($(this).val());
		});
		
		// Changement de la présence d'un cd par lot
		$('#lot_cd').on('change', function(){				
			$('.cd').val($(this).val());
		});
				
		// Changement de la date d'achat par lot
		$('#lot_etat').on('change', function(){				
			$('.etat option[value='+$(this).val()+']').prop('selected', true)
		});		
	});
	

	
</script>

<?PHP

	$action = $_GET['action'];


	#***************************************************************************
	# 				@@ CREATION
	#***************************************************************************
	
	if ( $action == 'add' ) {	// Formulaire vierge de création

		?>
		
		
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
			<div class="container-fluid">
				
				<!--<div class="navbar-title">AJOUT DES EXEMPLAIRES</div>-->
				
				<!-- Left part -->

				<ul class="nav navbar-nav">
					<li><a class="navbar-brand" href="#"></a></li>
					<li class="navbar-title">AJOUTER des EXEMPLAIRES</li>
				</ul>

				<!-- Right part -->
				<ul class="nav navbar-nav navbar-right">
					<!-- Help -->
					<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Cette page permet d'ajouter des exemplaires de manuels. Le traitement par lot permet d'appliquer à TOUS les exemplaires un prix, une date d'achat et un état."><span class="glyphicon glyphicon-info-sign"></span> Info</a></li>
				</ul>
				
			</div>
		</nav>

		<div class="spacer"></div>	
			
		<center>
		
			<form method=post action="exemplaires_post.php?action=add" id='formulaire'>
			
				<br><br>

				nombre : <input type=number size=2 maxlength=2 class='ref' id='manNB' value=1 min=1 max=150> &nbsp;
				isbn : <input type=text size=14 maxlength=13 class='ref' id='manREF' onchange="filterEAN13($(this));"> <span id='msg'></span>		

				<br><br>
				
				<!-- Modification par lot des états, prix et dates d'achat -->
				
				<a href='#' id='bt_lot' title='Applique une valeur à tous les exemplaires'>traitement par lot</a><br>
				<div id='div_lot' style='display:none;border:1px solid grey;width:240px;text-align:left;padding:3px;margin-top:3px;'>
					<table class='formtable'>
						<tr>
							<td>date d'achat</td>
							<td><input type=text size=4 maxlength=4 id='lot_achat'></td>
						</tr>
						<tr>
							<td>prix</td>
							<td><input type=text size=5 maxlength=5 id='lot_prix'></td>
						</tr>
						<tr>
							<td>cd</td>
							<td><select name='cd[]' id='lot_cd'>
								<option value=1>OUI</option>
								<option value=0>NON</option>
								</select></td>
						</tr>
						<tr>
							<td>état</td>
							<td><select name='etat[]' id='lot_etat'>
							<option value=4>4 - NEUF</option>
							<option value=3>3 - BON</option>
							<option value=2>2 - MOYEN</option>
							<option value=1>1 - PASSABLE</option>
						</select></td>
						</tr>						
					</table>
				</div>
				
				<br>				
								
				<input type=submit value="AJOUTER les EXEMPLAIRES" id="post_form"><br><br>		
				
				<br><br>
				
				<table class='table alternate table-condensed' id='exemplaires_liste'>
					<th>REF</th>
					<th>Matière</th>
					<th>Editeur</th>
					<th>Niveau</th>
					<th>Titre</th>
					<th>Edition</th>
					<th>CD</th>
					<th>Date<br>d'achat</th>
					<th>Prix</th>
					<th>Etat</th>
					<th>&nbsp;</th>
					
					<!-- Ajax ajoute les lignes ici via le fichier exemplaires_search.php -->
				</table>
				
			</form>
		
		</center>
		
		
		<script>
			$('#manREF').focus();
		</script>
				

		<?PHP
		
	} 
	
	
	#***************************************************************************
	# 				@@ CREATION UNIQUE
	#***************************************************************************
	
	if ( $action == 'create' ) {	// Formulaire vierge de création unique

?>
	
		<center>
		
			<form method=post action="exemplaires_post.php?action=create" id='formulaire'>
			
				<br>

				ISBN : <input type=text size=14 maxlength=13 class='ref' id='manREF' onchange="filterEAN13unique($(this));"> <span id='msg'></span>		

				<br><br>
				
				<!-- Ajax ajoute les lignes ici via le fichier exemplaires_search.php -->
				<div id='exemplaires_liste'></div>
				
				<br><br>
												
				<input type=submit value="AJOUTER Exemplaire" id="post_form"><br><br>		
				
				
			</form>

<?PHP
	}

	#***************************************************************************
	# 				@@ SUPPRESSION
	#***************************************************************************
	
	if ($action == 'del') {

		$id = $_GET['id'];
		$exeREF = $cnx->QueryOne ( "SELECT exeREF FROM exemplaires WHERE exeID=$id" );

		$nb_prets = $cnx->QueryOne ( "SELECT COUNT(pretID) FROM prets WHERE exeID=$id" );

		if ($nb_prets > 0) {echo "<h3>Vous ne pouvez pas supprimer l'exemplaire <u>$exeREF</u> : <br>Ce manuel est prêté !</h3>"; exit();}

		echo "Voulez vous vraiment supprimer l'exemplaire $exeREF ?";
	?>	
		<center><br><br>
		<form method=post action="exemplaires_post.php?action=del" id='formulaire'>
			<input type=hidden value="<?PHP echo $id;?>" name="id">
			
			<input type=submit value="Supprimer" id="post_form">
			<!--<input type=button id="post_form">-->
			
			<input type=button onclick="$('#dialog').dialog('close');" value='Annuler'>
		</form>
		</center>
		
	<?PHP	
	}
	
	
	#***************************************************************************
	# 				@@ SUPPRESSION LOT
	#***************************************************************************
	
	if ($action == 'dellot') {

		echo "Voulez vous vraiment supprimer les exemplaires sélectionnés ?<br><br>Ne seront supprimés que les exemplaires <b>NON PRETES</b> !";
	?>	
		<center><br><br>
		
		<script>$('#lotid').val($('#id_a_poster').val())</script>
		
		<form method=post action="exemplaires_post.php?action=dellot" id='formulaire'>
			<input type=hidden value="" id="lotid" name="lotid">
			
			<input type=submit value="Supprimer le lot" id="post_form">
			
			<input type=button onclick="$('#dialog').dialog('close');" value='Annuler'>
		</form>
		</center>
		
	<?PHP	
	}
	
	
	#***************************************************************************
	# 				@@ MODIFIER LOT
	#***************************************************************************
	
	if ($action == 'modlot') {

		echo "Laisser vide les champs à ne pas modifier !";
	?>	
		<center><br><br>
		
		<script>$('#lotid').val($('#id_a_poster').val())</script>
		
		<form method=post action="exemplaires_post.php?action=modlot" id='formulaire'>
			<input type=hidden value="" id="lotid" name="lotid">
			
			<table class='formtable'>
				<tr>
					<td>date d'achat</td>
					<td><input type=text size=4 maxlength=4 name='lot_achat'></td>
				</tr>
				<tr>
					<td>CD</td>
					<td><select name='lot_cd'>
					<option value=''></option>
					<option value=1>OUI</option>
					<option value=0>NON</option>
				</select></td>
				</tr>
				<tr>
					<td>prix</td>
					<td><input type=text size=5 maxlength=5 name='lot_prix'></td>
				</tr>
				<tr>
					<td>état</td>
					<td><select name='lot_etat'>
					<option value=''></option>
					<option value=4>4 - NEUF</option>
					<option value=3>3 - BON</option>
					<option value=2>2 - MOYEN</option>
					<option value=1>1 - PASSABLE</option>
				</select></td>
				</tr>	
				<tr>
					<td>FLAG</td>
					<td><select name='lot_flag'>
					<option value=''></option>
					<option value=1>OUI</option>
					<option value=0>NON</option>
				</select></td>
				</tr>				
			</table>
			
			<br>
			
			<input type=submit value="Modifier le lot" id="post_form">
			
			<input type=button onclick="$('#dialog').dialog('close');" value='Annuler'>
		</form>
		</center>
		
	<?PHP	
	}
		
	
	#***************************************************************************
	# 				@@ MODIFIER
	#***************************************************************************
	
	if ($action == 'mod') {
		
		$id = $_GET['id'];
				
		// Requete pour récupérer les données des champs à modifier
		$exemplaire = $cnx->QueryRow ( "SELECT exePrix, exeAchat, exeEtat, exeCD FROM exemplaires WHERE exeID=$id" );		
		
		// valeurs à affecter aux champs
		$exePrix	= $exemplaire['exePrix'];
		$exeAchat	= $exemplaire['exeAchat'];
		$exeEtat	= $exemplaire['exeEtat'];
		$exeCD		= $exemplaire['exeCD'];
	
	?>	
		<center>
		
		<form method=post action="exemplaires_post.php?action=mod" id='formulaire'>
			<input type=hidden id="id" name="id" value="<?PHP echo $id;?>">
			
			<table class='formtable'>
				<tr>
					<td>date d'achat</td>
					<td><input type=text size=4 maxlength=4 name='achat' value="<?PHP echo $exeAchat;?>"></td>
				</tr>
				<tr>
					<td>prix</td>
					<td><input type=text size=5 maxlength=5 name='prix' value="<?PHP echo $exePrix;?>"></td>
				</tr>
				<tr>
					<td>CD</td>
					<td>
						<?PHP	
						echo "<select name='cd' class='cd'>";
							echo "<option value=1"; if ($exeCD == 1) echo " selected"; echo ">OUI</option>";
							echo "<option value=0"; if ($exeCD == 0) echo " selected"; echo ">NON</option>";
						echo "</select>";
						?>
					</td>
				</tr>
				<tr>
					<td>état</td>
					<td>
						<?PHP	
						echo "<select name='etat' class='etat'>";
							echo "<option value=4"; if ($exeEtat == 4) echo " selected"; echo ">4 - NEUF</option>";
							echo "<option value=3"; if ($exeEtat == 3) echo " selected"; echo ">3 - BON</option>";
							echo "<option value=2"; if ($exeEtat == 2) echo " selected"; echo ">2 - MOYEN</option>";
							echo "<option value=1"; if ($exeEtat == 1) echo " selected"; echo ">1 - PASSABLE</option>";
						echo "</select>";
						?>
					</td>
				</tr>	
				<tr>
					<td>FLAG</td>
					<td>
						<?PHP	
						echo "<select name='flag' class='flag'>";
							echo "<option value=1"; if ($exeCD == 1) echo " selected"; echo ">OUI</option>";
							echo "<option value=0"; if ($exeCD == 0) echo " selected"; echo ">NON</option>";
						echo "</select>";
						?>
					</td>
				</tr>					
			</table>
			
			<br>
			
			<input type=submit value="Modifier" id="post_form">
			
			<input type=button onclick="$('#dialog').dialog('close');" value='Annuler'>
		</form>
		</center>	

	<?PHP
	}
	?>		
