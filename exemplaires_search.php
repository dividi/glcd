<?PHP


	// fichier de creation / modif / suppr d'un éditeur
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');	

if($_REQUEST) {

	// cnx
	$cnx = new Sql($host, $user, $pass, $db);

	$ref 	= $_REQUEST['ref'];

	$manuel = $cnx->QueryRow ( "SELECT manuelID, manuelREF, manuelNiveau, manuelTitre, manuelEdition, matiereNom, editeurNom, manuels.matiereID, manuels.editeurID FROM manuels, matieres, editeurs WHERE manuels.matiereID=matieres.matiereID AND manuels.editeurID=editeurs.editeurID AND	manuelREF = '$ref'" );
	
	if ($manuel) {
		
		$type = $_GET["type"];
		$manuelID = $manuel['manuelID'];
		$manuelREF = $manuel['manuelREF'];
		$manuelNiveau = $manuel['manuelNiveau'];
		$manuelTitre = $manuel['manuelTitre'];
		$manuelEdition = $manuel['manuelEdition'];
		$editeurNom = $manuel['editeurNom'];
		$matiereNom = $manuel['matiereNom'];

		if ($type == "multi") {
			echo "<td>$ref <input type=hidden value='$manuelID' name='manuelID[]'> </td>";
			echo "<td>$matiereNom</td>";
			echo "<td>$editeurNom</td>";
			echo "<td>$manuelNiveau</td>";
			echo "<td>$manuelTitre</td>";
			echo "<td>$manuelEdition</td>";
			echo "<td><select name='cd[]' class='cd'>
				<option value=1>OUI</option>
				<option value=0>NON</option>
				</select></td>";
			echo "<td><input type=text maxlength=4 size=4 name='achat[]' class='achat'></td>";
			echo "<td><input type=text maxlength=5 size=5 name='prix[]' class='prix'></td>";
			echo "<td><select name='etat[]' class='etat'>
				<option value=4>4 - NEUF</option>
				<option value=3>3 - BON</option>
				<option value=2>2 - MOYEN</option>
				<option value=1>1 - PASSABLE</option>
			</select></td>";
			echo "<td><a href='#' class='delete' onclick='deleteEXE(\"$ref\");'><span class='glyphicon table-icon glyphicon-remove color-red'></a></td>";
		
			echo "<script> $('#msg').text('');</script>";
		}
		
		if ($type == "unique") {
			
			$manuelInfo = $cnx->QueryRow ("SELECT manuelNiveau, manuelTitre, manuelEdition, matiereREF, editeurREF FROM manuels, matieres, editeurs WHERE manuels.matiereID=matieres.matiereID AND manuels.editeurID=editeurs.editeurID AND manuelID= ".$manuelID."; ");
			$prefixe = $manuelInfo['manuelNiveau'] . $manuelInfo['matiereREF'] . $manuelInfo['editeurREF'] . substr($manuelInfo['manuelEdition'], -2);
					
			
			echo "<tr><td>ISBN</td><td>$ref <input type=hidden value='$manuelID' name='manuelID'></td></tr>";
			echo "<tr><td>Matière</td><td>$matiereNom</td></tr>";
			echo "<tr><td>Editeur</td><td>$editeurNom</td></tr>";
			echo "<tr><td>Niveau</td><td>$manuelNiveau</td></tr>";
			echo "<tr><td>Titre</td><td>$manuelTitre</td></tr>";
			echo "<tr><td>Edition</td><td>$manuelEdition</td></tr>";
			echo "<tr><td>CD</td><td><select name='cd' class='cd'>
				<option value=1>OUI</option>
				<option value=0>NON</option>
				</select></td></tr>";
			echo "<tr><td>Date Achat</td><td><input class='valid nonvide' type=text maxlength=4 size=4 name='achat' class='achat'></td></tr>";
			echo "<tr><td>Prix</td><td><input class='valid nonvide' type=text maxlength=5 size=5 name='prix' class='prix'></td></tr>";
			echo "<tr><td>Etat</td><td><select name='etat' class='etat'>
				<option value=4>4 - NEUF</option>
				<option value=3>3 - BON</option>
				<option value=2>2 - MOYEN</option>
				<option value=1>1 - PASSABLE</option>
			</select></td></tr>";
			echo "<tr><td>ExeREF</td><td><input type='text' name='prefixe' readonly value='$prefixe' size='4'><input class='valid nonvide num' name='suffixe' type='text' maxlength='6' size='6' placeholder='suffixe'></td></tr>";
		
			echo "<script> $('#msg').text('');</script>";
		}
	
	}
	
	else {
		echo "<script>$('#msg').text('ISBN inconnu !');</script>";
	}
}
?>
