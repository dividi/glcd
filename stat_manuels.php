<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="js/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="js/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="js/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="js/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="js/jqplot.pointLabels.min.js"></script>

<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />

<style>
	.piechart{width:750px; height:700px;}
	.bt{cursor:pointer;display:inline;}

	#contenu{}
	
</style>


<?PHP
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');		
	
	
	
	// Cnx à  la base
	$cnx = new Sql($host, $user, $pass, $db);

?>
	
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
		<div class="container-fluid">
					
		<!-- Left part -->
		<ul class="nav navbar-nav">
			<li><a class="navbar-brand" href="#"></a></li>	
			<li class="navbar-title">STATISTIQUES et GRAPH</li>
		</ul>


		<!-- Right part -->
		<ul class="nav navbar-nav navbar-right">

			<!-- Help -->
			<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Cette page permet l'affichage les statistiques de répartition par des tas de trucs en fonction des machins."><span class="glyphicon glyphicon-info-sign"></span> Info </a></li>
			
		</ul>
		</div>
	</nav>

	<div class="spacer"></div>	




<div id="bt_matieres" class="bt">[MATIERES]</div>
<div id="bt_niveaux" class="bt">[NIVEAUX]</div>
<div id="bt_editeurs" class="bt">[EDITEURS]</div>
<div id="bt_stock" class="bt">[STOCK]</div>

<p>

<?PHP

	/*************************************
			REPARTITION PAR MATIERES !
	*************************************/

	$liste = $cnx->QueryAll ( "SELECT matiereNom, count( matieres.matiereID ) as compte FROM manuels, matieres WHERE manuels.matiereID=matieres.matiereID GROUP BY matieres.matiereID" );

	foreach ( $liste as $record) {
		
		$key = $record["matiereNom"];
		$value = $record["compte"];
		
		$data_matieres .= "['$key', $value],";

	}
	// On vire la dernière virgule
	$data_matieres = preg_replace("[,$]", "", $data_matieres);
	
	
	
	/*************************************
			REPARTITION PAR NIVEAUX !
	*************************************/

	$liste = $cnx->QueryAll ( "SELECT manuelNiveau, count( manuelNiveau ) as compte FROM manuels GROUP BY manuelNiveau" );

	foreach ( $liste as $record) {
		
		$key = $record["manuelNiveau"];
		$value = $record["compte"];
		
		$data_niveaux .= "['$key', $value],";

	}
	// On vire la dernière virgule
	$data_niveaux = preg_replace("[,$]", "", $data_niveaux);

	
	
	/*************************************
			REPARTITION PAR EDITEURS !
	*************************************/

	$liste = $cnx->QueryAll ( "SELECT editeurNom, count( manuels.editeurID ) as compte FROM manuels, editeurs WHERE manuels.editeurID=editeurs.editeurID GROUP BY manuels.editeurID" );

	foreach ( $liste as $record) {
		
		$key = $record["editeurNom"];
		$value = $record["compte"];
		
		$data_editeurs .= "['$key', $value],";

	}
	// On vire la dernière virgule
	$data_editeurs = preg_replace("[,$]", "", $data_editeurs);

	
	
	/*************************************
			REPARTITION DES STOCKS !
	*************************************/

	$liste = $cnx->QueryAll ( "SELECT manuels.manuelID, count( exemplaires.manuelID ) as total FROM manuels, exemplaires WHERE manuels.manuelID=exemplaires.manuelID GROUP BY manuels.manuelID" );
	
	foreach ( $liste as $record) {
		
		$manuelID = $record["manuelID"];
		$total = $record["total"];
		
		$manuel = $cnx->QueryRow ("SELECT manuelTitre, manuelNiveau, manuelEdition, matiereREF, editeurREF FROM manuels, matieres, editeurs WHERE manuels.matiereID=matieres.matiereID AND manuels.editeurID=editeurs.editeurID AND manuels.manuelID=$manuelID");
		$titre = addslashes($manuel['manuelTitre'] . " " . $manuel['matiereREF'] . $manuel['manuelNiveau']) . " [$total]";
		
		$nb_pret = $cnx->QueryOne ("SELECT COUNT(manuelID) as pret FROM exemplaires WHERE manuelID=$manuelID AND exeID IN (SELECT exeID FROM prets)");
		$nb_stock = intval($total) - intval($nb_pret);
		
		$nb_perdu = $cnx->QueryOne ("SELECT COUNT(manuelID) as perdu FROM exemplaires WHERE manuelID=$manuelID AND exeRendu=0;");
		
		$data_pret .= "$nb_pret,";
		$data_stock .= "$nb_stock,";
		$data_perdu .= "$nb_perdu,";
		$data_label .= "'$titre',";

	}
	// On vire la dernière virgule
	$data_pret = preg_replace("[,$]", "", $data_pret);		
	$data_stock = preg_replace("[,$]", "", $data_stock);		
	$data_label = preg_replace("[,$]", "", $data_label);		
	$data_perdu = preg_replace("[,$]", "", $data_perdu);		

?>

<div id="pie-matieres" class="piechart"></div>
<div id="pie-niveaux" class="piechart"></div>
<div id="pie-editeurs" class="piechart"></div>
<div id="bar-stock" class="piechart"></div>




<script>
	$(document).ready(function(){
	

		var data_matieres = [<?PHP echo $data_matieres;?> ];
		var plotmatieres = jQuery.jqplot ('pie-matieres', [data_matieres], { 
			title: 'MANUELS par MATIERES',
			seriesDefaults: {
				renderer: jQuery.jqplot.PieRenderer, 
				rendererOptions: {
					showDataLabels: true
					}
				}, 
			legend: { show:true, location: 'ne', placement:"insideGrid" },
			highlighter: {
				show: true,
				formatString:'%s<br>%d manuel(s)', 
				tooltipLocation:'se', 
				useAxesFormatters:false,
				},
				grid:{borderColor:'transparent',shadow:false,drawBorder:false,shadowColor:'transparent'}
			}
		);

		  
		var data_niveaux = [<?PHP echo $data_niveaux;?> ];
		var plotniveaux = jQuery.jqplot ('pie-niveaux', [data_niveaux], 
		{ 
			title: 'MANUELS par NIVEAUX',
			seriesDefaults: {
				renderer: jQuery.jqplot.PieRenderer, 
				rendererOptions: {showDataLabels: true}
			}, 
				legend: { show:true, location: 'ne', placement:"insideGrid",formatString:'%d ème', },
				highlighter: {
					show: true,
					formatString:'%s ème<br>%d manuels(s)', 
					tooltipLocation:'se', 
					useAxesFormatters:false,
				},
				grid:{borderColor:'transparent',shadow:false,drawBorder:false,shadowColor:'transparent'}
			}
		);

		  
		var data_editeurs = [<?PHP echo $data_editeurs;?> ];
		var plotsalles = jQuery.jqplot ('pie-editeurs', [data_editeurs], 
		{ 
			title: 'MANUELS par EDITEURS',
			seriesDefaults: {
				renderer: jQuery.jqplot.PieRenderer, 
				rendererOptions: {showDataLabels: true}
			}, 
				legend: { show:true, location: 'ne', placement:"insideGrid" },
				highlighter: {
					show: true,
					formatString:'%s<br>%d manuels(s)', 
					tooltipLocation:'se', 
					useAxesFormatters:false,
				},
				grid:{borderColor:'transparent',shadow:false,drawBorder:false,shadowColor:'transparent'}
			}
		);	
 
		var pret = [<?PHP echo $data_pret;?> ];
		var perdu = [<?PHP echo $data_perdu;?> ];
		var stock = [<?PHP echo $data_stock;?> ];
		var label = [<?PHP echo $data_label;?> ];
		
		var plotstock = $.jqplot('bar-stock', [pret, perdu, stock], {
			title: 'MANUELS PRÊTÉS et en STOCKS',  
			stackSeries: true, 
			seriesDefaults: {
				renderer: $.jqplot.BarRenderer,
				shadow: false,
				rendererOptions:{
					barMargin: 1,
					barPadding:1,
					barDirection: 'horizontal', 
					barWidth:35,
					groups:1
				}, 
				pointLabels:{
					show:true, 
					stackedValue:false, 
					location :'w', 
					labelsFromSeries:true, 
					hideZeros:true
				}
			},
			axes: {
				xaxis:{
					min:0, 
					tickInterval:5, 
					tickOptions:{formatString:'%d'},
					label:'Nombre de manuels'
				},
				yaxis:{
					renderer:$.jqplot.CategoryAxisRenderer,
					ticks:[<?PHP echo $data_label;?>],
					tickOptions:{showGridline: false},
				}
			},
			legend: {
				show: true,
				location: 'ne',
				placement: 'insideGrid',
				labels :['prêté', 'perdu', 'stock']
			},
			highlighter: {
				show: true,
				formatString:'%d manuel(s)', 
				tooltipLocation:'se', 
				tooltipAxes :'x',
				bringSeriesToFront:true,
				yvalues:3,
			},
			grid:{
				borderColor:'transparent',
				shadow:false,
				drawBorder:false,
				shadowColor:'transparent'
			}			
		});
	  		
			

		$(".piechart").hide();
		$("#bar-stock").show();
	
		$('#bt_matieres').click(function(){
			$(".piechart").hide();
			$("#pie-matieres").toggle();
		});
		
		$('#bt_niveaux').click(function(){
			$(".piechart").hide();
			$("#pie-niveaux").toggle();
		});
		
		$('#bt_editeurs').click(function(){
			$(".piechart").hide();
			$("#pie-editeurs").toggle();
		});
		
		$('#bt_stock').click(function(){
			$(".piechart").hide();
			$("#bar-stock").toggle();
		});	
	});

</script>
