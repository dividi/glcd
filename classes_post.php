<?PHP


	// fichier de creation / modif / suppr d'un éditeur
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');		
	
	
	
	// Cnx à  la base
	$cnx = new Sql($host, $user, $pass, $db);
	
	// Les LOGS
	$logsql = new log ("fichiers/log_sql.sql");
	$log = new log ("fichiers/log.txt");
	
	// on récupère les paramètres de l'url	
	$action 	= $_GET['action'];
		

	/**************** @@SUPPRESSION ********************/
	
	if ( $action == 'del' ) {
		
		$id = $_POST['id'];
	    $classeNom = $cnx->QueryOne ( "SELECT CONCAT(classeNiveau,classeNom) FROM classes WHERE classeID=$id" );
	
		// Suppression de l'utilisateur de la base
		$req_suppr = "DELETE FROM classes WHERE classeID=$id;";
		$cnx->Execute ( $req_suppr );
		$logsql->Insert( $req_suppr );
		
		echo "La CLASSE <b>$classeNom</b> a été supprimée.";	
		$log->Insert( "La CLASSE $classeNom a été supprimée." );		
	  
	}

	/**************** @@MODIFICATION ********************/	
	if ( $action == 'mod' ) {
	
		$id     	= $_POST ['id'];
		$niveau    	= $_POST ['niveau'];
		$nom 		= $_POST ['nom'];
	
		// on récupére les anciennes valeurs du compte pour les logs
		$old_info = $cnx->QueryRow("SELECT classeNiveau, classeNom FROM classes WHERE classeID=$id");
		$OLDclasseNiveau 	= $old_info["classeNiveau"];
		$OLDclasseNom 		= $old_info["classeNom"];
		
		
		$req_modif = "UPDATE classes SET classeNiveau='$niveau', classeNom='$nom' WHERE classeID=$id";
		$cnx->Execute ( $req_modif );
		$logsql->Insert( $req_modif );
	
		echo "La CLASSE <b>$OLDclasseNiveau$OLDclasseNom</b> a été modifiée en <b>$niveau$nom</b>";
		$log->Insert( "La CLASSE $OLDclasseNiveau$OLDclasseNom a été modifiée en $niveau$nom" );
	}
	
	/**************** @@INSERTION ********************/
	if ( $action == 'add' ) {
	
		$nom 		= $_POST ['nom'];
		$niveau		= $_POST ['niveau'];
		
		$req_add = "INSERT INTO classes ( classeNiveau, classeNom ) VALUES ( '$niveau', '$nom')";
		$cnx->Execute ( $req_add );
		$logsql->Insert( $req_add );
		
		echo "La CLASSE <b>$niveau$nom</b> a été créée.";
		$log->Insert( "La CLASSE $niveau$nom a été créée." );
	}
	

?>


