<?PHP

/*

	Liste des exemplaires

*/
?>

<div class="entetes" id="entete-editeurs">	

	<span class="entetes-titre">STOCK des EXEMPLAIRES<img class="help-button" src="<?PHP echo "construction/information.png";?>"></span>
	<div class="helpbox">Cette page permet de gérer l'ajout, la modification et la suppression des exemplaires de manuels.</div>

	<span class="entetes-options">
	
		<span class="option"><?PHP
			echo "
			<form action='exemplaires_barcodes.php' target=_blank method='post' id='form_barcodes'>
				<input type=hidden name='id_a_poster' id='id_a_poster' value=''>	
				
				<span id='nb_selectionnes' title='nombre d‘exemplaires sélectionnées'></span>
				<a href='#' class='actionselect' onclick='$(\"#form_barcodes\").submit();'>CODES BARRES</a>	
				
			</form>";?>
		</span>
		
		<span class="option"><a href='exemplaires_form.php?action=dellot' class='editbox actionselect' title='SUPPRIMER la Sélection'>SUPPRIMER LOT</a></span>
		<span class="option"><a href='exemplaires_form.php?action=modlot' class='editbox actionselect' title='MODIFIER la Sélection'>MODIFIER LOT</a></span>
	
		<!--<span class="option"><a href='exemplaires_form.php?action=add' class='editbox' title='Ajouter des EXEMPLAIRES'><img src='construction/add.png'></a></span>-->
		<span class="option"><a href='index.php?page=exeAdd&action=add' title='Ajouter des EXEMPLAIRES'><img src='construction/add.png'></a></span>

		<span class="option">
			<!-- 	bouton pour le filtrage du tableau	-->
			<form id="filterform">
				<input placeholder=" filtrer" name="filt" id="filt" onKeyPress="return disableEnterKey(event)" onkeyup="filter(this.value, 'exemplaires_table');" type="text" value=<?PHP echo $_GET['filter'];?>> <span id="filtercount" title="Nombre de lignes filtrées"></span>
			</form>
		</span>
	</span>

</div>

<div class="spacer"></div>



<?PHP 

	// cnx à la base de données
	$cnx = new Sql ( $host, $user, $pass, $db );

	// stockage des lignes retournées par sql dans un tableau nommé liste_des_materiels
	$liste_des_exemplaires = $cnx->QueryAll ( "
		SELECT exeID, manuelNiveau, manuelTitre, manuelEdition, matiereNom, editeurNom, exeEtat, exeREF, exePrix, exeAchat
		FROM manuels, matieres, editeurs, exemplaires
		WHERE 
			manuels.matiereID=matieres.matiereID AND
			manuels.editeurID=editeurs.editeurID AND
			exemplaires.manuelID=manuels.manuelID
		ORDER BY matiereNom, manuelNiveau, editeurNom" );
	
?>
		
	<input type=hidden name='id_a_poster' id='id_a_poster' value=''>


	<center>

	<table class="bigtable hover" id="exemplaires_table">
		
		<th> <input type=checkbox id='checkall'> </th>
		<th>Matière</th>
		<th>Niveau</th>
		<th>Manuel</th>
		<th>Editeur</th>
		<th>Edition</th>
		<th>exeREF</th>
		<th>Prix</th>
		<th>Achat</th>
		<th>Etat</th>
		<th>prêté à</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
		
		<?PHP	
		
			foreach ( $liste_des_exemplaires as $record ) {
				
						
				$exeID			= $record['exeID'];
				$manuelNiveau	= $record['manuelNiveau'];
				$manuelTitre	= $record['manuelTitre'];
				$manuelEdition	= $record['manuelEdition'];
				$matiereNom		= $record['matiereNom'];
				$editeurNom		= $record['editeurNom'];
				$exeEtat		= $record['exeEtat'];
				$exeREF			= $record['exeREF'];
				$exePrix		= $record['exePrix'];
				$exeAchat		= $record['exeAchat'];
				$userNom		= $cnx->QueryOne ("SELECT CONCAT (userNom, ' ', userPrenom) FROM users, prets WHERE prets.userID=users.userID AND exeID=$exeID;");
				
				switch ($exeEtat) {
					case 4 : $exeEtatLabel = "NEUF"; $etatClass="neuf"; break;
					case 3 : $exeEtatLabel = "BON"; $etatClass="bon"; break;
					case 2 : $exeEtatLabel = "MOYEN"; $etatClass="moyen"; break;
					case 1 : $exeEtatLabel = "PASSABLE"; $etatClass="passable"; break;
					case 0 : $exeEtatLabel = "PERDU"; $etatClass="perdu"; break;
				}	
				
				echo "<tr id='tr_id$exeID' class='tr_modif'>";
					echo "<td> <input type=checkbox name=chk indexed=true value='$exeID' id='$exeID' class='chk_line'> </td>";	
					echo "<td> $matiereNom</td>";
					echo "<td> $manuelNiveau</td>";
					echo "<td> $manuelTitre</td>";
					echo "<td> $editeurNom</td>";
					echo "<td> $manuelEdition</td>";
					echo "<td> $exeREF </td>";
					echo "<td> $exePrix </td>";
					echo "<td> $exeAchat </td>";
					echo "<td class='$etatClass'> $exeEtatLabel </td>";
					
					echo "<td>$userNom</td>";
					
					echo "<td width=20 align=center><a href='exemplaires_form.php?id=$exeID&action=mod' class='editbox' title='Modifier'><img class='icon' src='construction/edit.png'> </a></td>";
					echo "<td width=20 align=center> <a href='exemplaires_form.php?action=del&id=$exeID' class='editbox' title='Supprimer'>	<img class='icon' src='construction/delete.png'>	</a> </td>";
					
				echo "</tr>";

			}
		?>		

	</table>
	
	<br>
	
	</center>
	
	
<?PHP
	// On se déconnecte de la db
	$cnx->Close();
		
?>

<script>
	// Filtre rémanent
	filter ( $('#filt').val(), 'exemplaires_table' );
	
	$(function(){
	
		//--------------------------------------- Selection d'une ligne	
		$('.chk_line').click(function(){
			
			var id = $(this).attr('id');
			
			if ( $(this).is(':checked') ){		
				$('#id_a_poster').val( $('#id_a_poster').val() + ";" + id );
				$("#tr_id" + id).addClass("selected");
			}
			else {
				$('#id_a_poster').val( $('#id_a_poster').val().replace(";" + id + ";", ";") );	// Supprime la valeur au milieu de la chaine
				var re = new RegExp(";" + id + "$", "g"); $('#id_a_poster').val( $('#id_a_poster').val().replace(re, "") );			// Supprime la valeur en fin de la chaine
				$("#tr_id" + id).removeClass("selected");
				$('#checkall').prop("checked", false);
			}
			
			// On affiche les boutons
			if ( $('#id_a_poster').val() != "" ) {				
				$('#nb_selectionnes').show(); $('.actionselect').show(); $('#nb_selectionnes').html( $('.chk_line:checked').length + ' sélectionné(s)');
			} else { 
				$('#nb_selectionnes').hide(); $('.actionselect').hide();
			}
			
		});
		
		
		
		//--------------------------------------- Selection de toutes les lignes
		$('#checkall').click(function(){
			
			if ( $('#checkall').is(':checked') ){		
				
				$('.chk_line:visible').prop("checked", true);	// On coche toutes les cases visibles

				$('#id_a_poster').val("");	// On vide les matos à poster
				$('.chk_line:visible').each (function(){$('#id_a_poster').val( $('#id_a_poster').val() + ";" + $(this).attr('id') );	});	// On alimente le input à poster
				
				$('.actionselect').show();		// On fait apparaitre les boutons
				$('#nb_selectionnes').show(); $('#nb_selectionnes').html( $('.chk_line:checked').length + ' sélectionné(s)');
				$('.tr_modif:visible').addClass("selected");	// On colorie toutes les lignes	visibles
			}
			else {
				$('#id_a_poster').val("");	// On vide les matos à poster
				$('.chk_line').prop("checked", false);	// On décoche toutes les cases
				$('.tr_modif').removeClass("selected");	// On vire le coloriage de toutes les lignes	
				$('.actionselect').hide(); $('#nb_selectionnes').hide();
			}			
		});	
		
		
		// **************************************************************** POST AJAX FORMULAIRES
		$("#post_form").click(function(event) {

			/* stop form from submitting normally */
			event.preventDefault(); 
			
			if ( validForm() == true) {
			
				// Permet d'avoir les données à envoyer
				var dataString = $("#formulaire").serialize();
				
				// action du formulaire
				var url = $("#formulaire").attr( 'action' );
				
				var request = $.ajax({
					type: "POST",
					url: url,
					data: dataString,
					dataType: "html"
				 });
				 
				request.done(function(msg) {
					$('#targetback').show(); $('#target').show();
					$('#target').html(msg);
					window.setTimeout("document.location.href='index.php?page=exemplaires'", 3000);
				});
			}			 
		});	
		
	});

	

	
</script>
