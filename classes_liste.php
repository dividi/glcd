<!--

	Liste des classes

-->


		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
			<div class="container-fluid">
						
			<!-- Left part -->
			<ul class="nav navbar-nav">
				<li><a class="navbar-brand" href="#"></a></li>	
				<li class="navbar-title">CLASSES</li>
			</ul>


			<!-- Right part -->
			<ul class="nav navbar-nav navbar-right">

				<!-- Add item -->
				<li><a href='classes_form.php?action=add' class='editbox' data-tip="Ajouter une CLASSE" title='Ajouter une CLASSE'><span class="glyphicon glyphicon-plus-sign"></span> Ajout</a></li>
				
				<!-- Help -->
				<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Cette page permet de gérer l'ajout, la modification et la suppression des classes."><span class="glyphicon glyphicon-info-sign"></span> Info </a></li>
				
				<!-- Filter -->
				<li data-tip="Filtrer le tableau">
					<form class="navbar-form" role="search">
						<div class="form-group">
							<input class="form-control" placeholder=" filtrer" name="filt" id="filt" onKeyPress="return disableEnterKey(event)" onkeyup="filter(this.value, 'classes_table');" type="text" value=<?PHP echo $_GET['filter'];?>> 
							&nbsp;<span class="navbar-text navbar-right" id="filtercount" data-tip="Nombre de lignes filtrées" title="Nombre de lignes filtrées"></span>
						</div>
					</form>					
				</li>

			</ul>
			</div>
		</nav>

	<div class="spacer"></div>	



<?PHP 

	// cnx à la base de données
	$cnx = new Sql ( $host, $user, $pass, $db );

	// stockage des lignes retournées par sql dans un tableau nommé liste_des_materiels
	$liste_des_classes = $cnx->QueryAll ( "SELECT classeID, classeNiveau, classeNom FROM classes ORDER BY classeNom" );

?>
		
	<input type=hidden name='id_a_poster' id='id_a_poster' value=''>


	<center>
<div class="table-responsive">
	<table class="table table-condensed hover" id="classes_table">
		<th>Classe</th>
		<th>Effectifs</th>
		<th>Manuels</th>
		<th>Exemplaires</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
		
		<?PHP	
		
			foreach ( $liste_des_classes as $record ) {
				
						
				$classeID		= $record['classeID'];
				$classeNiveau	= $record['classeNiveau'];
				$classeNom		= $record['classeNom'];
				
				$effectifs = $cnx->QueryOne("SELECT count(userID) FROM users WHERE classeID=$classeID");
				$nb_manuels = $cnx->QueryOne("SELECT count(manuelID) FROM manuels WHERE manuelNiveau=$classeNiveau");
				$nb_exe = $cnx->QueryOne("SELECT count(exeID) FROM manuels, exemplaires WHERE exemplaires.manuelID=manuels.manuelID AND manuelNiveau=$classeNiveau");
				
				echo "<tr>";
					echo "<td> $classeNiveau$classeNom</td>";
					
					echo "<td> $effectifs </td>";
					echo "<td> $nb_manuels </td>";
					echo "<td> $nb_exe </td>";
					
					echo "<td width=20 align=center><a href='classes_form.php?id=$classeID&action=mod' class='editbox' title='Modifier'><span class='glyphicon glyphicon-edit table-icon'></span></a></td>";
					echo "<td width=20 align=center> <a href='classes_form.php?action=del&id=$classeID' class='editbox' title='Supprimer'><span class='glyphicon table-icon glyphicon-remove color-red'></span></a> </td>";
					
				echo "</tr>";

			}
		?>		

	</table>
</div>	
	<br>
	
	</center>
	
	
<?PHP
	// On se déconnecte de la db
	$cnx->Close();
?>

<script>
	// Filtre rémanent
	filter ( $('#filt').val(), 'classes_table' );
</script>
