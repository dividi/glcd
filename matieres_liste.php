<!--

	Liste des matieres

-->

		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
			<div class="container-fluid">

			<!-- Left part -->
			<ul class="nav navbar-nav">
				<li><a class="navbar-brand" href="#"></a></li>	
				<li class="navbar-title">MATIERES</li>
			</ul>


			<!-- Right part -->
			<ul class="nav navbar-nav navbar-right">

				<!-- Add item -->
				<li data-toggle="tooltip" data-title="Ajouter une MATIERE"><a href='matieres_form.php?action=add' class='editbox' title='Ajouter une MATIERE'><span class="glyphicon glyphicon-plus-sign"></span> Ajout</a></li>
				
				<!-- Help -->
				<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Cette page permet de gérer l'ajout, la modification et la suppression des matières."><span class="glyphicon glyphicon-info-sign"></span> Info </a></li>
				
				<!-- Filter -->
				<li data-toggle="tooltip-filtre">
					<form class="navbar-form" role="search">
						<div class="form-group">
							<input class="form-control" placeholder=" filtrer" name="filt" id="filt" onKeyPress="return disableEnterKey(event)" onkeyup="filter(this.value, 'matieres_table');" type="text" value=<?PHP echo $_GET['filter'];?>> 
							&nbsp;<span class="navbar-text navbar-right" id="filtercount" title="Nombre de lignes filtrées"></span>
						</div>
					</form>					
				</li>

			</ul>
			</div>
		</nav>

	<div class="spacer"></div>	



<?PHP 

	// cnx à la base de données
	$cnx = new Sql ( $host, $user, $pass, $db );

	// stockage des lignes retournées par sql dans un tableau nommé liste_des_materiels
	$liste_des_matieres = $cnx->QueryAll ( "SELECT matiereID, matiereREF, matiereNom FROM matieres ORDER BY matiereNom" );

?>
		
	<input type=hidden name='id_a_poster' id='id_a_poster' value=''>


	<center>

	<div class="table-responsive">	
	
	<table class="table hover table-condensed" id="matieres_table">
		<th>REF</th>
		<th>Nom</th>
		<th>Editeurs</th>
		<th>Manuels</th>
		<th>Exemplaires</th>
		<th>Prêts</th>
		<th>Stock</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
		
		<?PHP	
		
			foreach ( $liste_des_matieres as $record ) {
				
						
				$matiereID		= $record['matiereID'];
				$matiereREF		= $record['matiereREF'];
				$matiereNom		= $record['matiereNom'];
				
				$nb_editeurs = $cnx->QueryOne ("SELECT COUNT(editeurID) FROM manuels WHERE matiereID=$matiereID");
				$nb_manuels = $cnx->QueryOne ("SELECT COUNT(manuelID) FROM manuels WHERE matiereID=$matiereID");
				$nb_exemplaires = $cnx->QueryOne ("SELECT COUNT(exeID) FROM exemplaires, manuels WHERE exemplaires.manuelID=manuels.manuelID AND matiereID=$matiereID");
				$nb_prets = $cnx->QueryOne ("SELECT COUNT(prets.exeID) FROM prets, exemplaires, manuels WHERE prets.exeID=exemplaires.exeID AND exemplaires.manuelID=manuels.manuelID AND matiereID=$matiereID");
				$nb_stock = $nb_exemplaires - $nb_prets;
				
				echo "<tr>";
					echo "<td> $matiereREF</td>";
					echo "<td> $matiereNom </td>";
					
					echo "<td> $nb_editeurs </td>";
					echo "<td> $nb_manuels </td>";
					echo "<td> $nb_exemplaires </td>";
					echo "<td> $nb_prets </td>";
					echo "<td> $nb_stock </td>";
					
					echo "<td width=20 align=center><a href='matieres_form.php?id=$matiereID&action=mod' class='editbox' title='Modifier'><span class='glyphicon glyphicon-edit table-icon'></span></a></td>";
					echo "<td width=20 align=center> <a href='matieres_form.php?action=del&id=$matiereID' class='editbox' title='Supprimer'><span class='glyphicon table-icon glyphicon-remove color-red'></span></a> </td>";
					
				echo "</tr>";

			}
		?>		

	</table>
	
	</div>
	
	<br>
	
	</center>
	
	
<?PHP
	// On se déconnecte de la db
	$cnx->Close();
?>

<script>
	// Filtre rémanent
	filter ( $('#filt').val(), 'matieres_table' );
</script>
