	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
		<div class="container-fluid">
					
		<!-- Left part -->
		<ul class="nav navbar-nav">
			<li><a class="navbar-brand" href="#"></a></li>	
			<li class="navbar-title">SAUVEGARDE de la BASE</li>
		</ul>


		<!-- Right part -->
		<ul class="nav navbar-nav navbar-right">

			<!-- Help -->
			<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Cette page permet de sauvegarder la base de données du logiciel."><span class="glyphicon glyphicon-info-sign"></span> Info </a></li>
			
		</ul>
		</div>
	</nav>

	<div class="spacer"></div>	


<?php

	// Connexion à la base de données GESPAC
	//$cnx = new Sql ( $host, $user, $pass, $db );

	// nom du fichier dump
	$dumpfile = "glcd_savebase_".date("YmdHis").".sql"; 
	
	// création du fichier dump dans le dossier dump
	backup_base($host, $user, $pass, $db, "fichiers/".$dumpfile);
	
	// On écrit des choses interessantes ici ...
	echo "<center><h2>Création du fichier de sauvegarde ...";
	echo "<br>";
	echo "Pour le voir cliquez >> <a href='fichiers/$dumpfile' target=_blank> $dumpfile </a> << </H2></center>";


?>
