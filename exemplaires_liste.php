<!--

	Liste des exemplaires

-->

		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
			<div class="container-fluid">
			
			<!-- Left part -->
			<ul class="nav navbar-nav">
				<li><a class="navbar-brand" href="#"></a></li>	
				<li class="navbar-title">EXEMPLAIRES <span id="nb-liste"></span> </li>
			</ul>


			<!-- Right part -->
			<ul class="nav navbar-nav navbar-right">
			
				<!-- Nombre selection -->
				<li data-toggle="tooltip" data-title="Nombre d‘exemplaires sélectionnés" class='actionselect hide' ><a href="#"><span id='nb_selectionnes'></span></a></li>
				
				<!-- CODES BARRES  selection -->
				<li data-toggle="tooltip" data-title="IMPRIME les codes barres de la sélection" class='actionselect hide' >
					<form action='exemplaires_barcodes.php' target='_blank' method='post' id='form_barcodes'>
						<input type="hidden" name='id_a_poster' id='id_a_poster' value=''>	
					</form>

					<a href='#' onclick='$("#form_barcodes").submit();'><span class='glyphicon glyphicon-barcode'></span> Codes Barres</a>	
				</li>
				
				<!-- DELETE selection -->
				<li class="actionselect hide"><a href='exemplaires_form.php?action=dellot' class='editbox' data-tip="SUPPRIMER les exemplaires selectionnés" title='SUPPRIMER la Sélection'><span class="glyphicon glyphicon-remove-circle"></span> Supprimer LOT</a></li>
				
				<!-- EDIT selection -->
				<li class="actionselect hide"><a href='exemplaires_form.php?action=modlot' class='editbox' data-tip="MODIFIER les exemplaires selectionnés" title='MODIFIER la Sélection'><span class="glyphicon glyphicon-pencil"></span> Modifier LOT</a></li>

				<!-- Add item -->
				<li><a href='index.php?page=exeAdd&action=add' data-tip="Ajouter des EXEMPLAIRES par LOT" title='Ajouter des EXEMPLAIRES'><span class="glyphicon glyphicon-plus-sign"></span> Ajout Lot</a></li>
	
				<!-- Create item -->
				<li><a href='exemplaires_form.php?action=create' class='editbox' data-tip="Créer un EXEMPLAIRE UNIQUE" title='Créer un exemplaire UNIQUE'><span class="glyphicon glyphicon-plus-sign"></span> Création unique</a></li>
							
				<!-- Help -->
				<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Dans cette page on gère les exemplaires : Ce sont les vrais livres et ils sont uniques."><span class="glyphicon glyphicon-info-sign"></span> Info </a></li>
				
			</ul>
			</div>
		</nav>

	<div class="spacer"></div>



<?PHP 


	// cnx à la base de données
	$cnx = new Sql ( $host, $user, $pass, $db );
	
	

	$arg_matiere 	= $_POST['matiere'];
	$arg_niveau 	= $_POST['niveau'];
	$arg_manuel 	= $_POST['manuel'];
	$arg_editeur 	= $_POST['editeur'];
	$arg_edition 	= $_POST['edition'];
	$arg_cd 		= $_POST['cd'];
	$arg_exeref 	= $_POST['exeref'];
	$arg_prix 		= $_POST['prix'];
	$arg_achat		= $_POST['achat'];
	$arg_etat 		= $_POST['etat'];
	$arg_prete		= $_POST['prete'];
	$arg_rendu 		= $_POST['rendu'];
	$arg_flag 		= $_POST['flag'];
	

	// recherche vide : on ne retourne aucun résultat
	if ( $arg_matiere == '' && $arg_niveau == '' && $arg_manuel == '' && $arg_editeur == '' && $arg_edition == '' && $arg_cd == '' && $arg_exeref == '' && $arg_prix == '' && $arg_achat == '' && $arg_etat == '' && $arg_prete == '' && $arg_rendu == '' && $arg_flag == '' ) {
		$liste_des_exemplaires = array();
	}
	
	// la recherche a au moins un champ : on filtre
	else {
	
		$sql = "
			SELECT exeID, manuelNiveau, manuelTitre, manuelEdition, matiereNom, editeurNom, exeEtat, exeCD, exeREF, exePrix, exeAchat, exeRendu, exeFlag
			FROM manuels, matieres, editeurs, exemplaires
			WHERE 
				manuels.matiereID=matieres.matiereID AND
				manuels.editeurID=editeurs.editeurID AND
				exemplaires.manuelID=manuels.manuelID AND
				
				manuelNiveau LIKE '%$arg_niveau%' AND 
				manuelTitre LIKE '%$arg_manuel%' AND 
				manuelEdition LIKE '%$arg_edition%' AND 
				matiereNom LIKE '%$arg_matiere%' AND 
				editeurNom LIKE '%$arg_editeur%' AND 
				exeEtat LIKE '%$arg_etat%' AND 
				exeCD LIKE '%$arg_cd%' AND 
				exeREF LIKE '%$arg_exeref%' AND 
				exePrix LIKE '%$arg_prix%' AND 
				exeAchat LIKE '%$arg_achat%' AND 
				exeRendu LIKE '%$arg_rendu%' AND
				exeFlag LIKE '%$arg_flag%'  
					
			ORDER BY exeID";
			
		$liste_des_exemplaires = $cnx->QueryAll ( $sql );

	}
	
	
?>
		
	<input type='hidden' name='id_a_poster' id='id_a_poster' value=''>

	<center>
	
	<div class="table-responsive">	

	<table class="table table-condensed hover" id="exemplaires_table">
		
		<th><input type='checkbox' id='checkall'></th>
				
		<form method='post' action='index.php?page=exemplaires' id='searchform'>
			<th>Matière<br><input class='searchfield' name='matiere' placeholder='Matière' size=5 value='<?PHP echo $arg_matiere;?>'/></th>
			<th>Niveau<br><input class='searchfield'  name='niveau' placeholder='Niveau' size=5 value='<?PHP echo $arg_niveau;?>' /></th>
			<th>Manuel<br><input class='searchfield'  name='manuel' placeholder='Manuel' size=5 value='<?PHP echo $arg_manuel;?>' /></th>
			<th>Editeur<br><input class='searchfield'  name='editeur' placeholder='Editeur' size=4 value='<?PHP echo $arg_editeur;?>' /></th>
			<th>Edition<br><input class='searchfield'  name='edition' placeholder='Edition' size=4 value='<?PHP echo $arg_edition;?>' /></th>
			<th>CD<br><select name=cd width=4>
				<option value='' <?PHP if ($arg_cd == '') echo ' selected'; ?> >TOUS</option>
				<option value='1'<?PHP if ($arg_cd == '1') echo ' selected'; ?>>OUI</option>
				<option value='0'<?PHP if ($arg_cd == '0') echo ' selected'; ?>>NON</option>
			</select></th>
			<th>exeREF<br><input class='searchfield'  name='exeref' placeholder='exeREF' size=5 value='<?PHP echo $arg_exeref;?>' /></th>
			<th>Prix<br><input class='searchfield'  name='prix' placeholder='Prix' size=5 value='<?PHP echo $arg_prix;?>' /></th>
			<th>Achat<br><input class='searchfield'  name='achat' placeholder='Achat' size=5 value='<?PHP echo $arg_achat;?>' /></th>
			<th>Etat<br><select name='etat'>
				<option value='' <?PHP if ($arg_etat == '') echo ' selected'; ?> >TOUS</option>
				<option value=4 <?PHP if ($arg_etat == '4') echo ' selected'; ?> >NEUF</option>
				<option value=3 <?PHP if ($arg_etat == '3') echo ' selected'; ?> >BON</option>
				<option value=2 <?PHP if ($arg_etat == '2') echo ' selected'; ?> >MOYEN</option>
				<option value=1 <?PHP if ($arg_etat == '1') echo ' selected'; ?> >PASSABLE</option>
			</select></th>
			<th>Prêté à<br><input class='searchfield'  name='prete' placeholder='Prêté à' size=5 value='<?PHP echo $arg_prete;?>' /></th>
			<th>Rendu<br><select name=rendu width=4>
				<option value='' <?PHP if ($arg_rendu == '') echo ' selected'; ?> >TOUS</option>
				<option value='1'<?PHP if ($arg_rendu == '1') echo ' selected'; ?>>OUI</option>
				<option value='0'<?PHP if ($arg_rendu == '0') echo ' selected'; ?>>NON</option>
			</select></th>
			<th>Flag<br><select name='flag' width=4>
				<option value='' <?PHP if ($arg_flag == '') echo ' selected'; ?> >TOUS</option>
				<option value='1'<?PHP if ($arg_flag == '1') echo ' selected'; ?>>OUI</option>
				<option value='0'<?PHP if ($arg_flag == '0') echo ' selected'; ?>>NON</option>
			</select></th>

			<th><a href='index.php?page=exemplaires' data-tip='vider la recherche'><span class='glyphicon glyphicon-remove-circle'></span></a></th>
			<th><a href='#' onclick="$('#searchform').submit()" data-tip="Lancer la recherche"><span class='glyphicon glyphicon-search'></span></a></th>
		</form>
		
		<?PHP	
				
			foreach ( $liste_des_exemplaires as $record ) {
				
				$exeID			= $record['exeID'];
				$manuelNiveau	= $record['manuelNiveau'];
				$manuelTitre	= $record['manuelTitre'];
				$manuelEdition	= $record['manuelEdition'];
				$matiereNom		= $record['matiereNom'];
				$editeurNom		= $record['editeurNom'];
				$exeEtat		= $record['exeEtat'];
				$exeREF			= $record['exeREF'];
				$exeCD			= $record['exeCD'];
				$exePrix		= $record['exePrix'];
				$exeAchat		= $record['exeAchat'];
				$exeRendu		= $record['exeRendu'];
				$exeFlag		= $record['exeFlag'];
				$userNom		= stripslashes(utf8_encode($cnx->QueryOne ("SELECT CONCAT (userNom, ' ', userPrenom) FROM users, prets WHERE prets.userID=users.userID AND exeID=$exeID;")));
				
				switch ($exeEtat) {
					case 4 : $exeEtatLabel = "NEUF"; $etatClass="neuf"; break;
					case 3 : $exeEtatLabel = "BON"; $etatClass="bon"; break;
					case 2 : $exeEtatLabel = "MOYEN"; $etatClass="moyen"; break;
					case 1 : $exeEtatLabel = "PASSABLE"; $etatClass="passable"; break;
					case 0 : $exeEtatLabel = "PERDU"; $etatClass="perdu"; break;
				}	
				
				//$exeCDlabel = $exeCD==1 ? "<img src='construction/accept.png' height=15>" : "";
				$exeCDlabel = $exeCD==1 ? "<span class='glyphicon glyphicon-ok-sign color-green'></span>" : "";				
				$exeRenduLabel = $exeRendu==1 ? "" : "<span style='color:red;'>PERDU</span>";
				$exeFlagLabel = $exeFlag==1 ? "<span class='glyphicon glyphicon-star color-green'></span>" : "";
				
				echo "<tr id='tr_id$exeID' class='tr_modif'>";
					echo "<td> <input type=checkbox name=chk indexed=true value='$exeID' id='$exeID' class='chk_line'> </td>";	
					echo "<td> $matiereNom</td>";
					echo "<td> $manuelNiveau eme</td>";
					echo "<td> $manuelTitre</td>";
					echo "<td> $editeurNom</td>";
					echo "<td> $manuelEdition</td>";
					echo "<td> $exeCDlabel</td>";
					echo "<td> $exeREF </td>";
					echo "<td> $exePrix </td>";
					echo "<td> $exeAchat </td>";
					echo "<td class='$etatClass'> $exeEtatLabel </td>";
					echo "<td>$userNom</td>";
					echo "<td> $exeRenduLabel </td>";
					echo "<td> $exeFlagLabel </td>";
					
					echo "<td width=20 align=center><a href='exemplaires_form.php?id=$exeID&action=mod' class='editbox' title='Modifier'><span class='glyphicon table-icon glyphicon-edit'></span></a></td>";
					echo "<td width=20 align=center> <a href='exemplaires_form.php?action=del&id=$exeID' class='editbox' title='Supprimer'><span class='glyphicon table-icon glyphicon-remove color-red'></span>	</a> </td>";
					
				echo "</tr>";

			}
			
		?>		

	</table>
	
	<?PHP 
		if (count($liste_des_exemplaires) == 0 ) echo "Pas de résultat";
	?>
	
	
	</div>
	
	<br>
		
	</center>

	
<?PHP
	// On se déconnecte de la db
	$cnx->Close();
		
?>

<script>

	$(function(){
				
		$("#nb-liste").text("[" + $(".tr_modif").length + "]");
		
	
		//--------------------------------------- Selection d'une ligne	
		$('.chk_line').click(function(){
			
			var id = $(this).attr('id');
			
			if ( $(this).is(':checked') ){		
				$('#id_a_poster').val( $('#id_a_poster').val() + ";" + id );
				$("#tr_id" + id).addClass("selected");
			}
			else {
				$('#id_a_poster').val( $('#id_a_poster').val().replace(";" + id + ";", ";") );	// Supprime la valeur au milieu de la chaine
				var re = new RegExp(";" + id + "$", "g"); $('#id_a_poster').val( $('#id_a_poster').val().replace(re, "") );			// Supprime la valeur en fin de la chaine
				$("#tr_id" + id).removeClass("selected");
				$('#checkall').prop("checked", false);
			}
			
			// On affiche les boutons
			if ( $('#id_a_poster').val() != "" ) {				
				$('#nb_selectionnes').show(); $('.actionselect').removeClass("hide"); $('#nb_selectionnes').html( $('.chk_line:checked').length + ' sélectionné(s)');
			} else { 
				$('#nb_selectionnes').hide(); $('.actionselect').addClass("hide");
			}
			
		});
		
		
		
		//--------------------------------------- Selection de toutes les lignes
		$('#checkall').click(function(){
			
			if ( $('#checkall').is(':checked') ){		
				
				$('.chk_line:visible').prop("checked", true);	// On coche toutes les cases visibles

				$('#id_a_poster').val("");	// On vide les matos à poster
				$('.chk_line:visible').each (function(){$('#id_a_poster').val( $('#id_a_poster').val() + ";" + $(this).attr('id') );	});	// On alimente le input à poster
				
				$('.actionselect').removeClass("hide");		// On fait apparaitre les boutons
				$('#nb_selectionnes').show(); $('#nb_selectionnes').html( $('.chk_line:checked').length + ' sélectionné(s)');
				$('.tr_modif:visible').addClass("selected");	// On colorie toutes les lignes	visibles
			}
			else {
				$('#id_a_poster').val("");	// On vide les matos à poster
				$('.chk_line').prop("checked", false);	// On décoche toutes les cases
				$('.tr_modif').removeClass("selected");	// On vire le coloriage de toutes les lignes	
				$('.actionselect').addClass("hide"); $('#nb_selectionnes').hide();
			}			
		});	
		
		
		// **************************************************************** POST AJAX FORMULAIRES
		$("#post_form").click(function(event) {

			/* stop form from submitting normally */
			event.preventDefault(); 
			
			if ( validForm() == true) {
			
				// Permet d'avoir les données à envoyer
				var dataString = $("#formulaire").serialize();
				
				// action du formulaire
				var url = $("#formulaire").attr( 'action' );
				
				var request = $.ajax({
					type: "POST",
					url: url,
					data: dataString,
					dataType: "html"
				 });
				 
				request.done(function(msg) {
					$('#targetback').show(); $('#target').show();
					$('#target').html(msg);
					window.setTimeout("document.location.href='index.php?page=exemplaires'", 3000);
				});
			}			 
		});	
		
	});

	

	
</script>
