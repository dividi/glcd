<!--

	Liste des utilisateurs

-->


		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
			<div class="container-fluid">
			
			<!-- Left part -->
			<ul class="nav navbar-nav">
				<li><a class="navbar-brand" href="#"></a></li>	
				<li class="navbar-title">UTILISATEURS</li>
			</ul>


			<!-- Right part -->
			<ul class="nav navbar-nav navbar-right">
			
				<!-- Nombre selection -->
				<li data-toggle="tooltip" data-title="Nombre d‘utilisateurs sélectionnés" class='actionselect hide' ><a href="#"><span id='nb_selectionnes'></span></a></li>
				
				<!-- IMPRIMER la selection -->
				<li data-toggle="tooltip" data-title="IMPRIME les codes barres de la sélection" class='actionselect hide' >
					<form action='users_prets.php' target=_blank method='post' id='form_prets'>
						<input type="hidden" name='id_a_poster' id='id_a_poster' value=''>	
					</form>

					<a href='#' onclick='$("#form_prets").submit();'><span class='glyphicon glyphicon-print'></span> Imprimer par lot</a>	
				</li>

				<!-- Add item -->
				<li data-toggle="tooltip" data-title="Ajouter un UTILISATEUR"><a href='users_form.php?action=add' class='editbox' title='Ajouter un UTILISATEUR'><span class="glyphicon glyphicon-plus-sign"></span> Ajout</a></li>
				
				<!-- Help -->
				<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Cette page permet de gérer l'ajout, la modification et la suppression des utilisateurs."><span class="glyphicon glyphicon-info-sign"></span> Info </a></li>
								
				<!-- Filter -->
				<li data-tip="Filtrer les données du tableau">
					<form class="navbar-form" role="search">
						<div class="form-group">
							<input class="form-control" placeholder=" filtrer" name="filt" id="filt" onKeyPress="return disableEnterKey(event)" onkeyup="filter(this.value, 'users_table');" type="text" value=<?PHP echo $_GET['filter'];?>> 
							&nbsp;<span class="navbar-text navbar-right" id="filtercount" title="Nombre de lignes filtrées"></span>
						</div>
					</form>					
				</li>
				
			</ul>
			</div>
		</nav>

	<div class="spacer"></div>



<?PHP 

	// cnx à la base de données
	$cnx = new Sql ( $host, $user, $pass, $db );

	// stockage des lignes retournées par sql dans un tableau nommé liste_des_materiels
	$liste_des_users = $cnx->QueryAll ( "SELECT userID, userNom, userPrenom, userNaissance, userDette, classeNiveau, classeNom FROM users, classes WHERE users.classeID=classes.classeID ORDER BY classeNiveau, classeNom, userNom, userPrenom" );

?>
		
	<input type=hidden name='id_a_poster' id='id_a_poster' value=''>


	<center>
<div class="table-responsive">
	<table class="table table-condensed hover" id="users_table">
		<thead>
			<th><input type='checkbox' id='checkall'></th>
			<th>Nom<br><input class="searchfield" name="nom" placeholder="nom" data-col="nom"></th>
			<th>Prénom<br><input class="searchfield" name="Prénom" placeholder="Prénom" data-col="Prénom"></th>
			<th>Naissance<br><input class="searchfield" name="Naissance" placeholder="Naissance" data-col="Naissance"></th>
			<th>Dette<br><input class="searchfield" name="Dette" placeholder="Dette" data-col="Dette"></th>
			<th>Classe<br><input class="searchfield" name="Classe" placeholder="Classe" data-col="Classe"></th>
			<th>Manuels<br><input class="searchfield" name="manuels" placeholder="manuels" data-col="manuels"></th>
			<th>Prêter<br></th>
			<th>Rendre<br></th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</thead>
		<?PHP	
		
		foreach ( $liste_des_users as $record ) {
						
			$userID		= $record['userID'];
			$userDette	= $record['userDette'];
			$userNom	= stripslashes(utf8_encode($record['userNom']));
			$userPrenom	= stripslashes(utf8_encode($record['userPrenom']));
			$userNaissance	= $record['userNaissance'];
			$formatDate = date("d/m/Y", strtotime($userNaissance));
			$classe		= $record['classeNiveau'] . $record['classeNom'];
			
			$nbExemplaires = $cnx->QueryOne ("SELECT COUNT(*) FROM prets WHERE userID=$userID;");
	
			echo "<tr id='tr_id$userID' class='tr_modif'>";
				echo "<td> <input type=checkbox name=chk indexed=true value='$userID' id='$userID' class='chk_line'> </td>";	
				echo "<td class='nom'> $userNom</td>";
				echo "<td> $userPrenom </td>";
				echo "<td> $formatDate </td>";
				echo "<td> $userDette </td>";
				echo "<td> $classe </td>";
				
				echo "<td> <a href='users_form.php?id=$userID&action=liste' class='editbox' data-tip='Liste des manuels de $userNom $userPrenom'> $nbExemplaires </a></td>";
				
				echo "<td width=20 align=center><a href='index.php?page=preter&classes=0&users=$userID' data-tip='Prêter des livres à $userNom $userPrenom'><span class='glyphicon glyphicon-upload table-icon color-green'></a></td>";
				echo "<td width=20 align=center><a href='index.php?page=rendre&classes=0&users=$userID' data-tip='Rendre des livres de $userNom $userPrenom'><span class='glyphicon glyphicon-download table-icon color-blue'></a></td>";
			
				
				echo "<td width=20 align=center><a href='users_form.php?id=$userID&action=liste' class='editbox' title='Liste des manuels de $userNom $userPrenom'><span class='glyphicon glyphicon-book table-icon'></span></a></td>";
				echo "<td width=20 align=center><a href='users_form.php?id=$userID&action=mod' class='editbox' title='Modifier'><span class='glyphicon table-icon glyphicon-edit'></span></a></td>";
				echo "<td width=20 align=center> <a href='users_form.php?action=del&id=$userID' class='editbox' title='Supprimer'><span class='glyphicon table-icon glyphicon-remove color-red'></span></a></td>";
				
			echo "</tr>";

		}
		?>		

	</table>
</div>	
	<br>
	
	</center>
	
	
<?PHP
	// On se déconnecte de la db
	$cnx->Close();
?>

<script>
	// Filtre rémanent
	filter ( $('#filt').val(), 'users_table' );
	
	
	$(function(){
		
		
	$('.searchfield').multifilter();	
  
		
	
		//--------------------------------------- Selection d'une ligne	
		$('.chk_line').click(function(){
			
			var id = $(this).attr('id');
			
			if ( $(this).is(':checked') ){		
				$('#id_a_poster').val( $('#id_a_poster').val() + ";" + id );
				$("#tr_id" + id).addClass("selected");
			}
			else {
				$('#id_a_poster').val( $('#id_a_poster').val().replace(";" + id + ";", ";") );	// Supprime la valeur au milieu de la chaine
				var re = new RegExp(";" + id + "$", "g"); $('#id_a_poster').val( $('#id_a_poster').val().replace(re, "") );			// Supprime la valeur en fin de la chaine
				$("#tr_id" + id).removeClass("selected");
				$('#checkall').prop("checked", false);
			}
			
			// On affiche les boutons
			if ( $('#id_a_poster').val() != "" ) {				
				$('#nb_selectionnes').show(); $('.actionselect').removeClass("hide"); $('#nb_selectionnes').html( $('.chk_line:checked').length + ' sélectionné(s)');
			} else { 
				$('#nb_selectionnes').hide(); $('.actionselect').addClass("hide");
			}
			
		});
		
		
		
		//--------------------------------------- Selection de toutes les lignes
		$('#checkall').click(function(){
			
			if ( $('#checkall').is(':checked') ){		
				
				$('.chk_line:visible').prop("checked", true);	// On coche toutes les cases visibles

				$('#id_a_poster').val("");	// On vide les matos à poster
				$('.chk_line:visible').each (function(){$('#id_a_poster').val( $('#id_a_poster').val() + ";" + $(this).attr('id') );	});	// On alimente le input à poster
				
				$('.actionselect').removeClass("hide");		// On fait apparaitre les boutons
				$('#nb_selectionnes').show(); $('#nb_selectionnes').html( $('.chk_line:checked').length + ' sélectionné(s)');
				$('.tr_modif:visible').addClass("selected");	// On colorie toutes les lignes	visibles
			}
			else {
				$('#id_a_poster').val("");	// On vide les matos à poster
				$('.chk_line').prop("checked", false);	// On décoche toutes les cases
				$('.tr_modif').removeClass("selected");	// On vire le coloriage de toutes les lignes	
				$('.actionselect').addClass("hide"); $('#nb_selectionnes').hide();
			}			
		});	
		
		
		// **************************************************************** POST AJAX FORMULAIRES
		$("#post_form").click(function(event) {

			/* stop form from submitting normally */
			event.preventDefault(); 
			
			if ( validForm() == true) {
			
				// Permet d'avoir les données à envoyer
				var dataString = $("#formulaire").serialize();
				
				// action du formulaire
				var url = $("#formulaire").attr( 'action' );
				
				var request = $.ajax({
					type: "POST",
					url: url,
					data: dataString,
					dataType: "html"
				 });
				 
				request.done(function(msg) {
					$('#targetback').show(); $('#target').show();
					$('#target').html(msg);
					window.setTimeout("document.location.href='index.php?page=users'", 3000);
				});
			}			 
		});	
		
	});
	
	
</script>
