<?PHP


	// fichier de creation / modif / suppr d'un éditeur
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');		
	
	
	
	// Cnx à  la base
	$cnx = new Sql($host, $user, $pass, $db);
	
	// Les LOGS
	$logsql = new log ("fichiers/log_sql.sql");
	$log = new log ("fichiers/log.txt");
	
	// on récupère les paramètres de l'url	
	$action 	= $_GET['action'];
		

	/**************** @@SUPPRESSION ********************/
	
	if ( $action == 'del' ) {
		
		$id = $_POST['id'];
	    $editeurNom = $cnx->QueryOne ( "SELECT editeurNom FROM editeurs WHERE editeurID=$id" );
	
		// Suppression de l'utilisateur de la base
		$req_suppr_editeur = "DELETE FROM editeurs WHERE editeurID=$id;";
		$cnx->Execute ( $req_suppr_editeur );
		$logsql->Insert( $req_suppr_editeur );
		
		echo "L'EDITEUR <b>$editeurNom</b> a été supprimé.";	
		$log->Insert( "L'EDITEUR $editeurNom a été supprimé." );		
	  
	}

	/**************** @@MODIFICATION ********************/	
	if ( $action == 'mod' ) {
	
		$id     	= $_POST ['id'];
		$ref     	= $_POST ['ref'];
		$nom 		= addslashes($_POST ['nom']);
	
		// on récupére les anciennes valeurs du compte pour les logs
		$old_info = $cnx->QueryRow("SELECT editeurREF, editeurNom FROM editeurs WHERE editeurID=$id");
		$OLDediteurREF = $old_info["editeurREF"];
		$OLDediteurNom = $old_info["editeurNom"];
		
		
		$req_modif = "UPDATE editeurs SET editeurREF='$ref', editeurNom='$nom' WHERE editeurID=$id";
		$cnx->Execute ( $req_modif );
		$logsql->Insert( $req_modif );
	
		echo "L'EDITEUR <b>$OLDediteurREF-$OLDediteurNom</b> a été modifié en <b>$ref-$nom</b>";
		$log->Insert( "L'EDITEUR $OLDediteurREF-$OLDediteurNom a été modifié en $ref-$nom" );
	}
	
	/**************** @@INSERTION ********************/
	if ( $action == 'add' ) {
	
		$nom 		= addslashes(utf8_decode(urldecode($_POST ['nom'])));
		$ref 		= $_POST ['ref'];
		
		$req_add = "INSERT INTO editeurs ( editeurREF, editeurNom ) VALUES ( '$ref', '$nom')";
		$cnx->Execute ( $req_add );
		$logsql->Insert( $req_add );
		
		echo "L'EDITEUR <b>$nom</b> a été créé.";
		$log->Insert( "L'EDITEUR $nom a été créé." );
	}
	

?>


