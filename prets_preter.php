<?PHP

/*

	Le prêt

*/

	// connexion à la base de données
	$cnx = new Sql ($host, $user,$pass, $db);
	
	$currentClasse = @$_GET['classes'];
	$currentUser = @$_GET['users'];
	
?>



<script type="text/javascript"> 

	exeArray = new Array();

	// **************************************************************** FILTRE des EAN13
	function filterEXEREF (exeREF){
	
		var ref = $(exeREF).val();
		
		// On vérifie que l'exemplaire n'est pas déjà dans la liste
		if ($.inArray(ref, exeArray) >= 0) {$('#msg').html('Déjà dans la liste !'); return false;}
		
		if (ref.length == 13) {
		
			if ( $('#DIV' + ref) ) {$('#' + ref).parent().empty();}
						
			$.post("prets_search_preter.php", {
				ref: ref,
			}, function(response){			
				$('#exemplaires_liste').append("<tr class='exe-pret' id='DIV" + ref + "'>" + response + "</tr>");
			});
			
			exeArray.push(ref);
			
			// Le setTimeout est pour Firefox qui ne répond pas assez vite à la demande de focus on dirait ...
			$('#exeREF').val("").focus();
			setTimeout(function() { $('#exeREF').focus(); }, 50);
			
			$("#nb-liste").text("(" + parseInt($(".exe-pret").length +1) + " prêtés)");
		}
	
	}
	
	$('input').blur(function() {
		var $this = $(this);
		if (!$this.val()) {
			setTimeout(function() { $this.focus(); }, 50);
		}
	});	
	
	// **************************************************************** SUPPRIME une LIGNE
	function deleteEXE (ref){
		$('#DIV' + ref).remove();
				
		// Suppression de l'élément dans le tableau
		exeArray = jQuery.grep(exeArray, function(value) { return value != ref; });
		
		$("#nb-liste").text("(" + parseInt($(".exe-pret").length) + " prêtés)");
	}	
		

	$(function() {	

		
		// **************************************************************** Sur entrée dans la ref manuel
		$(".ref").on ('keydown', function(event){	

			if (event.which == 13) {
				event.preventDefault();
				$(this).blur();
			}
		});	
		
		
				
		
		// **************************************************************** POST AJAX FORMULAIRES
		$("#post_form").click(function(event) {
			
			// Sur post du formulaire on redirige la page !
			window.setTimeout("document.location.href='index.php?page=preter&classes=<?PHP echo $currentClasse; ?>'", 1500);
		});	
					
	});
	
	
</script>


	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
		<div class="container-fluid">
		
		<!-- Left part -->
		<ul class="nav navbar-nav">
			<li><a class="navbar-brand" href="#"></a></li>
			<li class="navbar-title">PRETER des EXEMPLAIRES <span id='nb-liste'></span></li>
			<!--<li data-toggle="tooltip-menu"><a href="#menu-toggle" id="menu-toggle"><span class="glyphicon glyphicon-fullscreen"></span> Menu</a></li>-->
		</ul>
		
		<!-- Right part -->
		<ul class="nav navbar-nav navbar-right">
			<!-- Help -->
			<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Cette page permet de prêter des exemplaires de manuel à des utilisateurs."><span class="glyphicon glyphicon-info-sign"></span> Aide </a></li>
		</ul>
		
		</div>
	</nav>

	<div class="spacer"></div>	
		



<center>
	
	
	<div style="float:left;">
	<form method=GET action='index.php'>

		<input type=hidden name=page value='preter'>

		<select name='classes' onchange="users.value='';submit();" class="selectClasse">
			
			<option value=0>TOUTES</option>
			
			<?PHP
				$classes = $cnx->QueryAll("SELECT classeID, CONCAT(classeNiveau, classeNom) as classe FROM classes ORDER BY classe");
			
				foreach ($classes as $classe) {
					$classeID = $classe["classeID"];
					$classe = $classe["classe"];
					
					$classeSelect = $classeID == $currentClasse ? " selected" : "";
					
					echo "<option value='$classeID' $classeSelect>$classe</option>";
				}
				
			?>
		</select>
				<br>		
		
		<select name='users' id="users" onchange='submit();' class="selectUser">
		
			<option value=""></option>
			<?PHP

				$where = $currentClasse == 0 ? "": " WHERE classeID=$currentClasse ";
				
				$users = $cnx->QueryAll("SELECT userID, userNom, userPrenom, userNaissance FROM users $where ORDER BY userNom, userPrenom");
			
				foreach ($users as $user) {
					$userID = $user["userID"];
					$userNom = stripslashes(utf8_encode($user["userNom"]));
					$userPrenom = stripslashes(utf8_encode($user["userPrenom"]));
					$userNaissance = $user["userNaissance"];
					
					$userSelect = $userID == $currentUser ? " selected" : "";
					
					echo "<option value='$userID' $userSelect>$userNom $userPrenom</option>";
				}
			?>
		</select>		
		
	</form>
	</div>
	
	
	
	<form method=post action="prets_post.php?action=pret" target=_blank id='formulaire'>

		<?PHP 

			if ($currentUser <> "" ) {
			
				$user = $cnx->QueryRow("SELECT userNom, userPrenom, userNaissance FROM users WHERE userID=$currentUser");
				$nom 		= stripslashes(utf8_encode($user["userNom"]));
				$prenom 	= stripslashes(utf8_encode($user["userPrenom"]));
				$naissance 	= $user["userNaissance"];
				$formatDate 	= date("d/m/Y", strtotime($naissance));
			
				echo "<div id='fiche'>PRET <b>$nom $prenom</b> ($formatDate)</div>";
				
				echo "<input type='submit' id='post_form' value='Prêter'><br>";
				
				echo "<input type=hidden name=userID value=$currentUser>";
			?>	
				<input type=text size=14 maxlength=13 class='ref' id='exeREF' tabindex=-1 onchange="filterEXEREF($(this));"><span id='msg'></span>
				
			<?PHP
			
			$prets = $cnx->QueryAll("
			SELECT exemplaires.exeID as exempID, manuelNiveau, manuelTitre, manuelEdition, matiereNom, editeurNom, exeEtat, exeCD, exeREF, exePrix, exeAchat
			FROM manuels, matieres, editeurs, exemplaires, prets
			WHERE 
				manuels.matiereID=matieres.matiereID AND
				manuels.editeurID=editeurs.editeurID AND
				exemplaires.manuelID=manuels.manuelID AND
				exemplaires.exeID=prets.exeID AND
				prets.userID=$currentUser;
				
				");
				
				
				echo "<table class='table table-condensed alternate' id='exemplaires_liste'>
					<th></th>
					<th>REF</th>
					<th>Matière</th>
					<th>Editeur</th>
					<th>Niveau</th>
					<th>Titre</th>
					<th>Edition</th>
					<th>Date<br>d'achat</th>
					<th>Prix</th>			
					<th>CD</th>		
					<th>Etat</th>
					<th>&nbsp;</th>";
					

				
				foreach ($prets as $pret) {
					$exeID = $pret['exempID'];
					$manuelNiveau = $pret['manuelNiveau'];
					$manuelTitre = $pret['manuelTitre'];
					$manuelEdition = $pret['manuelEdition'];
					$matiereNom = $pret['matiereNom'];
					$editeurNom = $pret['editeurNom'];
					$exeEtat = $pret['exeEtat'];	
					$exeREF = $pret['exeREF'];	
					$exePrix = $pret['exePrix'];	
					$exeAchat = $pret['exeAchat'];	
					$exeCD = $pret['exeCD'];	
										
					switch ($exeEtat) {
						case 4 : $exeEtatLabel = "NEUF"; $etatClass="neuf"; break;
						case 3 : $exeEtatLabel = "BON"; $etatClass="bon"; break;
						case 2 : $exeEtatLabel = "MOYEN"; $etatClass="moyen"; break;
						case 1 : $exeEtatLabel = "PASSABLE"; $etatClass="passable"; break;
						case 0 : $exeEtatLabel = "PERDU"; $etatClass="perdu"; break;
					}	
					
					$exeCDlabel = $exeCD==1 ? "<img src='construction/accept.png' height=15>" : "";
					
					//echo "<div class='exeLines'><span>  $matiereNom $editeurNom ($manuelNiveau ème) - $manuelTitre ($manuelEdition) - Etat : $exeEtatLabel</span></div>";
					echo "<tr class='exe_pret'>";
						echo "<td><input type=hidden name='oldExeID[]' value=$exeID></td>";
						echo "<td>$exeREF</td>";
						echo "<td>$matiereNom</td>";
						echo "<td>$editeurNom</td>";
						echo "<td>$manuelNiveau</td>";
						echo "<td>$manuelTitre</td>";
						echo "<td>$manuelEdition</td>";
						echo "<td>$exeAchat</td>";
						echo "<td>$exePrix</td>";
						echo "<td>$exeCDlabel</td>";
						echo "<td class=$etatClass>$exeEtatLabel</td>";
						echo "<td>&nbsp;</td>";
					echo "</tr>";
					
					
				}
				
				// Ajax ajoute les lignes ici via le fichier prets_search_preter.php
				echo "</table>";
			
			}


		?>

	</form>
</center>
