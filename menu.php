
<li class="menu-divider"><img src="construction/book.png">MANUELS</li>
<li id="manuels" class="menu-item"><a href="index.php?page=manuels">manuels</a></li>
<li id="exemplaires" class="menu-item"><a href="index.php?page=exemplaires">exemplaires</a></li>
<li id="matieres" class="menu-item"><a href="index.php?page=matieres">matières</a></li>
<li id="editeurs" class="menu-item"><a href="index.php?page=editeurs">editeurs</a></li>

<br>

<li><span class="menu-divider" title=""><img src="construction/books.png">EXEMPLAIRES</span></li>
<li id="preter" class="menu-item"><a href="index.php?page=preter">preter</a></li>
<li id="rendre" class="menu-item"><a href="index.php?page=rendre">rendre</a></li>
<li id="echanges" class="menu-item"><a href="index.php?page=echanges">échanges</a></li>
<li id="factures" class="menu-item"><a href="index.php?page=factures">factures</a></li>

<br>

<li><span class="menu-divider"><img src="construction/users.png">UTILISATEURS</span></li>
<li id="users" class="menu-item"><a href="index.php?page=users">utilisateurs</a></li>
<li id="classes" class="menu-item"><a href="index.php?page=classes">classes</a></li>
<li id="user_import" class="menu-item"><a href="index.php?page=user_import">importer utilisateurs</a></li>

<br>

<li><span class="menu-divider"><img src="construction/stat.png">STAT</span></li>
<li id="stat_manuels" class="menu-item"><a href="index.php?page=stat_manuels">par manuels</a></li>

<br>

<li><span class="menu-divider"><img src="construction/save.png">DONNEES</span></li>
<li id="data_save" class="menu-item"><a href="variables_form.php?action=mod" class='editbox' title='modification de la configuration'>configuration</a></li>
<li id="data_save" class="menu-item"><a href="index.php?page=data_save">sauvegarder</a></li>
<li id="apropos" class="menu-item"><a href="index.php?page=apropos">à propos</a></li>

<br>

<li class="menu-item"><a href='logout.php'><span class="glyphicon glyphicon-off"></span> Déconnexion</a></li>


	<div class="spacer"></div>	

<script>

	$(function(){

		// La page courante dans l'url
		var page = getQueryVariable("page");
		
		// On affiche le set d'item en fonction de la page
		if (page) {
			$("#" + page).addClass("menu-item-select");	// On affiche le set d'items
		}


	});

</script>
