<?PHP session_start(); ?>

<!DOCTYPE html>

<html  lang="fr">

	<head>
		<title>.: GLCD : Gestion des Livres du Collège DEFFERRE :.</title>
		
		<!--	META	-->
		<meta name="description" content="">
		<meta name=Keywords content="">
		<meta name=Robots content=All>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- jQuery and jQuery UI -->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		<link rel="stylesheet" href="css/jquery-ui.css" type="text/css" media="screen" charset="utf-8">
		<link rel="stylesheet" href="css/jquery-ui.structure.css" type="text/css" media="screen" charset="utf-8">
		<link rel="stylesheet" href="css/jquery-ui.theme.css" type="text/css" media="screen" charset="utf-8">
		<!--<script type="text/javascript" src="js/jquery.ui.resizable.min.js"></script> -->
		
		<!--    JS      -->
		<script type="text/javascript" src="js/main.js"></script> 
		<script type="text/javascript" src="js/multifilter.min.js"></script> 
			
		
		<!--    BOOTSTRAP CSS    -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/simple-sidebar.css" rel="stylesheet">
		
		<!--    CSS    -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen"  /> 
		<link rel="stylesheet" href="css/tooltips.css" type="text/css" media="screen"  />
	
			
		<!--	FAVICON	-->
		<link rel="SHORTCUT ICON" href="construction/book1.png"/>
		
	</head>
	
	<body>
	
		<div id="targetback"></div><div id="target"></div>
		
		<?PHP
			// lib
			include_once ('fonctions.php');
			include_once ('config.php');
			include_once ('class/Log.class.php');
			include_once ('class/Sql.class.php');


			// Si auth nok, on redirige vers la page de login
			if (!isset( $_SESSION['login'])) {	
				header ("Location: ./login.php");
			}
		?>
		
		
		
  
   <div id="wrapper">
		
		<div id="toggle-sidebar" data-tip='Masque ou montre le menu latéral.'><a href="#"><span class="glyphicon glyphicon-fullscreen"></span></a></div>
   
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
              	<?PHP include("menu.php");?>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
        
		
        <!-- Page Content -->
        <div id="page-content-wrapper">
		
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">					
                       
					   
					   	<?PHP 
				
						$page = @$_GET['page'];
						
						switch ($page) {
							
							case "manuels" : include("manuels_liste.php"); break;
							case "exemplaires" : include("exemplaires_liste.php"); break;
							case "exeAdd" : include("exemplaires_form.php"); break;
							case "matieres" : include("matieres_liste.php"); break;
							case "editeurs" : include("editeurs_liste.php"); break;
												
							case "preter" : include("prets_preter.php"); break;
							case "rendre" : include("prets_rendre.php"); break;
							case "echanges" : include("echanges_liste.php"); break;
							case "factures" : include("factures_liste.php"); break;
							
							case "stock" : include("exemplaires_stock.php"); break;
							
							case "users" : include("users_liste.php"); break;
							case "classes" : include("classes_liste.php"); break;
							
							case "stat_manuels" : include("stat_manuels.php"); break;
							
							case "data_save" : include("save_db.php"); break;
							case "user_import" : include("users_import.php"); break;
							case "apropos" : include("propos.php"); break;
						
							default : include("content.php"); break;
						}

						
						
						?>
					   
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	
	</body>

</html>


	<!--    BOOTSTRAP      -->
	<script src="js/bootstrap.js"></script>
	
	<script>
	
		// Auto toggle the sidebar
		if ( localStorage.getItem("sidebar") == 0 ) { $("#wrapper").toggleClass("toggled");}	
	</script>
