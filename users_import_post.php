<?PHP

	/*******************************************************
	*
	*		Requêtes pour Import des comptes utilisateurs 
	*
	********************************************************/
		
	$dossier = 'fichiers/'; 		// dossier où sera déplacé le fichier
	
	$pos_nom = 0;
	$pos_prenom = 1;
	$pos_naissance = 3;
	$pos_classe = 5;
	
	$extensions = array('.txt', '.csv');
	$extension = strrchr($_FILES['myfile']['name'], '.'); 
	
	//Si l'extension n'est pas dans le tableau
	if ( !in_array($extension, $extensions) )
		 $erreur = 'Vous devez uploader un fichier de txt ou csv...';

	if (!isset($erreur)) {	//S'il n'y a pas d'erreur, on upload
		 
		//On upload et on teste si la fonction renvoie TRUE
		if ( move_uploaded_file($_FILES['myfile']['tmp_name'], $dossier . "import_users.csv") ) {
					
			// ************ Traitement du fichier uploadé *****************
	
			// Libs
			require_once ('fonctions.php');
			include_once ('config.php');
			include_once ('class/Log.class.php');
			include_once ('class/Sql.class.php');

			
			// connexion à la base de données
			$cnx = new Sql($host, $user, $pass, $db);
			
			//Log SQL
			$log = new Log ("log_sql.sql");
	
			$chemin_import = $dossier . "import_users.csv";
			
			$handle = fopen($chemin_import, "r");


			while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
				
				
				// On saute l'entête si il existe
				if ($data[$pos_nom] <> "NOM") {
					
					// On gère les classes si la classe existe on récupère son id, sinon on la créée et on récupère son id
					
					$class_niv = substr ($data[$pos_classe], 0, 1);
					$class_nom = substr ($data[$pos_classe], -1);
					
					$classeID = $cnx->QueryOne ("SELECT classeID FROM classes WHERE classeNiveau='$class_niv' AND classeNom='$class_nom'");
					
					if (!$classeID) {
						$req_creer_classe = "INSERT INTO classes ( classeNiveau, classeNom ) VALUES ( '$class_niv', '$class_nom' );";
						$cnx->Execute ( $req_creer_classe );
						$log->Insert( $req_creer_classe );
						$classeID = $cnx->GetLastID();
					}
				
					$datesplit = explode ("/", $data[$pos_naissance]);
					$formatNaissance = $datesplit[2] . "-" . $datesplit[1] . "-" . $datesplit[0];

					$existe = $cnx->QueryOne ( "SELECT * FROM users WHERE userNom='" . addslashes($data[$pos_nom]) . "' AND userPrenom='" . addslashes($data[$pos_prenom]) . "' AND userNaissance='" . $formatNaissance . "';" );	// Le grade par défaut dans lequel nous allons ranger tous les utilisateurs.
					
					if (!$existe) {
						$req_import_users = "INSERT INTO users (userNom, userPrenom, userNaissance, classeID) VALUES ('" . addslashes($data[$pos_nom]) . "', '" . addslashes($data[$pos_prenom]) ."', '" . $formatNaissance . "', $classeID );";
						$cnx->Execute ( $req_import_users );
						$log->Insert ( $req_import_users );
						
						echo "Import de <b>". addslashes($data[$pos_nom]) . " " . addslashes($data[$pos_prenom]) . "</b> en classe de <b>" . $class_niv.$class_nom ."</b><br>";
					}
					else
						echo "L'élève <b>" . addslashes($data[$pos_nom]) . " " . addslashes($data[$pos_prenom]) . "</b> est déjà dans la base <br>";
				
				}
			}
							

			// On se déconnecte de la db
			$cnx->Close();	

		} else	// En cas d'échec d'upload
			echo "Echec de l'upload !";
			  
	} else // En cas d'erreur
		 echo $erreur;

?>
