<?PHP


	// fichier de creation / modif / suppr d'un élève
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');		
	
	
	
	// Cnx à  la base
	$cnx = new Sql($host, $user, $pass, $db);
	
	// Les LOGS
	$logsql = new log ("fichiers/log_sql.sql");
	$log = new log ("fichiers/log.txt");
	
	// on récupère les paramètres de l'url	
	$action 	= $_GET['action'];
	
	/**************** @@SUPPRESSION ********************/
	
	if ( $action == 'del' ) {
		
		$id = $_POST['id'];

		$eleve = $cnx->QueryRow ( "SELECT userNom, userPrenom, userNaissance, CONCAT(classeNiveau, classeNom) as classe FROM users, classes WHERE users.classeID=classes.classeID AND userID=$id" );
		
		$userNom 	= $eleve["userNom"];
		$userPrenom 	= $eleve["userPrenom"];
		$userNaissance 	= $eleve["userNaissance"];
		$classe 	= $eleve["classe"];
		
		// Suppression de l'utilisateur de la base
		$req_suppr = "DELETE FROM users WHERE userID=$id;";
		$cnx->Execute ( $req_suppr );
		$logsql->Insert( $req_suppr );
		
		echo "L'ELEVE <b>$userNom $userPrenom de $classe</b> a été supprimé.";	
		$log->Insert( "L'ELEVE $userNom $userPrenom de $classe a été supprimé." );		
	  
	}

	/**************** @@MODIFICATION ********************/	
	if ( $action == 'mod' ) {
	
		$id     	= $_POST ['id'];
		$prenom    	= $_POST ['prenom'];
		$nom 		= $_POST ['nom'];
		$naissance	= $_POST ['naissance'];
		$classeid	= $_POST ['classe'];
		$dette		= $_POST ['dette'];
		
		// on récupére les anciennes valeurs du compte pour les logs
		$old_info = $cnx->QueryRow("SELECT userNom, userPrenom, userNaissance, CONCAT(classeNiveau, classeNom) as classe FROM users, classes WHERE users.classeID=classes.classeID AND userID=$id");
		$OLDuserNom 	= $old_info["userNom"];
		$OLDuserPrenom 	= $old_info["userPrenom"];
		$classe 		= $old_info["classe"];


		$req_modif = "UPDATE users SET userNom='$nom', userPrenom='$prenom', userNaissance='$naissance', userDette=$dette, classeID=$classeid WHERE userID=$id";
		$cnx->Execute ( $req_modif );
		$logsql->Insert( $req_modif );
	
		echo "L'ELEVE <b>$OLDuserNom $OLDuserPrenom</b> a été modifié en <b>$nom $prenom</b>";
		$log->Insert( "L'ELEVE $OLDuserNom $OLDuserPrenom a été modifié en <b>$nom $prenom</b>" );
	}
	
	/**************** @@INSERTION ********************/
	if ( $action == 'add' ) {
	
		$nom 		= addslashes($_POST ['nom']);
		$prenom		= addslashes($_POST ['prenom']);
		$naissance	= $_POST ['naissance'];
		$classe		= $_POST ['classe'];
		
		$req_add = "INSERT INTO users ( userNom, userPrenom, userNaissance, classeID ) VALUES ( '$nom', '$prenom', '$naissance', $classe)";
		$cnx->Execute ( $req_add );
		$logsql->Insert( $req_add );
		
		echo "L'ELEVE <b>" . stripslashes($nom) . " " . stripslashes($prenom) . "</b> a été créé.";
		$log->Insert( "L'ELEVE " . stripslashes($nom) . " " . stripslashes($prenom) . " a été créé." );
	}
	


?>


