<?PHP
	#formulaire de modification des variables !
	
	// lib
	include ('config.php');	// fichiers de configuration des bases de données
	require_once ('fonctions.php');
	include_once ('class/Log.class.php');
	include_once ('class/Sql.class.php');
	
?>



<script type="text/javascript"> 


	$(function() {	
				
		// **************************************************************** POST AJAX FORMULAIRES
		$("#post_form").click(function(event) {

			/* stop form from submitting normally */
			event.preventDefault(); 
			
			if ( validForm() == true) {
			
				// Permet d'avoir les données à envoyer
				var dataString = $("#formulaire").serialize();
				
				// action du formulaire
				var url = $("#formulaire").attr( 'action' );
				
				var request = $.ajax({
					type: "POST",
					url: url,
					data: dataString,
					dataType: "html"
				 });
				 
				 request.done(function(msg) {
					$('#dialog').dialog('close');
					$('#targetback').show(); $('#target').show();
					$('#target').html(msg);
					window.setTimeout("document.location.href='index.php'", 2500);
				 });
			}			 
		});	
	});
</script>

<?PHP

	// connexion à la base de données
	$cnx 	= new Sql ($host, $user,$pass, $db);
	
	$action = $_GET['action'];

	
	
	#***************************************************************************
	# 				@@ MODIFICATION
	#***************************************************************************
	
	if ($action == 'mod') {
			
	
		// Requete pour récupérer les données des champs à modifier
		$config_liste = $cnx->QueryAll ( "SELECT * FROM config;" );		
		
	?>

		<br>
		
		<form action="variables_post.php?action=mod" method="post" name="post_form" id="formulaire">
			<center>
			<table class="formtable">
				<th>Clé</th>
				<th>Valeur</th>

<?PHP
	
		foreach ($config_liste as $config) {

			$key = $config["configKey"];
			$value = $config["configValue"];
			
			echo"<tr>";
				echo "<td>$key</td>";
				echo "<td><input type='text' name='$key' value='$value'></td>";
					
			echo "</tr>";
				
		}		
				
		?>		
			</table>
			
			<br>
			<input type=submit value='Modifier la CONFIG' id="post_form">

			</center>

		</FORM>
		
		<?PHP

	}
	
?>		
