<!--

	Liste des éditeurs

-->



		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
			<div class="container-fluid">

			<!-- Left part -->
			<ul class="nav navbar-nav">
				<li><a class="navbar-brand" href="#"></a></li>	
				<li class="navbar-title">EDITEURS</li>
			</ul>


			<!-- Right part -->
			<ul class="nav navbar-nav navbar-right">

				<!-- Add item -->
				<li data-toggle="tooltip" data-title="Ajouter un EDITEUR"><a href='editeurs_form.php?action=add' class='editbox' title='Ajouter un EDITEUR'><span class="glyphicon glyphicon-plus-sign"></span> Ajout</a></li>
				
				<!-- Help -->
				<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Cette page permet de gérer l'ajout, la modification et la suppression des éditeurs."><span class="glyphicon glyphicon-info-sign"></span> Info </a></li>
				
				<!-- Filter -->
				<li data-toggle="tooltip-filtre">
					<form class="navbar-form" role="search">
						<div class="form-group">
							<input class="form-control" placeholder=" filtrer" name="filt" id="filt" onKeyPress="return disableEnterKey(event)" onkeyup="filter(this.value, 'editeurs_table');" type="text" value=<?PHP echo $_GET['filter'];?>> 
							&nbsp;<span class="navbar-text navbar-right" id="filtercount" title="Nombre de lignes filtrées"></span>
						</div>
					</form>					
				</li>

			</ul>
			</div>
		</nav>

	<div class="spacer"></div>	


<?PHP 

	// cnx à la base de données
	$cnx = new Sql ( $host, $user, $pass, $db );

	// stockage des lignes retournées par sql dans un tableau nommé liste_des_materiels
	$liste_des_editeurs = $cnx->QueryAll ( "SELECT editeurID, editeurREF, editeurNom FROM editeurs ORDER BY editeurNom" );

?>
		
	<input type=hidden name='id_a_poster' id='id_a_poster' value=''>


	<center>

	<table class="table table-condensed hover" id="editeurs_table">
		<th>REF</th>
		<th>Nom</th>
		<th>Matières</th>
		<th>Manuels</th>
		<th>Exemplaires</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
		
		<?PHP	
		
			foreach ( $liste_des_editeurs as $record ) {
				
						
				$editeurID		= $record['editeurID'];
				$editeurREF		= $record['editeurREF'];
				$editeurNom		= $record['editeurNom'];
				
				$nb_matieres = $cnx->QueryOne ("SELECT COUNT(DISTINCT matiereID) FROM manuels WHERE editeurID=$editeurID");
				$nb_manuels = $cnx->QueryOne ("SELECT COUNT(manuelID) FROM manuels WHERE editeurID=$editeurID");
				$nb_exe = $cnx->QueryOne ("SELECT COUNT(exemplaires.exeID) FROM manuels, exemplaires WHERE exemplaires.manuelID=manuels.manuelID AND editeurID=$editeurID");
				
				echo "<tr>";
					echo "<td> $editeurREF</td>";
					echo "<td> $editeurNom </td>";
					
					echo "<td> $nb_matieres </td>";
					echo "<td> $nb_manuels </td>";
					echo "<td> $nb_exe </td>";
					
					echo "<td width=20 align=center><a href='editeurs_form.php?id=$editeurID&action=mod' class='editbox' title='Modifier'><span class='glyphicon glyphicon-edit table-icon'></span></a></td>";
					echo "<td width=20 align=center> <a href='editeurs_form.php?action=del&id=$editeurID' class='editbox' title='Supprimer'><span class='glyphicon table-icon glyphicon-remove color-red'></span></a> </td>";
					
				echo "</tr>";

			}
		?>		

	</table>
	
	<br>
	
	</center>
	
	
<?PHP
	// On se déconnecte de la db
	$cnx->Close();
?>

<script>
	// Filtre rémanent
	filter ( $('#filt').val(), 'editeurs_table' );
</script>
