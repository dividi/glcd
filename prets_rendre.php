<?PHP

/*

	Rendre

*/

	// connexion à la base de données
	$cnx = new Sql ($host, $user,$pass, $db);
	
	$currentClasse = @$_GET['classes'];
	$currentUser = @$_GET['users'] == "" ? 0 : $_GET['users'] ; 
	
?>


<script type="text/javascript"> 

	exeArray = new Array();

	// **************************************************************** FILTRE des EAN13
	function filterEXEREF (exeREF){
	
		var ref = $(exeREF).val();
		
		// On vérifie que l'exemplaire n'est pas déjà dans la liste
		if ($.inArray(ref, exeArray) >= 0) {$('#msg').html('Déjà dans la liste !'); return false;}
		
		if (ref.length == 13) {
		
			if ( $('#DIV' + ref) ) {$('#' + ref).parent().empty();}	//je me rappelle plus pourquoi j'ai fait ça :/

			$.post("prets_search_rendre.php", {
				ref: ref,
				userid: <?PHP echo $currentUser;?>,
			}, function(response){			
				$('#rendus').append("<tr id='DIV" + ref + "' class='exeLines'>" + response + "</tr>");
			});
			
			// liste des exemplaires rendus
			exeArray.push(ref);
			
			// Le setTimeout est pour Firefox qui ne répond pas assez vite à la demande de focus on dirait ...
			$('#exeREF').val("").focus();
			setTimeout(function() { $('#exeREF').focus(); }, 50);

			$("#nbrendus").text( $(".exeLines").length +1 );
			

		}
	}	
	
	// **************************************************************** SUPPRIME une LIGNE
	function deleteEXE (ref){
		$('#DIV' + ref).remove();
		
		// Suppression de l'élément dans le tableau
		exeArray = jQuery.grep(exeArray, function(value) { return value != ref; });

		$("#nbrendus").text( $(".exeLines").length );
	}	
		

	$(function() {	
	
		// **************************************************************** Sur entrée dans la ref manuel
		$(".ref").on ('keydown', function(event){	

			if (event.which == 13) {
				event.preventDefault();
				$(this).blur();
			}
		});	
		
		
				
		// **************************************************************** POST AJAX FORMULAIRES
		$("#post_form").click(function(event) {
			
			// Sur post du formulaire on redirige la page !
			//window.setTimeout("document.location.href='index.php?page=rendre'", 1500);	 
			window.setTimeout("document.location.href='index.php?page=rendre&classes=" + <?PHP echo $currentClasse; ?> + "&users='", 1500);	
		});			

	});
	

	
</script>


	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
		<div class="container-fluid">
		
		<!-- Left part -->
		<ul class="nav navbar-nav">
			<li><a class="navbar-brand" href="#"></a></li>
			<li class="navbar-title">RENDRE des EXEMPLAIRES</li>
			<!--<li data-toggle="tooltip-menu"><a href="#menu-toggle" id="menu-toggle"><span class="glyphicon glyphicon-fullscreen"></span> Menu</a></li>-->
		</ul>
		
		<!-- Right part -->
		<ul class="nav navbar-nav navbar-right">
			<!-- Help -->
			<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Cette page permet de rendre des exemplaires de manuel prêtés à des utilisateurs."><span class="glyphicon glyphicon-info-sign"></span> Aide </a></li>
		</ul>
		
		</div>
	</nav>

	<div class="spacer"></div>	


<center>
	
	
	<div style="float:left;">
	<form method=GET action='index.php'>

		<input type=hidden name=page value='rendre'>

		<select name='classes' onchange="users.value='';submit();" class="selectClasse">
			
			<option value=0>TOUTES</option>
			
			<?PHP
				$classes = $cnx->QueryAll("SELECT classeID, CONCAT(classeNiveau, classeNom) as classe FROM classes ORDER BY classe");
			
				foreach ($classes as $classe) {
					$classeID = $classe["classeID"];
					$classe = $classe["classe"];
					
					$classeSelect = $classeID == $currentClasse ? " selected" : "";
					
					echo "<option value='$classeID' $classeSelect>$classe</option>";
				}
				
			?>
		</select>
		
		<br>
		
		<select name='users' id="users" onchange='submit();' class="selectUser">
		
			<option value=""></option>
			<?PHP

				$where = $currentClasse == 0 ? "": " WHERE classeID=$currentClasse ";
				
				$users = $cnx->QueryAll("SELECT userID, userNom, userPrenom, userNaissance FROM users $where ORDER BY userNom, userPrenom");
			
				foreach ($users as $user) {
					$userID = $user["userID"];
					$userNom = stripslashes(utf8_encode($user["userNom"]));
					$userPrenom = stripslashes(utf8_encode($user["userPrenom"]));
					$userNaissance = $user["userNaissance"];
					
					$userSelect = $userID == $currentUser ? " selected" : "";
					
					echo "<option value='$userID' $userSelect>$userNom $userPrenom</option>";
				}
			?>
		</select>		
		
	</form>
	</div>
	
	<?PHP
		// Montre la partie "rendre" si un utilisateur est selectionné, sinon -> vide
		$display = $currentUser == "" ? "none":"";
		echo "<div style='display:$display;'>";
	?>
	
	
	<form method=post action="prets_post.php?action=rendre" target=_blank id='formulaire'>

		<?PHP 
			
			$user = $cnx->QueryRow("SELECT userNom, userPrenom, userNaissance FROM users WHERE userID=$currentUser");
			$nom 		= stripslashes(utf8_encode($user["userNom"]));
			$prenom 	= stripslashes(utf8_encode($user["userPrenom"]));
			$naissance 	= $user["userNaissance"];
			$formatDate 	= date("d/m/Y", strtotime($naissance));
							
							
			echo "<div id='fiche'>RENDRE <b>$nom $prenom</b> ($formatDate)</div>";				
							
			
			$prets = $cnx->QueryAll("
			SELECT exemplaires.exeID as exempID, manuelNiveau, manuelTitre, manuelEdition, matiereNom, editeurNom, exeEtat, exePrix, exeAchat, exeREF, exeCD
			FROM manuels, matieres, editeurs, exemplaires, prets
			WHERE 
				manuels.matiereID=matieres.matiereID AND
				manuels.editeurID=editeurs.editeurID AND
				exemplaires.manuelID=manuels.manuelID AND
				exemplaires.exeID=prets.exeID AND
				prets.userID=$currentUser;
				
				");
				
			if (count($prets)==0 ) {echo "Aucun exemplaire prêté ... Aucun exemplaire à rendre ;p";exit;}
			
			
			echo "<input type=submit id=post_form value='RENDRE'><br>";
			
			echo "<input type=hidden name=userID value=$currentUser>";


		?>	
			<input type=text size=14 maxlength=13 class='ref' id='exeREF' onchange="filterEXEREF($(this));"><span id='msg'></span>
			
		<?PHP


			
			echo "<br><h2>Livres rendus (<span id='nbrendus'>0</span>)</h2>";
			
			echo "<table id='rendus' class='bigtable alternate'>";
				echo "<th></th>";
				echo "<th>REF</th>";
				echo "<th>Titre</th>";
				echo "<th>CD</th>";
				echo "<th>Etat</th>";
				echo "<th>Status</th>";
				echo "<th></th>";
			echo "</table>";


							
			echo "<br><br><h2>Ses livres (" . count($prets) . ")</h2>";
			
			echo "<table class='bigtable alternate'>";
			
				echo "<th>REF</th>";
				echo "<th>Matière</th>";
				echo "<th>Editeur</th>";
				echo "<th>Niveau</th>";
				echo "<th>Titre</th>";
				echo "<th>Edition</th>";
				echo "<th>Achat</th>";
				echo "<th>Prix</th>";
				echo "<th>Etat<br>d'origine</th>";
				echo "<th>CD</th>";
				//echo "<th>Etat<br>rendu</th>";
					
				foreach ($prets as $pret) {
					$exeID = $pret['exempID'];
					$manuelNiveau = $pret['manuelNiveau'];
					$manuelTitre = $pret['manuelTitre'];
					$manuelEdition = $pret['manuelEdition'];
					$matiereNom = $pret['matiereNom'];
					$editeurNom = $pret['editeurNom'];
					$exeEtat = $pret['exeEtat'];	
					$exeREF = $pret['exeREF'];	
					$exePrix = $pret['exePrix'];	
					$exeAchat = $pret['exeAchat'];	
					$exeCD = $pret['exeCD'];
					
					switch ($exeEtat) {
						case 4 : $exeEtatLabel = "NEUF"; $etatClass="neuf"; break;
						case 3 : $exeEtatLabel = "BON"; $etatClass="bon"; break;
						case 2 : $exeEtatLabel = "MOYEN"; $etatClass="moyen"; break;
						case 1 : $exeEtatLabel = "PASSABLE"; $etatClass="passable"; break;
						case 0 : $exeEtatLabel = "PERDU"; $etatClass="perdu"; break;
					}
					
					$exeCDlabel = $exeCD==1 ? "<img src='construction/accept.png' height=15>" : "";
					
					echo "<tr>";
						echo "<td><input type=hidden name=exeID[] value=$exeID>$exeREF</td>";
						echo "<td>$matiereNom</td>";
						echo "<td>$editeurNom</td>";
						echo "<td>$manuelNiveau</td>";
						echo "<td>$manuelTitre</td>";
						echo "<td>$manuelEdition</td>";
						echo "<td>$exeAchat</td>";
						echo "<td>$exePrix</td>";
						echo "<td class='$etatClass'>$exeEtatLabel</td>";
						echo "<td>$exeCDlabel</td>";
						
					echo "</tr>";
					
				}
								
			echo "</table>";


		?>

	</form>
	
	</div>
</center>
