<?PHP
	include_once "config.php"; // on charge les param�tres bdd
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');	
	

	try {
		
		$dbh = new PDO("mysql:host=$host", $user, $pass);
		
		// La base existe
		if (!$dbh->exec ("CREATE DATABASE $db;") ) {
			
			// On se connecte � la base
			$cnx = new Sql ( $host, $user, $pass, $db );
					
		
			$requetes="";
		 
			$sql=file("database.sql"); // on charge le fichier SQL

			foreach($sql as $l){ // on nettoie le fichier en virant les commentaires
				if (substr(trim($l),0,2)!="--")	$requetes .= $l;
			}
			
			// on s�pare les requ�tes
			$reqs = split(";",$requetes);
		
		
			// on execute les requ�tes
			foreach($reqs as $req) {				
				$cnx->exec($req);
			}

			// On redirige vers la page d'index
			header ('location:index.php');
			
			
		}
		else {
			// La base n'existait pas
			// Elle est cr��e et on relance la proc�dure
			header ('location:install.php');
		}
		 

	} catch (PDOException $e) {
		die("DB ERROR: ". $e->getMessage());
		
	}
	
	
?>
