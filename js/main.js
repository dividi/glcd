// *********************************************************************************
//
//		FONCTIONS GENERALES
//
// *********************************************************************************

/*
* @name: AffichePage
* @param : touche frappée
* @return : rien
* @description : désactive postage sur touche entrée
* @reference : toutes les pages avec un filtre
*/	
function disableEnterKey(e){
	var key = e.which;

	if(key == 13) return false;
	else return true;
};
	
/*
* @name: AffichePage
* @param : string:variable, string:page
* @return : rien
* @description : Permet d'afficher dans un div le contenu d'une page (genre ajax)
* @reference : ???? encore utile ???
*/
function AffichePage(div_dest, page) {
	$(div_dest).set('load', {method: 'post'});	//On change la methode d'affichage de la page de GET à POST (en effet, avec GET il récupère la totalité du tableau get en paramètres pour ne pas dépasser la taille maxi d'une url)
	$(div_dest).load(page);
};


/*
* @name: getQueryVariable
* @param : string:variable
* @return : string:valeur de la variable
* @description : Permet de récupérer la valeur d'une variable de l'url
* @reference : menu.php
*/
function getQueryVariable(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		if (decodeURIComponent(pair[0]) == variable) {
			return decodeURIComponent(pair[1]);
		}
	}
}
		
		
/*
* @name: filter
* @param : string:phrase, string:tableid
* @return : rien
* @description : Permet de filtrer une table et éventuellement de marquer le nombre de lignes filtrées
* @reference : Presque toutes les pages
*/
function filter (phrase, tableid){
	
	phrase = phrase.replace(",","|");
	var data = phrase.split("|");
	var cells=$("#" + tableid + " td");
				
	if(data != "") {
		// On cache toutes les lignes
		cells.parent("tr").hide();
		// puis on filtre pour n'afficher que celles qui répondent au critère du filtre
		cells.filter(function() {
			return $(this).text().toLowerCase().indexOf(data) > -1;
		}).parent("tr").show();		
	} else {
		// On montre toutes les lignes
		cells.parent("tr").show();
	}
	
	if ($("#filtercount")) $("#filtercount").html( $("#" + tableid + " tr:visible").length -1 );
}	


/*
* @name: validation
* @param : queud
* @return : true si le formulaire est valide, sinon ... false
* @description : Permet de valider les input ayant la classe "valid" dans un formulaire
* @reference : Presque toutes les pages formulaires
*/

function validForm () {
	
	var valid = true;
	$('.validInfo').html("");
	
	$('.valid').each (function(){
		
		// test sur champ vide
		if ($(this).hasClass("nonvide")) {	
			if ( $(this).val() == "" ) {
				valid=false;
				$(this).after("<span class='validInfo'>*non vide </span>");
			}
		}
		
		
		// test sur uai
		if ($(this).hasClass("uai") && $(this).val() != "") {	
			if ( $(this).val().match(/^[0-9]{7}[A-Z]{1}$/) == null ) {
				valid=false;
				$(this).after("<span class='validInfo'>*uai en majuscules </span>");
			}
		}
		
		
		// test sur numérique
		if ($(this).hasClass("num") && $(this).val() != "") {	
			if ( $(this).val().match(/[0-9]/) == null ) {
				valid=false;
				$(this).after("<span class='validInfo'>*numérique </span>");
			}
		}
		
				
		// test sur majuscules
		if ($(this).hasClass("caps") && $(this).val() != "") {	
			if ( $(this).val().match(/[A-Z0-9]/) == null ) {
				valid=false;
				$(this).after("<span class='validInfo'>*en majuscules </span>");
			}
		}
		
		// test sur mail
		if ($(this).hasClass("mail") && $(this).val() != "") {	
			if ( $(this).val().match(/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/) == null ) {
				valid=false;
				$(this).after("<span class='validInfo'>*mail invalide </span>");
			}
		}	
		
		// test sur mail non cg13
		if ($(this).hasClass("mailnoncg13") && $(this).val() != "") {	
			if ( $(this).val().match(/^[A-Za-z0-9._%+-]+@cg13\.fr$/) != null ) {
				valid=false;
				$(this).after("<span class='validInfo'>*mail non cg13 </span>");
			}
		}		
		
		// test sur url
		if ($(this).hasClass("url") && $(this).val() != "") {	
			if ( $(this).val().match(/(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-=?]*)*\/?/) == null ) {
				valid=false;
				$(this).after("<span class='validInfo'>*url invalide </span>");
			}
		}	
		
		// test sur mac
		if ($(this).hasClass("mac") && $(this).val() != "") {	
			if ( $(this).val().match(/^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/i) == null ) {
				valid=false;
				$(this).after("<span class='validInfo'>*mac invalide </span>");
			}
		}		
	});
	
	return valid;	
}


// *********************************************************************************
//
//		FONCTIONS INIT
//
// *********************************************************************************


	
$(window).resize(function() {
	// Ajuste la taille de la liste des tâches
	$('#menu').height( $(window).height() );	
	$('#content').height( $(window).height() );	
	$('#content').width( $(window).width() -210 );
});	

	
$(function () {


	// Ajuste la taille de la liste des tâches
/*	$('#menu').height( $(window).height() );	
	$('#content').height( $(window).height() );
	$('#content').width( $(window).width() -210 );	*/



	// **************************************************************** Toggle the sidebar
	 $("#toggle-sidebar").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        
        // On stcke le param
        if ( localStorage.getItem("sidebar") == 0 ) { localStorage.setItem("sidebar", 1); }
		else { localStorage.setItem("sidebar", 0); }
        
    });
	
	// **************************************************************** show popover
	$('#help').popover('hide');
	
	// **************************************************************** show tooltip
/*	$('[data-toggle="tooltip"]').tooltip({ placement : 'bottom' });
	$('[data-toggle="tooltip-filtre"]').tooltip({ title : 'Filtrer le tableau', placement : 'bottom' });
	$('[data-toggle="tooltip-menu"]').tooltip({ title : 'Affiche ou Masquer le menu latéral', placement : 'bottom' });
*/

	// **************************************************************** AFFICHE L'AIDE EN LIGNE
	/*$('.help-button').click( function(e)  {
		$(".helpbox").toggle("fade");
	});*/
	
	// **************************************************************** CREATION / EDITION DANS UNE DIALOGBOX
	$('a.editbox').click(function(){

		var url = this.href;
		var title = this.title;
		
		var width = "auto";
		var height = "auto";
		var maxheight = "auto";
		var modal = false;
		
		if (url.match(/[&|?]width=([^&]+)/)) width = url.match(/[&|?]width=([^&]+)/)[1];
		if (url.match(/[&|?]height=([^&]+)/)) height = url.match(/[&|?]height=([^&]+)/)[1];
		if (url.match(/[&|?]maxheight=([^&]+)/)) maxheight = url.match(/[&|?]maxheight=([^&]+)/)[1];
		if (url.match(/[&|?]modal=([^&]+)/)) modal = url.match(/[&|?]modal=([^&]+)/)[1];
							
		var dialog = $("#dialog");
		if ($("#dialog").length == 0) {	dialog = $('<div id="dialog" style="display:hidden"></div>').appendTo('body');	} 

		// load remote content
		dialog.load(
			url,
			{},
			function(responseText, textStatus, XMLHttpRequest) {
				dialog.dialog({	title:title, width:width, height:height, modal:modal, stack: false});
				dialog.css('maxHeight', maxheight + "px"); //on applique une hauteur maximum
			}
		);
		
		return false;	//on ne suit pas le lien cliquable
		
	});
	
	
	// TOOLTIPS
	$('*[data-tip]').hover(function(){	
        // sur mouse over
        var title = $(this).attr('data-tip');
       
        $(this).data('tipText', title).removeAttr('title');
        $('<p class="tool-tip"></p>')
        .text(title)
        .appendTo('body')
        .fadeIn('slow');
	}, function() {
        // sur mouse out
        $(this).attr('title', $(this).data('tipText'));
        $('.tool-tip').remove();
	}).mousemove(function(e) {
        var mousex = e.pageX + 15; //Get X coordinates
        var mousey = e.pageY + 10; //Get Y coordinates
        $('.tool-tip')
        .css({ top: mousey, left: mousex })
	});


});
