<?PHP


	// fichier de creation / modif / suppr d'un éditeur
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');		
	
	
	
	// Cnx à  la base
	$cnx = new Sql($host, $user, $pass, $db);
	
	// Les LOGS
	$logsql = new log ("fichiers/log_sql.sql");
	$log = new log ("fichiers/log.txt");
	
	// on récupère les paramètres de l'url	
	$action 	= $_GET['action'];
		

	/**************** @@SUPPRESSION ********************/
	
	if ( $action == 'del' ) {
		
		$id = $_POST['id'];
	    $exeREF = $cnx->QueryOne ( "SELECT exeREF FROM exemplaires WHERE exeID=$id" );
	
		// Suppression de l'exemplaire de la base
		$req_suppr = "DELETE FROM exemplaires WHERE exeID=$id;";
		$cnx->Execute ( $req_suppr );
		$logsql->Insert( $req_suppr );
		
		echo "L'EXEMPLAIRE <b>$exeREF</b> a été supprimé.";	
		$log->Insert( "L'EXEMPLAIRE $exeREF a été supprimé." );		
	  
	}

	
	/**************** @@SUPPRESSION LOT ********************/
	
	if ( $action == 'dellot' ) {
		
		$lot = $_POST['lotid'];
		$lot_array = explode(";", $lot);
		
		foreach ($lot_array as $exe) {
			if ( $exe <> "" ) {
				$nb_prets = $cnx->QueryOne ( "SELECT COUNT(pretID) FROM prets WHERE exeID=$exe" );
				
				if (!$nb_prets) {		
					// Suppression de l'exemplaire de la base
					$req_suppr = "DELETE FROM exemplaires WHERE exeID=$exe;";
					$cnx->Execute ( $req_suppr );
					$logsql->Insert( $req_suppr );
				}
			}
		}
				
		echo "Suppression du lot d'EXEMPLAIRES";	
		$log->Insert( "Suppression du lot d'EXEMPLAIRES" );		
	  
	}
	
	
	
	/**************** @@MODIFICATION LOT ********************/
	
	if ( $action == 'modlot' ) {
		
		$etat = $_POST['lot_etat'];
		$prix = $_POST['lot_prix'];
		$achat = $_POST['lot_achat'];
		$cd = $_POST['lot_cd'];
		$flag = $_POST['lot_flag'];
				
		if ( !$etat && !$achat && !$prix && !$flag){ echo "Rien à modifier !";}	
		else {		
			$lot = $_POST['lotid'];
			$lot_array = explode(";", $lot);
						
			$sqletat = $etat<>"" ? " exeEtat=$etat," : "";
			$sqlcd = $cd<>"" ? " exeCD=$cd," : "";
			$sqlprix = $prix<>"" ? " exePrix=$prix," : "";
			$sqlachat = $achat<>"" ? " exeAchat='$achat'," : "";
			$sqlflag = $flag<>"" ? " exeFlag='$flag'," : "";
			
			$set = $sqletat . $sqlprix . $sqlachat . $sqlcd . $sqlflag;
			$set = preg_replace ("/[,]$/", "", $set);
			
			foreach ($lot_array as $exe) {
				if ( $exe <> "" ) {
					$req_upd = "UPDATE exemplaires SET $set WHERE exeID=$exe";
					$cnx->Execute ( $req_upd );
					$logsql->Insert( $req_upd );
				}
			}
			
			echo "Mise à jour du lot d'EXEMPLAIRES";	
			$log->Insert( "Mise à jour du lot d'EXEMPLAIRES" );
		}	  
	}
	
	
	
	/**************** @@MODIFICATION ********************/
	
	if ( $action == 'mod' ) {
		
		$etat = $_POST['etat'];
		$prix = $_POST['prix'];
		$achat = $_POST['achat'];
		$cd = $_POST['cd'];
		$flag = $_POST['flag'];
					
		$id = $_POST['id'];
				
		$sqletat = $etat<>"" ? " exeEtat=$etat," : "";
		$sqlprix = $prix<>"" ? " exePrix=$prix," : "";
		$sqlachat = $achat<>"" ? " exeAchat='$achat'," : "";
		$sqlcd = $cd<>"" ? " exeCD=$cd," : "";
		$sqlflag = $flag<>"" ? " exeFlag=$flag," : "";
			
		$set = $sqletat . $sqlprix . $sqlachat . $sqlcd . $sqlflag;
		$set = preg_replace ("/[,]$/", "", $set);

		$req_upd = "UPDATE exemplaires SET $set WHERE exeID=$id";
		$cnx->Execute ( $req_upd );
		$logsql->Insert( $req_upd );
			
		echo "Mise à jour de l'EXEMPLAIRE";	
		$log->Insert( "Mise à jour de l'EXEMPLAIRE" );
			  
	}
		
	
	/**************** @@INSERTION ********************/
	if ( $action == 'add' ) {

		$manuelID 	= $_POST ['manuelID'];
		$etat		= $_POST ['etat'];
		$prix		= $_POST ['prix'];
		$achat		= $_POST ['achat'];
		$cd			= $_POST ['cd'];

		/* 
		x : niveau 
		xx : matiere
		xx : editeur
		xx : edition
		xxxxxx : id
		
		code128 : 5ANNA06000001 
		*/
		

		for ($i = 0 ; $i < count($manuelID) ; $i++) {
		
			$manuelInfo = $cnx->QueryRow ("SELECT manuelNiveau, manuelTitre, manuelEdition, matiereREF, editeurREF FROM manuels, matieres, editeurs WHERE manuels.matiereID=matieres.matiereID AND manuels.editeurID=editeurs.editeurID AND manuelID= ".$manuelID[$i]."; ");
		
			$req_add = "INSERT INTO exemplaires ( exeEtat, manuelID, exePrix, exeAchat, exeCD ) VALUES ( " . $etat[$i] . ", " . $manuelID[$i] . "," . $prix[$i] . ", '" . $achat[$i] . "'," . $cd[$i] .")";
			$cnx->Execute ( $req_add );
			$logsql->Insert( $req_add );
			
			$lastID = $cnx->GetLastID();

			$prefixe = $manuelInfo['manuelNiveau'] . $manuelInfo['matiereREF'] . $manuelInfo['editeurREF'] . substr($manuelInfo['manuelEdition'], -2);
			$suffixe = str_pad("$lastID", 6, "0", STR_PAD_LEFT);
			
			$exeREF = $prefixe . $suffixe;

			$req_upd = "UPDATE exemplaires SET exeREF='$exeREF' WHERE exeID=$lastID";
			$cnx->Execute ( $req_upd );
			$logsql->Insert( $req_upd );
		
		}
		
		echo count($manuelID) . " MANUEL(S) ajouté(s).";
		$log->Insert( count($manuelID) . " MANUEL(S) ajouté(s)." );
		
	}
		
	
	/**************** @@CREATION UNIQUE ********************/
	if ( $action == 'create' ) {

		$manuelID 	= $_POST ['manuelID'];
		$etat		= $_POST ['etat'];
		$prix		= $_POST ['prix'];
		$achat		= $_POST ['achat'];
		$cd			= $_POST ['cd'];
		$prefixe	= $_POST ['prefixe'];
		$suffixe	= $_POST ['suffixe'];
		
		$exeREF = $prefixe . $suffixe;

		$existe = $cnx->QueryOne ("SELECT exeID FROM exemplaires WHERE exeREF='$exeREF'");
		
		if (!$existe) {
			$req_add = "INSERT INTO exemplaires ( exeREF, exeEtat, manuelID, exePrix, exeAchat, exeCD ) VALUES ( '$exeREF', $etat, $manuelID, $prix, '$achat', $cd)";
			$cnx->Execute ( $req_add );
			$logsql->Insert( $req_add );
			echo "Exemplaire ajouté.";
		}
		else {
			echo "Ce numéro d'exemplaire (exeREF) existe déjà...";
		}
		

	}	

?>

