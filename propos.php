<!--

	Page à propos

-->		
	
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
		<div class="container-fluid">
					
		<!-- Left part -->
		<ul class="nav navbar-nav">
			<li><a class="navbar-brand" href="#"></a></li>	
			<li class="navbar-title">A PROPOS</li>
		</ul>


		<!-- Right part -->
		<ul class="nav navbar-nav navbar-right">

			<!-- Help -->
			<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Qui a fait ça, pourquoi, la licence et tout et tout."><span class="glyphicon glyphicon-info-sign"></span> Info </a></li>
			
		</ul>
		</div>
	</nav>

	<div class="spacer"></div>	
		
		<style>
			
			.propos {margin:10px;}
			.p1 {margin:10px; font-size:14px;}
			.titre {font-size:32px; font-weight:900;}
			.soustitre {font-size:18px; }
			
		</style>
		
<div class=propos>
	<div class=titre>Logiciel de gestion des manuels scolaires</div>
	<div class=soustitre>Fait par DAVID LE VIAVANT</div>

	<div class=p1>
		Ce logiciel permet la gestion des prêts de manuels ou livres à des utilisateurs.<br>
		Son but premier est la gestion des manuels scolaires du COLLEGE GASTON DEFFERRE.<br>
		<br>
		Ce logiciel est fourni sous licence GNU/GPL (ici en <a href='licence_fra.txt' target=_blank>français</a> et en <a href='licence_uk.txt' target=_blank>anglais</a>).<BR>
		Ce logiciel est donné "tel quel", sans garantie d'aucune sorte.<br>
		
	</div>
	
</div>
