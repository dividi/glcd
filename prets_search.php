<?PHP


	// fichier ajax pour les prêts
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');	

if($_REQUEST) {

	// cnx
	$cnx = new Sql($host, $user, $pass, $db);

	$ref 	= $_REQUEST['ref'];
	
	$deja_prete = $cnx->QueryOne("SELECT userID FROM prets, exemplaires WHERE exemplaires.exeID=prets.exeID AND exeREF='$ref' ");
	
	if ($deja_prete) {
		echo "<span style='color:red;'>L'exemplaire <b>$ref</b> est DEJA prêté !</span>";
	}
	else {
		
		$exemplaire = $cnx->QueryRow ( "
			SELECT exeID, manuelNiveau, manuelTitre, manuelEdition, matiereNom, editeurNom, exeEtat, exeREF
			FROM manuels, matieres, editeurs, exemplaires
			WHERE 
				manuels.matiereID=matieres.matiereID AND
				manuels.editeurID=editeurs.editeurID AND
				exemplaires.manuelID=manuels.manuelID AND
				exeREF = '$ref'" );
				
				$exeID = $exemplaire['exeID'];
				$manuelNiveau = $exemplaire['manuelNiveau'];
				$manuelTitre = $exemplaire['manuelTitre'];
				$manuelEdition = $exemplaire['manuelEdition'];
				$matiereNom = $exemplaire['matiereNom'];
				$editeurNom = $exemplaire['editeurNom'];
				$exeEtat = $exemplaire['exeEtat'];

		
		echo "<input type=hidden value='$exeID' name='exeID[]'>";
		echo "<span>  $matiereNom $editeurNom ($manuelNiveau ème) - $manuelTitre ($manuelEdition)</span>";
		
		echo "<select name='etat[]' class='etat'>";
			echo "<option value=4". if ($exeEtat == 4) echo " selected"; .">4 - NEUF</option>";
			echo "<option value=3". if ($exeEtat == 3) echo " selected"; .">3 - BON</option>";
			echo "<option value=2". if ($exeEtat == 2) echo " selected"; .">2 - MOYEN</option>";
			echo "<option value=1". if ($exeEtat == 1) echo " selected"; .">1 - PASSABLE</option>";
		echo "</select>";
		
		echo "<a href='#' class='delete' onclick='deleteEXE(\"$ref\");'><img src='construction/delete.png'></a>";
	}
	
}
?>
