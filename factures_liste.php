<!--

	Liste des factures

-->


	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
		<div class="container-fluid">
		
		<!-- Left part -->
		<ul class="nav navbar-nav">
			<li><a class="navbar-brand" href="#"></a></li>	
			<li class="navbar-title">FACTURES</li>
		</ul>


		<!-- Right part -->
		<ul class="nav navbar-nav navbar-right">

			<!-- Help -->
			<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="D'ici on peut consulter l'historique des factures."><span class="glyphicon glyphicon-info-sign"></span> Info </a></li>
			
			<!-- Montre les factures nulles -->
			<li><a href="#"  id="factures_nulles" title="Montre les factures nulles."><span class="glyphicon glyphicon-search"></span> factures nulles </a></li>

			<!-- Filter -->
			<li data-toggle="tooltip-filtre">
				<form class="navbar-form" role="search">
					<div class="form-group">
						<input class="form-control" placeholder=" filtrer" name="filt" id="filt" onKeyPress="return disableEnterKey(event)" onkeyup="filter(this.value, 'factures_table');" type="text" value=<?PHP echo $_GET['filter'];?>> 
						&nbsp;<span class="navbar-text navbar-right" id="filtercount" title="Nombre de lignes filtrées"></span>
					</div>
				</form>					
			</li>

		</ul>
		</div>
	</nav>

	<div class="spacer"></div>	
	
	<center>

	<table class="table table-condensed hover" id="factures_table">
	<thead><tr>
		<th>Classe<br> <input class='filter' data-col="Classe"/></th>
		<th>Eleve<br> <input class='filter' data-col="Eleve"/></th>
		<th>Montant<br> <input class='filter' data-col="Montant"/></th>
		<th>Payée<br> <input class='filter' data-col="Payée"/></th>
		<th>Date<br> <input class='filter' data-col="Date"/></th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
	</tr></thead>

<?PHP
	$dossier = opendir("factures");
	while ( $facture = readdir($dossier) ) {
		

		if (pathinfo($facture, PATHINFO_EXTENSION) == "html") {
			
			$explode = explode("_", pathinfo($facture, PATHINFO_FILENAME));
			
			if ($explode[3] == "" || $explode[3] == "0") {
				$showNull = "hide";
			}
			else {
				$showNull = "";	
			}

			if ( $explode[4] && $explode[4] == "P" ) {
				$payee = "PAYEE";
			}
			else { $payee = ""; }
			


			echo "<tr class='$showNull showNull'>";
			
				echo "<td>";
					echo $explode[0];
				echo "</td>";
				
				echo "<td>";
					echo $explode[1] . " " . $explode[2];
				echo "</td>";
	
				echo "<td>";
					echo $explode[3];
				echo "</td>";
	
				echo "<td>";
					echo $payee;
				echo "</td>";


				echo "<td>";
					echo date("d-m-Y H:i", filectime("factures/" . $facture) );
				echo "</td>";
				

				echo "<td><a href='#' id='$facture' class='payer-facture'><span class='glyphicon glyphicon-euro table-icon'></span></a></td>";
				
				echo "<td><a href='#' id='$facture' class='delete-facture'><span class='glyphicon glyphicon-remove-sign table-icon'></span></a></td>";
				
				echo "<td><a href='factures/$facture' target=_blank><span class='glyphicon glyphicon-eye-open table-icon'></span></a></td>";
			
			echo "</tr>";
	
		}
	}
	closedir($dossier);


?>

<script>
	// Filtre rémanent
	filter ( $('#filt').val(), 'factures_table' );

	$(function(){

			// Filter 
	if ($('.filter')) {
		$('.filter').multifilter();
	}


		$(".payer-facture").click(function(){

			var validation = confirm('Voulez-vous vraiment PAYER la facture  ' + $(this).prop('id') + ' ?');
			
			if (validation) {
				console.log ($(this).prop('id'));


				var request = $.ajax({
					type: "GET",
					url: 'factures_post.php',
					data: "facture=" + $(this).prop('id') + "&type=payer",
					dataType: "html"
				 });
				 
				 request.done(function(msg) {
					$('#target').html(msg);
					window.setTimeout("document.location.href='index.php?page=factures'", 1500);
				 });

			}

		});


		$(".delete-facture").click(function(){

			var validation = confirm('Voulez-vous vraiment SUPPRIMER la facture  ' + $(this).prop('id') + ' ?');
			
			if (validation) {
				console.log ($(this).prop('id'));


				var request = $.ajax({
					type: "GET",
					url: 'factures_post.php',
					data: "facture=" + $(this).prop('id') + "&type=delete",
					dataType: "html"
				 });
				 
				 request.done(function(msg) {
					$('#target').html(msg);
					window.setTimeout("document.location.href='index.php?page=factures'", 1500);
				 });

			}

		});



		$("#factures_nulles").click(function(){

			$.each ($(".hide"), function(key, value){

				$(this).removeClass("hide");

			});

		});

	});


</script>
