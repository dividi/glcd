<!--

	Page import des utilisateurs

-->		
		
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="header">
			<div class="container-fluid">
						
			<!-- Left part -->
			<ul class="nav navbar-nav">
				<li><a class="navbar-brand" href="#"></a></li>	
				<li class="navbar-title">IMPORT des UTILISATEURS</li>
			</ul>


			<!-- Right part -->
			<ul class="nav navbar-nav navbar-right">

				<!-- Help -->
				<li><a href="#"  id="help"  data-container="body" data-toggle="popover popover-dismiss" data-placement="bottom" data-content="Cette page permet d'importer des élèves par un fichier CSV."><span class="glyphicon glyphicon-info-sign"></span> Info </a></li>
				
			</ul>
			</div>
		</nav>

	<div class="spacer"></div>	


<center>

<form enctype="multipart/form-data">
     <input type="hidden" name="MAX_FILE_SIZE" value="10000000">
	 
	 <table width=400 align=center cellpadding=10px>
		<tr>
			<td>Fichier CSV</td>
			<td><input type="file" name="myfile">  </td>
		</tr>
		<tr>
			<td colspan=2><br><input type="button" name="envoyer" value="Envoyer le fichier"></td>
		</tr>
	 </table>
	 
      
</form>

<br>
! Les classes seront créées automatiquement !
<br><br>

<h4><b>FORMAT du FICHIER CSV :</b></h4>
<h6>
Le fichier CSV doit être créé à travers une extraction SIECLE <br>
L'extraction s'appelle <b>EXP_Liste_des_eleves_par_division.csv</b>
</h6>


</center>



<script>
	
//---------------------------------------- Post file par Ajax (fonction par olanod : http://stackoverflow.com/users/931340/olanod)
$(':button').click(function(){
    var formData = new FormData($('form')[0]);
    $.ajax({
        url: 'users_import_post.php',  //server script to process data
        type: 'POST',
        xhr: function() {  // custom xhr
            var myXhr = $.ajaxSettings.xhr();
            return myXhr;
        },
		// Data du formulaire
        data: formData,
        //Options to tell JQuery not to process data or worry about content-type
        cache: false,
        contentType: false,
        processData: false,
        complete : function(res) {
			$('#targetback').show(); $('#target').show();
			$('#target').html(res.responseText);
			window.setTimeout("document.location.href='index.php?page=users'", 2500);
		}
    });
});

</script>
