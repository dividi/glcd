<?PHP


	// fichier de creation / modif / suppr d'un éditeur
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');		
	
	
	
	// Cnx à  la base
	$cnx = new Sql($host, $user, $pass, $db);
	
	// Les LOGS
	$logsql = new log ("fichiers/log_sql.sql");
	$log = new log ("fichiers/log.txt");
	
	// on récupère les paramètres de l'url	
	$action 	= $_GET['action'];
		

	/**************** @@SUPPRESSION ********************/
	
	if ( $action == 'del' ) {
		
		$id = $_POST['id'];

		$manuel = $cnx->QueryRow ( "SELECT manuelNiveau, manuelTitre, manuelEdition, matiereNom, editeurNom FROM manuels, matieres, editeurs WHERE manuels.matiereID=matieres.matiereID AND manuels.editeurID=editeurs.editeurID AND manuelID=$id" );
		
		$manuelNiveau 	= $manuel["manuelNiveau"];
		$manuelTitre 	= $manuel["manuelTitre"];
		$manuelEdition 	= $manuel["manuelEdition"];
		$matiereNom 	= $manuel["matiereNom"];
		$editeurNom 	= $manuel["editeurNom"];
		
		// Suppression de l'utilisateur de la base
		$req_suppr = "DELETE FROM manuels WHERE manuelID=$id;";
		$cnx->Execute ( $req_suppr );
		$logsql->Insert( $req_suppr );
		
		echo "Le MANUEL <b>$editeurNom $manuelTitre ($manuelEdition) en $matiereNom</b> a été supprimé.";	
		$log->Insert( "Le MANUEL $editeurNom $manuelTitre ($manuelEdition) en $matiereNom a été supprimé." );		
	  
	}

	/**************** @@MODIFICATION ********************/	
	if ( $action == 'mod' ) {
	
		$id     	= $_POST ['id'];
		$ref 		= $_POST ['ref'];
		$niveau 	= $_POST ['niveau'];
		$titre		= addslashes($_POST ['titre']);
		$editeur	= $_POST ['editeur'];
		$edition	= $_POST ['edition'];
		$matiere	= $_POST ['matiere'];
	
		// on récupére les anciennes valeurs du compte pour les logs
		$OLDmanuelTitre = $cnx->QueryOne("SELECT manuelTitre FROM manuels WHERE manuelID=$id");
		
		$req_modif = "UPDATE manuels SET manuelNiveau='$niveau', manuelREF='$ref', manuelTitre='$titre', manuelEdition='$edition', matiereID=$matiere, editeurID=$editeur WHERE manuelID=$id";
		$cnx->Execute ( $req_modif );
		$logsql->Insert( $req_modif );
	
		echo "Le MANUEL <b>$OLDmanuelTitre</b> a été modifié.";
		$log->Insert( "Le MANUEL $OLDmanuelTitre a été modifié." );
	}
	
	/**************** @@INSERTION ********************/
	if ( $action == 'add' ) {
	
		$ref 		= $_POST ['ref'];
		$niveau 	= $_POST ['niveau'];
		$titre		= addslashes($_POST ['titre']);
		$editeur	= $_POST ['editeur'];
		$edition	= $_POST ['edition'];
		$matiere	= $_POST ['matiere'];
		
		$req_add = "INSERT INTO manuels ( manuelNiveau, manuelTitre, manuelEdition, matiereID, editeurID, manuelREF ) VALUES ( '$niveau', '$titre', '$edition', $matiere, '$editeur', '$ref')";
		$cnx->Execute ( $req_add );
		$logsql->Insert( $req_add );
		
		echo "Le MANUEL <b>" . stripslashes($titre) . "</b> a été créé.";
		$log->Insert( "Le MANUEL " . stripslashes($titre) . " a été créé." );
	}
	


?>


