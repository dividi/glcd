<?PHP
	#formulaire d'ajout et de modification
	#des matieres !
	
	// lib
	include ('config.php');	// fichiers de configuration des bases de données
	require_once ('fonctions.php');
	include_once ('class/Log.class.php');
	include_once ('class/Sql.class.php');
	
?>



<script type="text/javascript"> 


	$(function() {	
				
		// **************************************************************** POST AJAX FORMULAIRES
		$("#post_form").click(function(event) {

			/* stop form from submitting normally */
			event.preventDefault(); 
			
			if ( validForm() == true) {
			
				// Permet d'avoir les données à envoyer
				var dataString = $("#formulaire").serialize();
				
				// action du formulaire
				var url = $("#formulaire").attr( 'action' );
				
				var request = $.ajax({
					type: "POST",
					url: url,
					data: dataString,
					dataType: "html"
				 });
				 
				 request.done(function(msg) {
					$('#dialog').dialog('close');
					$('#targetback').show(); $('#target').show();
					$('#target').html(msg);
					window.setTimeout("document.location.href='index.php?page=matieres&filter=" + $('#filt').val() + "'", 2500);
				 });
			}			 
		});	
	});
</script>

<?PHP

	// connexion à la base de données
	$cnx 	= new Sql ($host, $user,$pass, $db);
	
	$action = $_GET['action'];

	
	
	
	#***************************************************************************
	# 				@@ CREATION
	#***************************************************************************
	
	if ( $action == 'add' ) {	// Formulaire vierge de création

		?>
		
		<script>
			// Donne le focus au premier champ du formulaire
			$('#nom').focus();
		</script>

		<form action="matieres_post.php?action=add" method="post" name="post_form" id="formulaire">
			<center>
			<table class="formtable" >
				
				<tr>
					<TD>REF *</TD>
					<TD><input type=text name=ref id=ref size=2 maxlength=2 class='valid nonvide'></TD>
				</tr>
				
				<tr>
					<TD>Nom *</TD>
					<TD><input type=text name=nom id=nom class='valid nonvide'></TD>
				</tr>
				
			</table>

			<br>
			<input id='post_form' type=submit value='Ajouter MATIERE'>
			</center>

		</FORM>
				

		<?PHP
		
	} 
	
	
	#***************************************************************************
	# 				@@ MODIFICATION
	#***************************************************************************
	
	if ($action == 'mod') {
			
		$id = $_GET['id'];
				
		// Requete pour récupérer les données des champs à modifier
		$matiere_a_modifier = $cnx->QueryRow ( "SELECT matiereREF, matiereNom FROM matieres WHERE matiereID=$id" );		
		
		// valeurs à affecter aux champs
		$matiereREF	= $matiere_a_modifier["matiereREF"];
		$matiereNom	= $matiere_a_modifier["matiereNom"];
		
		?>
		
		<script>
			// Donne le focus au premier champ du formulaire
			$('#nom').focus();
		</script>
		
		<form action="matieres_post.php?action=mod" method="post" name="post_form" id="formulaire">
			<input type=hidden name=id value=<?PHP echo $id;?> >
			<center>
			<table class="formtable">
			
				<tr>
					<TD>REF *</TD>
					<TD><input type="text" name="ref" class='valid nonvide' id="nom" size=2 maxlength=2 value= "<?PHP echo $matiereREF; ?>" 	/></TD>
				</tr>
				
				<tr>
					<TD>Nom *</TD>
					<TD><input type="text" name="nom" class='valid nonvide' value= "<?PHP echo $matiereNom; ?>"	/></TD>
				</tr>
				
			</table>
			
			<br>
			<input type=submit value='Modifier matiere' id="post_form">

			</center>

		</FORM>
		
		<?PHP

	}

	
	#***************************************************************************
	# 				@@ SUPPRESSION
	#***************************************************************************
	
	if ($action == 'del') {

		$id = $_GET['id'];
		$matiereNom = $cnx->QueryOne ( "SELECT matiereNom FROM matieres WHERE matiereID=$id" );

		$nb_manuels = $cnx->QueryOne ( "SELECT COUNT(manuelID) FROM manuels WHERE matiereID=$id" );

		if ($nb_manuels > 0) {echo "<h3>Vous ne pouvez pas supprimer l'matiere <u>$matiereNom</u> : <br>Des manuels sont associés à cet éditeur !</h3>"; exit();}

		echo "Voulez vous vraiment supprimer l'éditeur $matiereNom ?";
	?>	
		<center><br><br>
		<form action="matieres_post.php?action=del" method="post" name="post_form" id='formulaire'>
			<input type=hidden value="<?PHP echo $id;?>" name="id">
			<input type=submit value='Supprimer' id="post_form">
			<input type=button onclick="$('#dialog').dialog('close');" value='Annuler'>
		</form>
		</center>
		
	<?PHP	
	}
	
	
?>		
