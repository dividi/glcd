<?PHP
	#formulaire d'ajout et de modification
	#des élèves !
	
	// lib
	include ('config.php');	// fichiers de configuration des bases de données
	require_once ('fonctions.php');
	include_once ('class/Log.class.php');
	include_once ('class/Sql.class.php');
	
?>



<script type="text/javascript"> 


	$(function() {	
				
		// **************************************************************** POST AJAX FORMULAIRES
		$("#post_form").click(function(event) {

			/* stop form from submitting normally */
			event.preventDefault(); 
			
			if ( validForm() == true) {
			
				// Permet d'avoir les données à envoyer
				var dataString = $("#formulaire").serialize();
				
				// action du formulaire
				var url = $("#formulaire").attr( 'action' );
				
				var request = $.ajax({
					type: "POST",
					url: url,
					data: dataString,
					dataType: "html"
				 });
				 
				 request.done(function(msg) {
					$('#dialog').dialog('close');
					$('#targetback').show(); $('#target').show();
					$('#target').html(msg);
					window.setTimeout("document.location.href='index.php?page=users&filter=" + $('#filt').val() + "'", 2500);
				 });
			}			 
		});	
	});
</script>

<?PHP

	// connexion à la base de données
	$cnx = new Sql ($host, $user,$pass, $db);
	
	$action = $_GET['action'];

	
	
	
	#***************************************************************************
	# 				@@ CREATION
	#***************************************************************************
	
	if ( $action == 'add' ) {	// Formulaire vierge de création

		?>
		
		<script>
			// Donne le focus au premier champ du formulaire
			$('#nom').focus();
		</script>

		<form action="users_post.php?action=add" method="post" name="post_form" id="formulaire">
			<center>
			<table class="formtable" >
				
				<tr>
					<TD>Nom *</TD>
					<TD><input type=text name=nom id=nom class='valid nonvide'></TD>
				</tr>
				
				<tr>
					<TD>Prénom *</TD>
					<TD><input type=text name=prenom id=prenom class='valid nonvide'></TD>
				</tr>
				
				<tr>
					<TD>Naissance *</TD>
					<TD><input type=date name=naissance id=naissance class='valid nonvide'></TD>
				</tr>
				
				<tr>
					<TD>CLASSE *</TD>
					<TD>
						<select name=classe>
							<?PHP
								$classes = $cnx->QueryAll("SELECT classeID, CONCAT(classeNiveau, classeNom) as classe FROM classes ORDER BY classe");
							
								foreach ($classes as $classe) {
									$classeID = $classe["classeID"];
									$classe = $classe["classe"];
									
									echo "<option value=$classeID>$classe</option>";
								}
							?>
						</select>
					</TD>
				</tr>					
				
			</table>

			<br>
			<input id='post_form' type=submit value='Ajouter ELEVE'>
			</center>

		</FORM>
				

		<?PHP
		
	} 
	
	
	#***************************************************************************
	# 				@@ MODIFICATION
	#***************************************************************************
	
	if ($action == 'mod') {
			
		$id = $_GET['id'];
				
		// Requete pour récupérer les données des champs à modifier
		$user_a_modifier = $cnx->QueryRow ( "SELECT userID, userNom, userPrenom, userNaissance, userDette, users.classeID as classe FROM users, classes WHERE users.classeID=classes.classeID AND userID=$id" );		
		
		// valeurs à affecter aux champs
		$userNom		= $user_a_modifier["userNom"];
		$userPrenom		= $user_a_modifier["userPrenom"];
		$userNaissance	= $user_a_modifier["userNaissance"];
		$userClasseID	= $user_a_modifier["classe"];
		$userDette		= $user_a_modifier["userDette"];

		?>
		
		<script>
			// Donne le focus au premier champ du formulaire
			$('#nom').focus();
		</script>
		
		<form action="users_post.php?action=mod" method="post" name="post_form" id="formulaire">
			<input type=hidden name=id value=<?PHP echo $id;?> >
			<center>
			<table class="formtable">
			
				<tr>
					<TD>Nom *</TD>
					<TD><input type=text name=nom id=nom class='valid nonvide' value='<?PHP echo $userNom;?>'></TD>
				</tr>
				
				<tr>
					<TD>Prénom *</TD>
					<TD><input type=text name=prenom id=prenom class='valid nonvide' value='<?PHP echo $userPrenom;?>'></TD>
				</tr>
				
				<tr>
					<TD>Naissance *</TD>
					<TD><input type=date name=naissance id=naissance class='valid nonvide' value='<?PHP echo $userNaissance;?>'></TD>
				</tr>
				
				<tr>
					<TD>Classe *</TD>
					<TD>
						<select name=classe>
							<?PHP
								$classes = $cnx->QueryAll("SELECT classeID, CONCAT(classeNiveau, classeNom) as classe FROM classes ORDER BY classe");
									
								foreach ($classes as $classe) {
									$classeID = $classe["classeID"];
									$classe = $classe["classe"];
									
									$select_classe = $classeID == $userClasseID ? " selected" : "";
									
									echo "<option value='$classeID' $select_classe>$classe</option>";
								}
							?>
						</select>
					</TD>
				</tr>
				
								
				<tr>
					<TD>Dette</TD>
					<TD><input type=text name=dette id=dette class='' value='<?PHP echo $userDette;?>'></TD>
				</tr>
				
			</table>
			
			<br>
			<input type=submit value='Modifier ELEVE' id="post_form">

			</center>

		</FORM>
		
		<?PHP

	}

	
	#***************************************************************************
	# 				@@ SUPPRESSION
	#***************************************************************************
	
	if ($action == 'del') {

		$id = $_GET['id'];
		$eleve = $cnx->QueryRow ( "SELECT userNom, userPrenom, userNaissance, classeNom FROM users, classes WHERE users.classeID=classes.classeID AND userID=$id" );
	
		$userNom 		= $eleve["userNom"];
		$userPrenom 	= $eleve["userPrenom"];
		$userNaissance 	= $eleve["userNaissance"];
		$classeNom 		= $eleve["classeNom"];
	
		$prets = $cnx->QueryOne ( "SELECT COUNT(pretID) FROM prets WHERE userID=$id" );

		if ($prets > 0) {echo "<h3>Vous ne pouvez pas supprimer l'ELEVE <u>$userNom $userPrenom</u> : <br>Des manuels sont prêtés à cet élève !</h3>"; exit();}

		echo "Voulez vous vraiment supprimer l'élève $userNom $userPrenom ?";
	?>	
		<center><br><br>
		<form action="users_post.php?action=del" method="post" name="post_form" id='formulaire'>
			<input type=hidden value="<?PHP echo $id;?>" name="id">
			<input type=submit value='Supprimer' id="post_form">
			<input type=button onclick="$('#dialog').dialog('close');" value='Annuler'>
		</form>
		</center>
		
	<?PHP	
	}
	

	
	#***************************************************************************
	# 				@@ LISTE DES MANUELS
	#***************************************************************************
	
	if ($action == 'liste') {	
		
		$userID = $_GET['id'];
		
		$liste = $cnx->QueryAll("SELECT exemplaires.exeID as exempID, manuelNiveau, manuelTitre, manuelEdition, matiereNom, editeurNom, exeEtat, exeREF, exeCD, exeRendu 
			FROM manuels, matieres, editeurs, exemplaires, prets, users
			WHERE 
				manuels.matiereID=matieres.matiereID AND
				manuels.editeurID=editeurs.editeurID AND
				exemplaires.manuelID=manuels.manuelID AND
				prets.userID=users.userID AND
				prets.exeID=exemplaires.exeID AND
				prets.userID=$userID 
			ORDER BY matiereNom;
		");
		
		echo "<br>";
	echo "<div class='table-responsive'>";
		echo "<table class='table table-condensed alternate'>";
		
		echo "
			<th>Matière</th>
			<th>Niveau</th>
			<th>Editeur</th>
			<th>Titre</th>
			<th>Edition</th>
			<th>REF</th>
			<th>CD</th>
			<th>Etat</th>
			<th>Rendu</th>
		
		";
		
		foreach ($liste as $exe) {
		
			$manuelNiveau = $exe['manuelNiveau'];
			$manuelTitre = $exe['manuelTitre'];
			$manuelEdition = $exe['manuelEdition'];
			$matiereNom = $exe['matiereNom'];
			$editeurNom = $exe['editeurNom'];
			$exeEtat = $exe['exeEtat'];
			$exeREF = $exe['exeREF'];
			$exeCD = $exe['exeCD'];
			$exeRendu = $exe['exeRendu'];
			
			switch ($exeEtat) {
				case 4 : $exeEtatLabel = "NEUF"; $etatClass="neuf"; break;
				case 3 : $exeEtatLabel = "BON"; $etatClass="bon"; break;
				case 2 : $exeEtatLabel = "MOYEN"; $etatClass="moyen"; break;
				case 1 : $exeEtatLabel = "PASSABLE"; $etatClass="passable"; break;
				case 0 : $exeEtatLabel = "PERDU"; $etatClass="perdu"; break;
			}	
		
			$exeCDlabel = $exeCD==1 ? "<span class='glyphicon glyphicon-ok-sign color-green'></span>" : "";
			$exeRendulabel = $exeRendu==1 ? "<span class='glyphicon glyphicon-ok-sign color-green'></span>" : "<span class='glyphicon glyphicon-remove-sign color-red'></span>";
			
			echo "<tr>";
				echo "<td>$matiereNom</td>";
				echo "<td>$manuelNiveau</td>";
				echo "<td>$editeurNom</td>";
				echo "<td>$manuelTitre</td>";
				echo "<td>$manuelEdition</td>";
				echo "<td>$exeREF</td>";
				echo "<td>$exeCDlabel</td>";
				echo "<td class='$etatClass'>$exeEtatLabel</td>";
				echo "<td>$exeRendulabel</td>";
			echo "</tr>";
			
		}
		
		echo "</table>";
	echo "</div>";	
		
		
	}
?>		
