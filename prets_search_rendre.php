<?PHP


	// fichier ajax pour les prêts
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');	

if($_REQUEST) {

	// cnx
	$cnx = new Sql($host, $user, $pass, $db);

	$ref 	= $_REQUEST['ref'];
	$userID	= $_REQUEST['userid'];
	
	$proprietaire = $cnx->QueryOne("SELECT userID FROM prets, exemplaires WHERE exemplaires.exeID=prets.exeID AND exeREF='$ref'");
	
	// exemplaire rendu
	$exemplaire = $cnx->QueryRow ( "
		SELECT exeID, manuelNiveau, manuelTitre, manuelEdition, matiereNom, editeurNom, exeEtat, exeREF, exeCD
		FROM manuels, matieres, editeurs, exemplaires
		WHERE 
			manuels.matiereID=matieres.matiereID AND
			manuels.editeurID=editeurs.editeurID AND
			exemplaires.manuelID=manuels.manuelID AND
			exeREF = '$ref'" );
			
			$exeID = $exemplaire['exeID'];
			$manuelNiveau = $exemplaire['manuelNiveau'];
			$manuelTitre = $exemplaire['manuelTitre'];
			$manuelEdition = $exemplaire['manuelEdition'];
			$matiereNom = $exemplaire['matiereNom'];
			$editeurNom = $exemplaire['editeurNom'];
			$exeEtat = $exemplaire['exeEtat'];
			$exeCD = $exemplaire['exeCD'];

		
		echo "<td><input type=hidden value='$exeID' name='renduID[]'></td>";
		echo "<td>$ref</td>";
		echo "<td><span>  $matiereNom $editeurNom ($manuelNiveau ème) - $manuelTitre ($manuelEdition)</span></td>";
		
		echo "<td><select name='cd[]' class='cd'>";
			echo "<option value=1"; if ($exeCD == 1) echo " selected"; echo ">OUI</option>";
			echo "<option value=0"; if ($exeCD == 0) echo " selected"; echo ">NON</option>";
		echo "</select></td>";
		
		echo "<td><select name='etat[]' class='etat'>";
			echo "<option value=4"; if ($exeEtat == 4) echo " selected"; echo ">4 - NEUF</option>";
			echo "<option value=3"; if ($exeEtat == 3) echo " selected"; echo ">3 - BON</option>";
			echo "<option value=2"; if ($exeEtat == 2) echo " selected"; echo ">2 - MOYEN</option>";
			echo "<option value=1"; if ($exeEtat == 1) echo " selected"; echo ">1 - PASSABLE</option>";
		echo "</select></td>";
		
		
		if ($proprietaire <> $userID) {
			
			if ($proprietaire) $proprietaire_nom = $cnx->QueryOne ("SELECT CONCAT(userNom, ' ', userPrenom) as nom FROM users WHERE userID=$proprietaire ");
			else $proprietaire_nom = "stock";
			
			if ($proprietaire == "") $proprietaire_nom = "stock";
			
			echo "<td><input type='hidden' name='paslesien[]' value='$proprietaire'><span style='color:red;'>Pas le propriétaire ($proprietaire_nom) </span>";
			
			$exemplaire_sans_random = substr($ref, 0, 7);
			$exeSemblables = $cnx->QueryAll("SELECT exemplaires.exeID as id, exeREF FROM prets, exemplaires WHERE exemplaires.exeID=prets.exeID AND userID=$userID AND exeREF LIKE '$exemplaire_sans_random%'");
			
			echo "<br>Echange : ";
			
			echo "<SELECT name='echange[]'>";
				echo "<OPTION value=''>NE PAS ECHANGER</OPTION>";
				
				foreach ($exeSemblables as $exe) {
					$semblableID = $exe['id'];
					$semblableREF = $exe['exeREF'];
					echo "<OPTION value=$semblableID>$semblableREF</OPTION>";
				}
			
			echo "</SELECT>";
			
			
			echo "</td>";
					
		}
		else 
			echo "<td><input type='hidden' name='paslesien[]' value=''><SELECT name='echange[]' style='display:none;'><OPTION value=''></OPTION></SELECT>OK</td>";
		
		echo "<td><a href='#' class='delete' onclick='deleteEXE(\"$ref\");'><img class='icon' src='construction/delete.png'></a></td>";
		
}
?>
