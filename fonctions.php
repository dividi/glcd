<?PHP

// convert html to plain text
function html2text ($text) {

	$search = ARRAY ("'<script[^>]*?>.*?</script>'si",  // Strip out javascript
					 "'<[/!]*?[^<>]*?>'si",          // Strip out HTML tags
					 "'([rn])[s]+'",                // Strip out white space
					 "'&(quot|#34);'i",                // Replace HTML entities
					 "'&(amp|#38);'i",
					 "'&(lt|#60);'i",
					 "'&(gt|#62);'i",
					 "'&(nbsp|#160);'i",
					 "'&(iexcl|#161);'i",
					 "'&(cent|#162);'i",
					 "'&(pound|#163);'i",
					 "'&(copy|#169);'i",
					 "'&#(d+);'e");                    // evaluate as php
	 
	$replace = ARRAY ("",
					 "",
					 "\1",
					 "\"",
					 "&",
					 "<",
					 ">",
					 " ",
					 CHR(161),
					 CHR(162),
					 CHR(163),
					 CHR(169),
					 "chr(\1)");
 
	return PREG_REPLACE($search, $replace, $text);

}


// Permet de tronquer une chaine de caractères
function TruncateIt ($ch, $limite, $suffixe='') {
	if ( strlen($ch) > $limite && strlen($ch) > 0 ) { 
		return substr(html2text($ch), 0, $limite) . ' ' . $suffixe;
	} else {
		return $ch;
	}
}

// Vire les accents
function stripAccents($string){
	return str_replace( array('à','á','â','ã','ä', 'ç', 'è','é','ê','ë', 'ì','í','î','ï', 'ñ', 'ò','ó','ô','õ','ö', 'ù','ú','û','ü', 'ý','ÿ', 'À','Á','Â','Ã','Ä', 'Ç', 'È','É','Ê','Ë', 'Ì','Í','Î','Ï', 'Ñ', 'Ò','Ó','Ô','Õ','Ö', 'Ù','Ú','Û','Ü', 'Ý'), array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i','i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A','A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U','U','U', 'Y'), $string); 
}


// redimension par le grand côté
function RedimByBigSide ($pic, $destpic, $new_size) {	//$new_side= 100 par ex

	//set_time_limit(0); // on vire le timeout

	// on vérifie que les valeurs de rognage ne supprime pas toute l'image 
	$oriinfo = getimagesize ( $pic ); 
  
	// en fonction du type, on sélectionne la bonne fonction
	switch ($oriinfo[2]){
		case 1:   //   gif -> jpg
			$src_img = imagecreatefromgif($pic);
			break;
		case 2:   //   jpeg -> jpg
			$src_img = imagecreatefromjpeg($pic);
			break;
		case 3:  //   png -> jpg
			$src_img = imagecreatefrompng($pic);
			break;
	}


	if ( ImageSX ($src_img) >= ImageSY ($src_img) ) {	// Paysage
		$new_w = $new_size;
		$new_h = floor ( ImageSY ($src_img) * $new_size / ImageSX ($src_img) );
	} 
	else {	// Portrait
		$new_h = $new_size;
		$new_w = floor ( ImageSX ($src_img) * $new_size / ImageSY ($src_img) );
	}

	
	$dst_img=imagecreatetruecolor($new_w,$new_h);	// creation de l'img de destination
	
	ImageCopyResized($dst_img,$src_img,0,0,0,0,$new_w,$new_h,ImageSX($src_img),ImageSY($src_img));	// on redim la copie

	ImageJPEG($dst_img, $destpic, 90);	// on colle dans l'img de destination

}   

// redimension d'une image en fonction de sa hauteur
function pic_resize_by_height ($pic, $destpic, $new_h) //$new_h = 100 par ex
{
	
	$src_img=ImageCreateFromJPEG($pic);	// on chope l'image source

	$new_w = floor ( ImageSX ($src_img) * $new_h / ImageSY ($src_img) );	// on calcule la largeur de l'image (w)

	
	$dst_img=imagecreatetruecolor($new_w,$new_h);	// creation de l'img de destination
	
	ImageCopyResized($dst_img,$src_img,0,0,0,0,$new_w,$new_h,ImageSX($src_img),ImageSY($src_img));	// on redim la copie

	ImageJPEG($dst_img, $destpic);	// on colle dans l'img de destination

}   


// permet le dump d'une base mysql

function backup_base ($host, $user, $pass, $base, $file) {

    $db = new PDO( 'mysql:host=' . $host . ';dbname=' . $base, $user, $pass );
    $f = fopen( $file, 'wt' );
 
    $tables = $db->query( 'SHOW TABLES' );
    foreach ( $tables as $table ) {
        echo $table[0] . ' ... '; flush();
        $sql = '-- TABLE: ' . $table[0] . PHP_EOL;
        $create = $db->query( 'SHOW CREATE TABLE `' . $table[0] . '`' )->fetch();
        $sql .= $create['Create Table'] . ';' . PHP_EOL;
        fwrite( $f, $sql );
 
        $rows = $db->query( 'SELECT * FROM `' . $table[0] . '`' );
        $rows->setFetchMode( PDO::FETCH_ASSOC );
        foreach ( $rows as $row ) {
            $row = array_map( array( $db, 'quote' ), $row );
            $sql = 'INSERT INTO `' . $table[0] . '` (`' . implode( '`, `', array_keys( $row ) ) . '`) VALUES (' . implode( ', ', $row ) . ');' . PHP_EOL;
            fwrite( $f, $sql );
        }
 
        $sql = PHP_EOL;
        $result = fwrite( $f, $sql );
        if ( $result !== FALSE ) {
            echo 'OK<br>' . PHP_EOL;
        } else {
            echo 'ERROR!!<br>' . PHP_EOL;
        }
        flush();
    }
    fclose( $f );

}


function dump_base($host, $user, $pass, $base) {

	set_time_limit(0); //on vire le time out de 30s

	mysql_connect($host, $user, $pass);
	mysql_select_db($base);
	$tables = mysql_listtables($base);


	while ( $donnees = mysql_fetch_array($tables) ) {
		$table = $donnees[0];
		$res = mysql_query("SHOW CREATE TABLE $table");
		
		if ( $res ) {
			$insertions = "";
			$tableau = mysql_fetch_array($res);
			$tableau[1] .= ";";
			$dumpsql[] = str_replace("\n", "", $tableau[1]);
			$req_table = mysql_query("SELECT * FROM $table");
			$nbr_champs = mysql_num_fields($req_table);

			while ( $ligne = mysql_fetch_array($req_table) ) {
				$insertions .= "INSERT INTO $table VALUES(";
				
				for ( $i=0; $i<=$nbr_champs-1; $i++ ) {
					$insertions .= "'" . mysql_real_escape_string($ligne[$i]) . "', ";
				}
				
				$insertions = substr($insertions, 0, -2);
				$insertions .= ");\n";
			}
			
			if ( $insertions != "" ) {
				$dumpsql[] = $insertions;
			}
		}
	}
	
	return implode("\r", $dumpsql);
}


function filigrane($fileInHD, $wmFile, $transparency = 40, $jpegQuality = 90) {

	$wmImg   = imageCreateFromGIF($wmFile);
	$jpegImg = imageCreateFromJPEG($fileInHD);

	// WM position
	$wmX = imageSX($jpegImg) - 55;
	$wmY = imageSY($jpegImg) - 55;
	
	
	
	// Water mark process
	imageCopyMerge($jpegImg, $wmImg, $wmX, $wmY, 0, 0, imageSX($wmImg), imageSY($wmImg), $transparency);
	//imageCopyMerge($jpegImg, $wmImg, 0, 0, 0, 0, imageSX($wmImg), imageSY($wmImg), $transparency);

	// Overwriting image
	ImageJPEG($jpegImg, $fileInHD, $jpegQuality);
}





// Pour couper les magic quote chez free !

function fix_magic_quotes ($var = NULL, $sybase = NULL)
{
	// si $sybase n'est pas spécifié, on regarde la configuration ini
	if ( !isset ($sybase) )
	{
		$sybase = ini_get ('magic_quotes_sybase');
	}
 
	// si $var n'est pas spécifié, on corrige toutes les variables superglobales
	if ( !isset ($var) )
	{
		// si les magic_quotes sont activées
		if ( get_magic_quotes_gpc () )
		{
			// tableaux superglobaux a corriger
			$array = array ('_REQUEST', '_GET', '_POST', '_COOKIE');
			if ( substr (PHP_VERSION, 0, 1) <= 4 )
			{
				// PHP5 semble ne pas changer _ENV et _SERVER
				array_push ($array, '_ENV', '_SERVER');
				// les magic_quotes ne changent pas $_SERVER['argv']
				$argv = isset($_SERVER['argv']) ? $_SERVER['argv'] : NULL;        
			}
			foreach ( $array as $var )
			{
				$GLOBALS[$var] = fix_magic_quotes ($GLOBALS[$var], $sybase);
			}
			if ( isset ($argv) )
			{
				$_SERVER['argv'] = $argv;
			}
			// désactive les magic quotes dans ini_set pour que les 
			// scripts qui y sont sensibles fonctionnent
			ini_set ('magic_quotes_gpc', 0);
		}
 
		// idem, pour magic_quotes_sybase
		if ( $sybase )
		{
			ini_set ('magic_quotes_sybase', 0);
		}
 
		// désactive magic_quotes_runtime
		set_magic_quotes_runtime (0);
		return TRUE;
	}
 
	// si $var est un tableau, appel récursif pour corriger chaque élément
	if ( is_array ($var) )
	{
		foreach ( $var as $key => $val )
		{
			$var[$key] = fix_magic_quotes ($val, $sybase);
		}
 
		return $var;
	}
 
	// si $var est une chaine on utilise la fonction stripslashes,
	// sauf si les magic_quotes_sybase sont activées, dans ce cas on 
	// remplace les doubles apostrophes par des simples apostrophes
	if ( is_string ($var) )
	{
		return $sybase ? str_replace ('\'\'', '\'', $var) : stripslashes ($var);
	}
 
	// sinon rien
	return $var;
}

?>
