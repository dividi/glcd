<?PHP


	// fichier de creation / modif / suppr d'un éditeur
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');		
	
	
	
	// Cnx à  la base
	$cnx = new Sql($host, $user, $pass, $db);
	
	// Les LOGS
	$logsql = new log ("fichiers/log_sql.sql");
	$log = new log ("fichiers/log.txt");
	
	// on récupère les paramètres de l'url	
	$action 	= $_GET['action'];
		

	/**************** @@SUPPRESSION ********************/
	
	if ( $action == 'del' ) {
		
		$id = $_POST['id'];
	    $matiereNom = $cnx->QueryOne ( "SELECT matiereNom FROM matieres WHERE matiereID=$id" );
	
		// Suppression de l'utilisateur de la base
		$req_suppr = "DELETE FROM matieres WHERE matiereID=$id;";
		$cnx->Execute ( $req_suppr );
		$logsql->Insert( $req_suppr );
		
		echo "La MATIERE <b>$matiereNom</b> a été supprimée.";	
		$log->Insert( "La MATIERE $matiereNom a été supprimée." );		
	  
	}

	/**************** @@MODIFICATION ********************/	
	if ( $action == 'mod' ) {
	
		$id     	= $_POST ['id'];
		$ref     	= $_POST ['ref'];
		$nom 		= addslashes($_POST ['nom']);
	
		// on récupére les anciennes valeurs du compte pour les logs
		$old_info = $cnx->QueryRow("SELECT matiereREF, matiereNom FROM matieres WHERE matiereID=$id");
		$OLDmatiereREF = $old_info["matiereREF"];
		$OLDmatiereNom = $old_info["matiereNom"];
		
		
		$req_modif = "UPDATE matieres SET matiereREF='$ref', matiereNom='$nom' WHERE matiereID=$id";
		$cnx->Execute ( $req_modif );
		$logsql->Insert( $req_modif );
	
		echo "La MATIERE <b>$OLDmatiereREF-$OLDmatiereNom</b> a été modifiée en <b>$ref-$nom</b>";
		$log->Insert( "La MATIERE $OLDmatiereREF-$OLDmatiereNom a été modifiée en $ref-$nom" );
	}
	
	/**************** @@INSERTION ********************/
	if ( $action == 'add' ) {
	
		$nom 		= addslashes(utf8_decode(urldecode($_POST ['nom'])));
		$ref 		= $_POST ['ref'];
		
		$req_add = "INSERT INTO matieres ( matiereREF, matiereNom ) VALUES ( '$ref', '$nom')";
		$cnx->Execute ( $req_add );
		$logsql->Insert( $req_add );
		
		echo "La MATIERE <b>$nom</b> a été créée.";
		$log->Insert( "La MATIERE $nom a été créée." );
	}
	

?>


