<?PHP


	// fichier de modif de la configuration
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');		
	
	
	
	// Cnx à  la base
	$cnx = new Sql($host, $user, $pass, $db);
	
	// Les LOGS
	$logsql = new log ("fichiers/log_sql.sql");
	$log = new log ("fichiers/log.txt");
	
	// on récupère les paramètres de l'url	
	$action 	= $_GET['action'];
		

	/**************** @@MODIFICATION ********************/	
	if ( $action == 'mod' ) {
	
	
		foreach ($_POST as $key=>$value) {
			
			$req_modif = "UPDATE config SET configValue='$value' WHERE configKey='$key'";
			$cnx->Execute ( $req_modif );
			$logsql->Insert( $req_modif );
			
		}
		
		echo "La configuration a été sauvegardée.";
		$log->Insert( "Modification de la configuration" );

	}

?>


