SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `classes` (
  `classeID` int(11) NOT NULL AUTO_INCREMENT,
  `classeNiveau` int(11) NOT NULL,
  `classeNom` varchar(10) NOT NULL,
  PRIMARY KEY (`classeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

INSERT INTO `classes` (`classeID`, `classeNiveau`, `classeNom`) VALUES
(1, 3, 'A'),
(2, 6, 'B'),
(3, 5, 'C'),
(4, 6, 'C'),
(5, 4, 'E'),
(6, 4, 'D'),
(7, 6, 'A'),
(17, 4, 'A'),
(16, 3, 'E'),
(15, 3, 'D'),
(14, 3, 'C'),
(13, 3, 'B'),
(18, 4, 'B'),
(19, 4, 'C'),
(20, 5, 'A'),
(21, 5, 'B'),
(22, 5, 'D'),
(23, 5, 'E'),
(24, 6, 'D'),
(25, 6, 'E');

CREATE TABLE IF NOT EXISTS `config` (
  `configKey` varchar(255) NOT NULL,
  `configValue` varchar(255) NOT NULL,
  `configHelp` varchar(255) NOT NULL,
  PRIMARY KEY (`configKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `config` (`configKey`, `configValue`, `configHelp`) VALUES
('adresse', '12 rue Paul Codaccioni 13007 MARSEILLE', 'Adresse de l''établissement'),
('college', 'COLLEGE GASTON DEFFERRE', 'Nom de l''établissement'),
('rendre_fiche', '25/09/2014', 'Date pour rendre la fiche Etat des manuels prêtés '),
('rentree', '2014', 'Année de la rentrée scolaire');

CREATE TABLE IF NOT EXISTS `echanges` (
  `echangeID` int(11) NOT NULL AUTO_INCREMENT,
  `exeID` int(11) NOT NULL,
  `userID1` int(11) NOT NULL,
  `userID2` int(11) NOT NULL,
  `echangeDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`echangeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `editeurs` (
  `editeurID` int(11) NOT NULL AUTO_INCREMENT,
  `editeurREF` varchar(2) NOT NULL,
  `editeurNom` varchar(255) NOT NULL,
  PRIMARY KEY (`editeurID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

INSERT INTO `editeurs` VALUES(1, 'NA', 'NATHAN');
INSERT INTO `editeurs` VALUES(2, 'BE', 'BELIN');
INSERT INTO `editeurs` VALUES(3, 'HA', 'HACHETTE');
INSERT INTO `editeurs` VALUES(4, 'HT', 'HATIER');
INSERT INTO `editeurs` VALUES(5, 'DE', 'DELAGRAVE');
INSERT INTO `editeurs` VALUES(6, 'MA', 'MAGNARD');
INSERT INTO `editeurs` VALUES(7, 'LS', 'LELIVRESCOLAIRE');
INSERT INTO `editeurs` VALUES(8, 'EL', 'EVOLANGUES');
INSERT INTO `editeurs` VALUES(9, 'DI', 'DIDIER');
INSERT INTO `editeurs` VALUES(10, 'LR', 'LE ROBERT');

CREATE TABLE IF NOT EXISTS exemplaires (
  exeID int(11) NOT NULL AUTO_INCREMENT,
  exeREF varchar(13) DEFAULT NULL,
  exePrix decimal(10,2) NOT NULL,
  exeAchat varchar(4) NOT NULL,
  exeEtat smallint(6) NOT NULL,
  exeCD tinyint(1) NOT NULL DEFAULT '0',
  exeRendu tinyint(1) NOT NULL DEFAULT '1',
  exeFlag smallint(6) NOT NULL,
  manuelID int(11) NOT NULL,
  PRIMARY KEY (exeID),
  UNIQUE KEY exeREF (exeREF)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `manuels` (
  `manuelID` int(11) NOT NULL AUTO_INCREMENT,
  `manuelREF` varchar(13) NOT NULL,
  `manuelNiveau` varchar(2) NOT NULL,
  `manuelTitre` varchar(255) NOT NULL,
  `manuelEdition` varchar(4) NOT NULL,
  `matiereID` int(11) NOT NULL,
  `editeurID` int(11) NOT NULL,
  PRIMARY KEY (`manuelID`),
  UNIQUE KEY `manuelREF` (`manuelREF`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `matieres` (
  `matiereID` int(11) NOT NULL AUTO_INCREMENT,
  `matiereREF` varchar(2) NOT NULL,
  `matiereNom` varchar(255) NOT NULL,
  PRIMARY KEY (`matiereID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

INSERT INTO `matieres` (`matiereID`, `matiereREF`, `matiereNom`) VALUES
(1, 'MA', 'MATH'),
(2, 'FR', 'FRANCAIS'),
(3, 'AN', 'ANGLAIS'),
(4, 'SV', 'SVT'),
(5, 'SP', 'PHYSIQUE CHIMIE'),
(6, 'ES', 'ESPAGNOL'),
(7, 'IT', 'ITALIEN'),
(8, 'LA', 'LATIN'),
(9, 'GR', 'GREC'),
(10, 'HG', 'HISTGEO-EDUCATION CIV'),
(11, 'TC', 'TECHNOLOGIE'),
(13, 'RU', 'RUSSE');

CREATE TABLE IF NOT EXISTS `prets` (
  `pretID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `exeID` int(11) NOT NULL,
  PRIMARY KEY (`pretID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `userNom` varchar(255) NOT NULL,
  `userPrenom` varchar(255) NOT NULL,
  `userNaissance` date NOT NULL,
  `classeID` int(11) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `auth` (
  `authID` int(11) NOT NULL AUTO_INCREMENT,
  `authLogin` varchar(15) NOT NULL,
  `authPass` varchar(255) NOT NULL,
  PRIMARY KEY (`authID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

INSERT INTO `auth` (`authID`, `authLogin`, `authPass`) VALUES (1, 'glcd', 'defferre');
