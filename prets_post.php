<link rel="stylesheet" href="css/fiches_print.css" type="text/css" media="print"  /> 
<meta http-equiv=Content-Type content="text/html; charset=utf-8" />


<?PHP


	// fichier de creation / modif / suppr d'un éditeur
	
	// lib
	require_once ('fonctions.php');
	include_once ('config.php');
	include_once ('class/Log.class.php');	
	include_once ('class/Sql.class.php');		
	
	
	
	// Cnx à  la base
	$cnx = new Sql($host, $user, $pass, $db);
	
	// Les LOGS
	$logsql = new log ("fichiers/log_sql.sql");
	$log = new log ("fichiers/log.txt");
	
	// on récupère les paramètres de l'url	
	$action 	= $_GET['action'];
		
	$college = $cnx->QueryOne ("SELECT configValue FROM config WHERE configKey='college'");
	$adresse = $cnx->QueryOne ("SELECT configValue FROM config WHERE configKey='adresse'");
	$rentree = $cnx->QueryOne ("SELECT configValue FROM config WHERE configKey='rentree'");
	$rentree2 = $rentree+1;
	
	/**************** @@PRET ********************/
	if ( $action == 'pret' ) {

		$userID = $_POST ['userID'];
		$exeID 	= $_POST ['exeID'];
		$oldExeID 	= $_POST ['oldExeID']; // Les id des livres déjà prêtés
		$etat	= $_POST ['etat'];
		$cd		= $_POST ['cd'];
				
		$user = $cnx->QueryRow ("SELECT userNom, userPrenom, CONCAT(classeNiveau,classeNom) as classe FROM users, classes WHERE users.classeID=classes.classeID AND userID=$userID;");
		$userNom = $user["userNom"];
		$userPrenom = $user["userPrenom"];
		$userClasse = $user["classe"];
		
		$buffer = "";

		$buffer .= "
	<center>
		
		<table id='fiche_pret_entete'>
			
			<tr>
				<td class='logo'><img src='construction/academie.png'></td>
				<td><span class='college'>$college</span><br><span class='college_adr'>$adresse</span></td>
				<td class='logo'><img src='construction/defferre.png'></td>
			</tr>
			<tr>
				<td colspan=3 class='fiche'>FICHE DE PRET MANUELS SCOLAIRES $rentree - $rentree2</td>
			</tr>
			<tr>
				<td class='eleve'>Nom : $userNom</td>
				<td class='eleve'>Prénom : $userPrenom</td>
				<td class='eleve'>Classe : $userClasse</td>
			</tr>		
			
		</table>
	
	
		<table id='fiche_pret_manuels'>
			<th>Titre</th>
			<th>Editeur</th>
			<th>Matière</th>
			<th>CD</th>
			<th>Date Achat</th>
			<th>Prix</th>
			<th>Etat</th>
			<th>Observations</th>
	";
	
		// Livres anciennement prêtés (prêt executé en plusieurs fois)
		for ($i = 0 ; $i < count($oldExeID) ; $i++) {
					
			$exemplaire = $oldExeID[$i];	
			
			$exeInfo = $cnx->QueryRow ("
				SELECT manuelTitre, manuelEdition, matiereNom, editeurNom, exeREF, exePrix, exeAchat, exeEtat, exeCD
				FROM manuels, matieres, editeurs, exemplaires
				WHERE 
					manuels.matiereID=matieres.matiereID AND
					manuels.editeurID=editeurs.editeurID AND
					exemplaires.manuelID=manuels.manuelID AND
					exeID=$exemplaire");
			
			$manuelTitre = $exeInfo["manuelTitre"];
			$matiereNom = $exeInfo["matiereNom"];
			$editeurNom = $exeInfo["editeurNom"];
			$exeREF = $exeInfo["exeREF"];
			$exePrix = $exeInfo["exePrix"];
			$exeAchat = $exeInfo["exeAchat"];
			$exeEtat = $exeInfo["exeEtat"];
			$exeCD = $exeInfo["exeCD"];
			
			switch ($exeEtat) {
				case 4 : $exeEtatLabel = "NEUF"; $etatClass="neuf"; break;
				case 3 : $exeEtatLabel = "BON"; $etatClass="bon"; break;
				case 2 : $exeEtatLabel = "MOYEN"; $etatClass="moyen"; break;
				case 1 : $exeEtatLabel = "PASSABLE"; $etatClass="passable"; break;
			}	
			
			$exeCDlabel = $exeCD == 1 ? "O": "N";

			$buffer .= "
			<tr>
				<td>$manuelTitre</td>			
				<td>$editeurNom</td>			
				<td>$matiereNom</td>			
				<td>$exeCDlabel</td>					
				<td>$exeAchat</td>			
				<td>$exePrix</td>			
				<td>$exeEtatLabel</td>			
				<td width=250></td>			
			<tr>";	
			
		}
	
	
	
		// Livres nouvellement prêtés
		for ($i = 0 ; $i < count($exeID) ; $i++) {
			
			$exemplaire = $exeID[$i];
			$exeEtat = $etat[$i];
			$exeCD	= $cd [$i];
			
			
			$exeInfo = $cnx->QueryRow ("
		SELECT manuelTitre, manuelEdition, matiereNom, editeurNom, exeREF, exePrix, exeAchat
		FROM manuels, matieres, editeurs, exemplaires
		WHERE 
			manuels.matiereID=matieres.matiereID AND
			manuels.editeurID=editeurs.editeurID AND
			exemplaires.manuelID=manuels.manuelID AND
			exeID=$exemplaire");
			
			$manuelTitre = $exeInfo["manuelTitre"];
			$matiereNom = $exeInfo["matiereNom"];
			$editeurNom = $exeInfo["editeurNom"];
			$exeREF = $exeInfo["exeREF"];
			$exePrix = $exeInfo["exePrix"];
			$exeAchat = $exeInfo["exeAchat"];
			
			switch ($exeEtat) {
				case 4 : $exeEtatLabel = "NEUF"; $etatClass="neuf"; break;
				case 3 : $exeEtatLabel = "BON"; $etatClass="bon"; break;
				case 2 : $exeEtatLabel = "MOYEN"; $etatClass="moyen"; break;
				case 1 : $exeEtatLabel = "PASSABLE"; $etatClass="passable"; break;
				case 0 : $exeEtatLabel = "PERDU"; $etatClass="perdu"; break;
			}	
			
			$exeCDlabel = $exeCD == 1 ? "O": "N";

			$buffer .= "
			<tr>
				<td>$manuelTitre</td>
				<td>$editeurNom</td>			
				<td>$matiereNom</td>			
				<td>$exeCDlabel</td>			
				<td>$exeAchat</td>
				<td>$exePrix</td>			
				<td>$exeEtatLabel</td>
				<td width=250></td>			
			<tr>";
			
			
		
			// Ajout du prêt
			$req_add = "INSERT INTO prets ( userID, exeID ) VALUES ( $userID, $exemplaire )";
			$cnx->Execute ( $req_add );
			$logsql->Insert( $req_add );
			
			// MAJ de l'état de l'exemplaire et du CD
			$req_upd = "UPDATE exemplaires SET exeEtat=$exeEtat, exeCD=$exeCD WHERE exeID=$exemplaire";
			$cnx->Execute ( $req_upd );
			$logsql->Insert( $req_upd );
			
		}
		
		$buffer .= "</table>";
		
		$buffer .= "
		<div id='fiche_pret_pied1'>
			Je m'engage à couvrir les manuels et à en prendre soin. En cas de détérioration ou de perte une dégradation sera appliquée en fonction d'un barème dégressif :<br>			
			1ère année = prix du livre neuf / 2ème année = 50% du prix du livre neuf<br>
			3ème année = 30% du prix du livre neuf / 4ème année = 20% du prix du livre neuf<br>
			5ème année et suivante = FORFAIT<br>
			CD = 5€
		</div>
		
		<div id='fiche_pret_arendre'>
			A RENDRE AU PROFESSEUR PRINCIPAL IMPERATIVEMENT AVANT LE :
		</div>
		
		<div id='fiche_pret_pied2'>
			ECRIRE SON NOM ET CLASSE SUR CHAQUE LIVRE<br>
			TOUTE PAGE ARRACHEE, ECRITE OU COUVERTURE ABIMEE = <b>DEGRADATION</b>
		</div>
				
		<div id='fiche_pret_signature'>
			<span style='float:left;'>Marseille, le</span> <br>
			<span style='float:left;'>Signature de l'élève</span><span style='float:right;'>Signature du représentant légal de l'élève</span>
		</div>		
		";
		
		echo $buffer;
		echo "<p class='fiche-break'></p>";
		echo $buffer;
		
	?>
	
		<!-- impression-->
		<script>
			window.print();
		</script>
		
		<?PHP
		
		$log->Insert( count($exeID) . " EXEMPLAIRES(S) prêté(s) à $userID" );
		
	}
	
	/**************** @@RENDRE ********************/
	if ( $action == 'rendre' ) {

		$userID 	= $_POST ['userID'];	// Le user qui rend le livre
		$exeID 		= $_POST ['exeID'];		// La liste des exe à rendre
		$exeIDtemp	= $_POST ['exeID'];		// La liste des exe à rendre : utilisée pour stocker les livres non rendus
		$renduID 	= $_POST ['renduID'];	// LA liste des exe rendus
		$etat 		= $_POST ['etat'];
		$cd			= $_POST ['cd'];
		$paslesien	= $_POST ['paslesien'];
		$echange	= $_POST ['echange'];
		$rentree	= $cnx->QueryOne ("SELECT configValue FROM config WHERE configKey='rentree'");

//echo "<pre>";print_r ($_POST);echo "</pre>";

		$user = $cnx->QueryRow ("SELECT userNom, userPrenom, userNaissance, classeNiveau, classeNom FROM classes, users WHERE users.classeID=classes.classeID AND userID=$userID");
				
		$userNom = $user['userNom'];
		$userPrenom = $user['userPrenom'];
		$userNaissance = $user['userNaissance'];
		$formatDate 	= date("d/m/Y", strtotime($userNaissance));
		$userClasse = $user['classeNiveau'] . $user['classeNom'];
		
		
		$facture = 0;
		$buffer = "";
		
			
	$buffer .= "
	<center>
		
		<table id='fiche_pret_entete'>
			
			<tr>
				<td class='logo'><img src='construction/academie.png'></td>
				<td><span class='college'>$college</span><br><span class='college_adr'>$adresse</span></td>
				<td class='logo'><img src='construction/defferre.png'></td>
			</tr>
			<tr>
				<td colspan=3 class='fiche'>FACTURE MANUELS SCOLAIRES $rentree - $rentree2</td>
			</tr>
			<tr>
				<td class='eleve'>Nom : $userNom</td>
				<td class='eleve'>Prénom : $userPrenom</td>
				<td class='eleve'>Classe : $userClasse</td>
			</tr>		
			
		</table>
				
		<table id='fiche_pret_manuels'>
			
			<th>référence</th>
			<th>propriétaire</th>
			<th>matière</th>
			<th>manuel</th>
			<th>date<br>achat</th>
			<th>prix</th>
			<th>CD<br>prêté</th>
			<th>CD<br>rendu</th>
			<th>état<br>origine</th>
			<th>état<br>rendu</th>
			<th>à payer</th>
		
		";	
			
			// Les livres rendus
			for ($i=0; $i<count($renduID) ;$i++) {
				
				$prix_payer = 0;
				$pc_payer = 0;
				$rendu = true;

				// Le livre n'est pas à lui
				if ($paslesien[$i] <> "" ) {
					// Pas le sien mais on échange avec le propriétaire
					if ($echange[$i] <> "") {
						
						// On supprime les id du tableau des livres à rendre
						$exeIDtemp = array_diff($exeIDtemp, array($echange[$i]));
						$rendu = true;
					}
					else {	
						$rendu = false;
					}
						
				}
				
				
				// Le livre est à lui
				else {
					// On supprime les id du tableau des livres à rendre
					$exeIDtemp = array_diff($exeIDtemp, array($renduID[$i]));	
					$rendu = true;				
				}	



				// On ne gère que les livres non perdus ou échangés
				if ($rendu == true) {

					$exeOrigine = $cnx->QueryRow ("SELECT exeREF, exePrix, exeEtat, exeCD, exeAchat, manuelNiveau, manuelTitre, manuelEdition, editeurNom, matiereNom FROM editeurs, manuels, exemplaires, matieres WHERE exemplaires.manuelID=manuels.manuelID AND manuels.editeurID=editeurs.editeurID AND matieres.matiereID=manuels.matiereID AND exemplaires.exeId=" . $renduID[$i]);
					
					$exeREF 		= $exeOrigine['exeREF'];
					$exePrix 		= $exeOrigine['exePrix'];
					$exeEtat 		= $exeOrigine['exeEtat'];
					$exeAchat 		= $exeOrigine['exeAchat'];
					$exeCD 			= $exeOrigine['exeCD'];
					$manuelNiveau 	= $exeOrigine['manuelNiveau'];
					$manuelTitre 	= $exeOrigine['manuelTitre'];
					$manuelEdition 	= $exeOrigine['manuelEdition'];
					$editeurNom 	= $exeOrigine['editeurNom'];
					$matiereNom 	= $exeOrigine['matiereNom'];
										
						
					
					// Le livre à 1 année scolaire
					if ( ($rentree - $exeAchat)<1 ) {
						
						$delta = $exeEtat - $etat[$i];
						
						if ($delta <= 1) $pc_payer = 0;
						if ($delta >= 2) $pc_payer = 0.5;
												
						$prix_payer = $pc_payer*$exePrix;
												
					}
				
				
					// Le livre à 2 années
					if ( ($rentree - $exeAchat)==1 ) {
						
						$delta = $exeEtat - $etat[$i];
						
						if ($delta <= 1) $pc_payer = 0;
						if ($delta >= 2) $pc_payer = 0.3;
						
						$prix_payer = $pc_payer*$exePrix;												
					}
					
					
					// Le livre à 3 années
					if ( ($rentree - $exeAchat)==2 ) {

						$delta = $exeEtat - $etat[$i];
						
						if ($delta <= 1) $pc_payer = 0;
						if ($delta >= 2) $pc_payer = 0.2;
							
						$prix_payer = $pc_payer*$exePrix;
					}
					
					
					
					// Le livre à 4 ou 5 années
					if ( ($rentree - $exeAchat)==3 || ($rentree - $exeAchat)==4 ) {

						$delta = $exeEtat - $etat[$i];
						
						if ($delta <= 1) $pc_payer = 0;
						if ($delta >= 2) $pc_payer = 0.1;
											
						$prix_payer = $pc_payer*$exePrix;
					}
					
					
					
					// Le livre à plus de 6 années !
					if ( ($rentree - $exeAchat)>4 ) {
						
						$delta = $exeEtat - $etat[$i];
						
						if ($delta <= 1) $pc_payer = 0;
						if ($delta > 1) $pc_payer = -1;

						if ( $pc_payer == 0 ) $prix_payer = $pc_payer*$exePrix;
						else $prix_payer = 0.5;						
					}
					
					
					// Test si le cd est perdu
					if ($cd[$i]==0 && $exeCD==1) {
							$prix_payer += 5;
					}
					
					
					// La douloureuse	
					$facture += $prix_payer;
							
					
					
					// Les labels des états
					switch ($exeEtat) {
						case 4 : $exeEtatOld = "NEUF"; $etatClass="neuf"; break;
						case 3 : $exeEtatOld = "BON"; $etatClass="bon"; break;
						case 2 : $exeEtatOld = "MOYEN"; $etatClass="moyen"; break;
						case 1 : $exeEtatOld = "PASSABLE"; $etatClass="passable"; break;
					}
								
					switch ($etat[$i]) {
						case 4 : $exeEtatNew = "NEUF"; $etatClass="neuf"; break;
						case 3 : $exeEtatNew = "BON"; $etatClass="bon"; break;
						case 2 : $exeEtatNew = "MOYEN"; $etatClass="moyen"; break;
						case 1 : $exeEtatNew = "PASSABLE"; $etatClass="passable"; break;
					}
					
					
					if ($paslesien[$i])
						$proprietaire_nom = $cnx->QueryOne ("SELECT CONCAT(userNom, ' ', userPrenom) as nom FROM users WHERE userID=".$paslesien[$i]);
					else $proprietaire_nom = "";
					
					// Label des CD
					$exeCdOld = $exeCD==1 ? "OUI" : "NON";
					$exeCdNew = $cd[$i]==1 ? "OUI" : "NON";
									
					
					$buffer .= "
					<tr>
						<td>$exeREF</td>
						<td>$proprietaire_nom</td>
						<td>$matiereNom  $editeurNom</td>
						<td>$manuelTitre</td>
						<td>$exeAchat</td>
						<td>$exePrix&nbsp;€</td>
						<td>$exeCdOld</td>
						<td>$exeCdNew</td>
						<td>$exeEtatOld</td>
						<td>$exeEtatNew</td>
						<td>$prix_payer €</td>
					</tr>";
						
						
					// Mise à jour de l'état et du CD
					$changements_etat_cd = "UPDATE exemplaires SET exeEtat=".$etat[$i].", exeCD=".$cd[$i]." WHERE exeREF='$exeREF'";
					$cnx->Execute ( $changements_etat_cd );
					$logsql->Insert( $changements_etat_cd );
																	
					// On tag le livre comme rendu
					$tag_rendu = "UPDATE exemplaires SET exeRendu=1 WHERE exeID=" . $renduID[$i];
					$cnx->Execute ( $tag_rendu );
					$logsql->Insert( $tag_rendu );
					
					
					// Echange des livres
					if ($echange[$i] <> "") {
						
						// On echange le livre rendu par le livre prêté
						$echange_exe1 = "UPDATE prets SET exeID=".$echange[$i]." WHERE userID=".$paslesien[$i]." AND exeID=".$renduID[$i];
						$cnx->Execute ( $echange_exe1 );
						$logsql->Insert( $echange_exe1 );
						
						// On fait l'inverse
						$echange_exe2 = "UPDATE prets SET exeID=".$renduID[$i]." WHERE userID=".$userID." AND exeID=".$echange[$i];
						$cnx->Execute ( $echange_exe2 );
						$logsql->Insert( $echange_exe2 );

						// On alimente la table des echanges pour l'exemplaire échangé
						$insert_echange1 = "INSERT INTO echanges (exeID, userID1, userID2) VALUES (" . $echange[$i] . "," . $userID .", " . $paslesien[$i] . ");";
						$cnx->Execute ( $insert_echange1 );
						$logsql->Insert( $insert_echange1 );
						
						// On alimente la table des echanges pour l'exemplaire non rendu
						$insert_echange2 = "INSERT INTO echanges (exeID, userID1, userID2) VALUES (" . $renduID[$i] . "," . $paslesien[$i] .",". $userID . ");";
						$cnx->Execute ( $insert_echange2 );
						$logsql->Insert( $insert_echange2 );
												
					}
				
					// Suppression du prêt pour les livres rendus UNIQUEMENT
					$req_rendre = "DELETE FROM prets WHERE userID=$userID AND exeID=" . $renduID[$i];
					$cnx->Execute ( $req_rendre );
					$logsql->Insert( $req_rendre );				
				}	
			
			}
			
			
			
			// Les livres perdus (en fait les livres non rendus, les livres dans le tableau $exeIDtemp)
			
			foreach ( $exeIDtemp as $perdu ) {
				
				
				$exeOrigine = $cnx->QueryRow ("SELECT exeREF, exePrix, exeEtat, exeCD, exeAchat, manuelNiveau, manuelTitre, manuelEdition, editeurNom, matiereNom FROM editeurs, manuels, exemplaires, matieres WHERE exemplaires.manuelID=manuels.manuelID AND manuels.editeurID=editeurs.editeurID AND matieres.matiereID=manuels.matiereID AND exemplaires.exeId=" . $perdu);
				
				$exeREF 		= $exeOrigine['exeREF'];
				$exePrix 		= $exeOrigine['exePrix'];
				$exeEtat 		= $exeOrigine['exeEtat'];
				$exeAchat 		= $exeOrigine['exeAchat'];
				$exeCD 			= $exeOrigine['exeCD'];
				$manuelNiveau 	= $exeOrigine['manuelNiveau'];
				$manuelTitre 	= $exeOrigine['manuelTitre'];
				$manuelEdition 	= $exeOrigine['manuelEdition'];
				$editeurNom 	= $exeOrigine['editeurNom'];
				$matiereNom 	= $exeOrigine['matiereNom'];
				
				
						
				// On tag le livre comme NON rendu
				$tag_rendu = "UPDATE exemplaires SET exeRendu=0 WHERE exeID=$perdu";
				$cnx->Execute ( $tag_rendu );
				$logsql->Insert( $tag_rendu );
			
				
				
				// Le livre à 1 année scolaire
				if ( ($rentree - $exeAchat)<1 ) { $prix_payer = $exePrix; }
			
				// Le livre à 2 années
				if ( ($rentree - $exeAchat)==1 ) { $prix_payer = 0.5*$exePrix; }
				
				// Le livre à 3 années
				if ( ($rentree - $exeAchat)==2 ) { $prix_payer = 0.3*$exePrix; }
				
				// Le livre à 4 ou 5 années
				if ( ($rentree - $exeAchat)==3 || ($rentree - $exeAchat)==4 ) { $prix_payer = 0.2*$exePrix; }
				
				// Le livre à plus de 6 années !
				if ( ($rentree - $exeAchat)>4 ) { $prix_payer = 0.5; }
				
				
				// Test si le cd est perdu
				if ($exeCD==1 && ($rentree - $exeAchat)>=1 && ($rentree - $exeAchat)<=4) {
					$prix_payer += 5;
				}
				
				
				// La douloureuse	
				$facture += $prix_payer;
				
				
				// Les labels des états
				switch ($exeEtat) {
					case 5 : $exeEtatOld = "PERDU"; $etatClass="perdu"; break;
					case 4 : $exeEtatOld = "NEUF"; $etatClass="neuf"; break;
					case 3 : $exeEtatOld = "BON"; $etatClass="bon"; break;
					case 2 : $exeEtatOld = "MOYEN"; $etatClass="moyen"; break;
					case 1 : $exeEtatOld = "PASSABLE"; $etatClass="passable"; break;
				}
							
				$exeEtatNew = "NON RENDU"; $etatClass="perdu";
				
				// Label des CD
				$exeCdOld = $exeCD==1 ? "OUI" : "NON";
				$exeCdNew = $cd[$i]==1 ? "OUI" : "NON";
	
	
				$buffer .= "
				<tr>
					<td>$exeREF</td>
					<td></td>
					<td>$matiereNom $editeurNom</td>
					<td>$manuelTitre</td>
					<td>$exeAchat</td>
					<td>$exePrix</td>
					<td>$exeCdOld</td>
					<td>$exeCdNew</td>
					<td>$exeEtatOld</td>
					<td>$exeEtatNew</td>
					<td>$prix_payer&nbsp;€</td>
				</tr>";	
								
			}
			
			$buffer .= "
			<tr>
				<td colspan=9></td>
				<td><b>TOTAL</b></td>
				<td><b>$facture&nbsp;€</b></td>
			</tr>
			
		</table>

		<br>FACTURE A PAYER IMPERATIVEMENT DANS LES 4 JOURS
		
		<br>
					
		<div id='fiche_pret_signature'>
			<span style='float:left;'>Marseille, le ";
			
			
			$buffer .= date("d/m/Y");
			$buffer .= "</span> <span style='float:right;'>Signature</span>
		</div>";
	
	
			
		echo $buffer;
		echo "<p class='fiche-break'></p>";
		echo $buffer;
	
	
	
		// Mise en forme pour le duplicata facture
		$facture_entete = "
			<link rel='stylesheet' href='css/fiches_print.css' type='text/css' media='print'  /> 
			<meta http-equiv=Content-Type content='text/html; charset=utf-8' />
			
			<style>
				#fiche_pret_entete img {height:50px;}
				#fiche_pret_entete .logo {width:100px; height:100px; text-align:center;}
				#fiche_pret_entete .college {font-size:32px; text-align:center;}
				#fiche_pret_entete .college_adr {font-size:12px; text-align:center;}
				#fiche_pret_entete .fiche {font-size:20px; border:1px solid black; text-align:center; width:90%;}
				#fiche_pret_entete .eleve {padding-top:20px;}

				#fiche_pret_manuels {margin-top:25px; margin-left:4px; margin-right:4px; text-align:center; font-size:small; border-collapse:collapse;}
				#fiche_pret_manuels th {padding:3px 10px 3px 10px; background-color:#CCCCCC; border:1px solid grey;}
				#fiche_pret_manuels td {padding:3px 10px 3px 10px; border:1px solid grey;}

				#fiche_pret_pied1 {font-size:small; margin-top:25px;width:80%;}
				#fiche_pret_arendre {font-size:small; margin-top:15px; float:left; width:90%;}
				#fiche_pret_signature {margin-top:15px; width:90%; float:left; width:90%;}
				#fiche_pret_pied2 {font-size:small; margin-top:15px; float:left; width:90%;}					
			</style>
		";
	

		// On supprime éventuellement les factures précédentes
		foreach (glob("factures/". $userClasse . "_" . $userNom . "_" . $userPrenom . "*") as $filename) {
   			unlink($filename);
		}



		// On stock la facture dans le dossier factures
		$fp = fopen("factures/" . $userClasse . "_" . $userNom . "_" . $userPrenom . "_" . $facture . ".html", 'w+');
		fwrite($fp,$facture_entete . $buffer);
		
	
		?>
		<!-- Impression -->
		<script>
			window.print();
		</script>

<?PHP
	}
?>
